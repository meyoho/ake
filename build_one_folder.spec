# -*- mode: python -*-
import ansible
import os
import six
import pkg_resources
from jinja2 import Template

block_cipher = None

SCRIPT_TEMP = """\
import pkg_resources

entries = {}

class EntryPoint(pkg_resources.EntryPoint):
    def load(self, require=False, *args, **kwargs):
        return super(EntryPoint, self).load(require=False, *args, **kwargs)

{% for entry in entries %}
if "{{ entry.group }}" not in entries:
    entries["{{ entry.group }}"] = []
entries["{{ entry.group }}"].append(
    EntryPoint(name="{{ entry.name }}",
               module_name="{{ entry.module_name }}",
               attrs={{ entry.attrs }})
)
{% endfor %}

def new_iter_entry_points(group, name=None):
    for f in entries.get(group, []):
        yield f

setattr(pkg_resources, "iter_entry_points", new_iter_entry_points)

from ake.interfaces.cli import ake
ake()
"""


def NewAnalysis(scripts=None, entry_groups=None, datas=None, **kwargs):

    scripts = scripts or []
    entry_groups = entry_groups or []
    datas = datas or []

    entries = []
    modules = set()
    for group in entry_groups:
        for entry in pkg_resources.iter_entry_points(group):
            entries.append({
                "group": group,
                "name": entry.name,
                "module_name": entry.module_name,
                "attrs": str(entry.attrs)
            })
            modules.add(entry.module_name.split(".")[0]) 

    for module in modules:
        m = __import__(module)
        if os.path.basename(m.__file__) == "__init__.py":
            datas.append((os.path.dirname(m.__file__), module))
        else:
            datas.append((m.__file__, "."))

    script_path = os.path.join(workpath, "shim.py")
    with open(script_path, "w") as fh:
        fh.write(Template(SCRIPT_TEMP).render(entries=entries))

    return Analysis([script_path] + scripts, datas=datas, **kwargs)


a = NewAnalysis(scripts=["ake/__main__.py"],
                entry_groups=["ake_features"],
                datas=[
                    (six.__file__, "."),
                    (os.path.dirname(ansible.__file__), "ansible"),
                    (os.path.abspath(os.path.join(".", "ake-playbook")), "ake-playbook"),
                    (os.path.abspath(os.path.join(".", "man")), "man")
                ],
                hiddenimports=["crypt", "pipes", "configparser", "inventory", "smtplib", "logging.handlers", "distutils.version", "pty", "dateutil.parser"], hookspath=[], runtime_hooks=[],
                excludes=[],
                win_no_prefer_redirects=False,
                win_private_assemblies=False,
                cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name="ake",
          debug=False,
          strip=False,
          upx=True,
          console=True)
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name="ake")
