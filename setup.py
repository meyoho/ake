#!/usr/bin/env python3

# Note: To use the 'upload' functionality of this file, you must:
#   $ pip install twine

import io
import os
import sys
from shutil import rmtree

from setuptools import find_packages, setup, Command

# Package meta-data.
NAME = 'ake'
DESCRIPTION = 'ake is alauda kubernetes engine.'
URL = 'https://bitbucket.com/mathildetech/ake'
EMAIL = 'devs@alauda.io'
AUTHOR = 'Alauda K8S Team'

here = os.path.abspath(os.path.dirname(__file__))

# What packages are required for this module to be executed?
with open(os.path.join(here, "requirements.txt")) as f:
    REQUIRED = [item.split() for item in f.read().split("\n")]

# The rest you shouldn't have to touch too much :)
# ------------------------------------------------
# Except, perhaps the License and Trove Classifiers!

# Import the README and use it as the long-description.
# Note: this will only work if 'README.md' is present in your MANIFEST.in file!
with io.open(os.path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = '\n' + f.read()

# Load the package's __version__.py module as a dictionary.
about = {}
with open(os.path.join(here, NAME, '__version__.py')) as f:
    exec(f.read(), about)


class PublishCommand(Command):
    """Support setup.py publish."""

    description = 'Build and publish the package.'
    user_options = []

    @staticmethod
    def status(s):
        """Prints things in bold."""
        print('\033[1m{0}\033[0m'.format(s))

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        try:
            self.status('Removing previous builds…')
            rmtree(os.path.join(here, 'dist'))
        except FileNotFoundError:
            pass

        self.status('Building Source and Wheel (universal) distribution…')
        os.system('{0} setup.py sdist bdist_wheel --universal'.format(sys.executable))

        self.status('Uploading the package to PyPi via Twine…')
        os.system('twine upload dist/*')

        sys.exit()


# Where the magic happens:
setup(
    name=NAME,
    version=about['__version__'],
    description=DESCRIPTION,
    long_description=long_description,
    author=AUTHOR,
    author_email=EMAIL,
    url=URL,
    packages=find_packages(exclude=('tests',)),
    # If your package is a single module, use this instead of 'packages':
    # py_modules=['mypackage'],

    entry_points={
        'console_scripts': ['ake=ake.interfaces.cli:ake'],
        'ake_features': ['proxy=ake.features.proxy:Proxy',
                         'audit=ake.features.audit:Audit',
                         'oidc=ake.features.oidc:OIDC',
                         'helm=ake.features.helm:Helm',
                         'nginx_ingress=ake.features.nginx_ingress:Ingress',
                         'cert_manager=ake.features.cert_manager:CertManager',
                         'dashboard=ake.features.dashboard:Dashboard',
                         'gpu_accelerator=ake.features.gpu_accelerator:GPUAccelerator',
                         'alauda_base=ake.features.alauda_base:AlaudaBase',
                         'alauda_ingress=ake.features.alauda_ingress:AlaudaIngress',
                         'alauda_portal=ake.features.alauda_portal:AlaudaPortal',
                         'alauda_oidc=ake.features.alauda_oidc:AlaudaOIDC',
                         'aml_core=ake.features.aml_core:AML',
                         'alauda_appcore=ake.features.alauda_appcore:AlaudaAppCore',
                         'alauda_dashboard=ake.features.alauda_dashboard:AlaudaDashboard',
                         'alauda_devops=ake.features.alauda_devops:AlaudaDevops',
                         'alauda_monkey=ake.features.alauda_monkey:AlaudaMonkey',
                         'cluster_registry=ake.features.cluster_registry:ClusterRegistry',
                         'alauda_cert_manager=ake.features.alauda_cert_manager:AlaudaCertManager']
    },
    install_requires=REQUIRED,
    include_package_data=True,
    license='ISC',
    classifiers=[
        # Trove classifiers
        # Full list: https://pypi.python.org/pypi?%3Aaction=list_classifiers
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy'
    ],
    # $ setup.py publish support.
    cmdclass={
        'publish': PublishCommand,
    },
)
