FROM index.alauda.cn/alaudak8s/python:3.6-prod

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
ENTRYPOINT ["ake"]
CMD ["-v"]
USER root
WORKDIR /app

RUN install_packages sshpass ssh libffi6
COPY requirements.txt .
RUN install_packages make gcc \
    && pip install -U --no-cache-dir pip \
    && pip install -r requirements.txt \
    && apt-get purge -y make gcc

COPY . /app
RUN pip install --editable .
