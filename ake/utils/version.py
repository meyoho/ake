from enum import Enum
from typing import Tuple

from semantic_version import Version


def _format_version(ver: str) -> Version:
    ver = ".".join([
        v.lstrip("0") if len(v) > 1 else v
        for v in ver.strip("v").split(".")
    ])
    return Version(ver, partial=True)


def is_semantic_version(ver: str) -> Tuple[bool, str]:
    try:
        _format_version(ver)
    except ValueError as err:
        return False, str(err)
    else:
        return True, ''


def is_patch_version(ver: str) -> bool:
    if len(ver.split(".")) < 3:
        return False
    return True


def is_version_supported(ver: str, support_list: list) -> Tuple[bool, str]:
    for v in support_list:
        assert is_semantic_version(v)[0], f"{v} is not semantic version."
    if not is_semantic_version(ver)[0]:
        return False, f"{ver} is not semantic version."

    ver = _format_version(ver)
    support_vers = [_format_version(v) for v in support_list]
    for v in support_vers:
        if ver == v:
            return True, ''
    else:
        return False, (f"version {ver} is not supported, "
                       f"supported versions are {support_list}")


class CompareResult(Enum):
    GT = 1
    LT = 2
    EQ = 3


def compare_version(ver1: str, ver2: str) -> CompareResult:
    assert is_semantic_version(ver1), f"{ver1} is not semantic version."
    assert is_semantic_version(ver2), f"{ver2} is not semantic version."

    ver1 = _format_version(ver1)
    ver2 = _format_version(ver2)
    if ver1 > ver2:
        return CompareResult.GT
    elif ver1 < ver2:
        return CompareResult.LT
    else:
        return CompareResult.EQ