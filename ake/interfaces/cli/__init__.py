import sys
import traceback
import random
import base64
import string

import click
from pydantic.exceptions import ValidationError
from tabulate import tabulate

from ake.__version__ import __version__
from ake.usecases.settings import (DEFAULT_KUBERNETES_TOKEN,
                                   KUBERNETES_NETWORKS, DEFAULT_REGISTRY,
                                   DEFAULT_PKG_REPO, KUBERNETES_NETWORK_FLANNEL,
                                   DEFAULT_KUBERNETES_PODS_SUBNET,
                                   DEFAULT_KUBE_CONFIG,
                                   Components)
from ake.usecases.kubernetes import KubernetesOperation
from ake.usecases.manpage import create_manpage
from ake.usecases.features import list_features


def ake_exception(command):
    def cache_exception(*args, **kwargs):
        try:
            command(*args, **kwargs)
        except Exception as e:
            if isinstance(e, ValidationError) or isinstance(e, ValueError):
                raise e
            print("", end="")
            print("[AKE] ❌  ❌  ❌  😱  😥  😭  Some terrible error occur. Please contact us.")
            with open("./.ake_error", "w") as f:
                f.write("{}\n".format(e))
                f.write("----" * 10 + "\n")
                f.write("{}".format(traceback.format_exc()))
            sys.exit(-1)
    cache_exception.__name__ = command.__name__
    return cache_exception


def parse_extra_arguments(obj):
    def _replace_key(k):
        return k.replace("-", "_")
    result = {}
    i = 0
    while i < len(obj) - 1:
        if not obj[i].startswith("--"):
            print("Invalid args: {}. Args should start with --.".format(obj[i]))
            sys.exit(2)
        args = obj[i].split("=", 1)
        if obj[i + 1].startswith("--"):
            if len(args) != 2:
                print("{} is not a valid args. Please specify value".format(obj[i]))
                sys.exit(2)
            result[_replace_key(args[0][2:])] = args[1]
            i = i + 1
            continue
        else:
            if len(args) == 2:
                print("Invalid args: {}. Args should start with --.".format(obj[i + 1]))
                sys.exit(2)
            result[_replace_key(obj[i][2:])] = obj[i + 1]
            i = i + 2
    if i == len(obj) - 1:
        if not obj[i].startswith("--"):
            print("Invalid args: {}. Args should start with --.".format(obj[i]))
            sys.exit(2)
        args = obj[i].split("=", 1)
        if len(args) != 2:
            print("{} is not a valid args. Please specify value".format(obj[i]))
            sys.exit(2)
        result[_replace_key(args[0][2:])] = args[1]
    return result


def _random_secret_key():
    return base64.b64encode(
        bytes(
            ''.join(random.choices(string.ascii_uppercase + string.digits, k=32)),
            encoding="ascii"
        )
    ).decode("ascii")


class State(object):

    def __init__(self):
        self.verbose = 0
        self.debug = False


pass_state = click.make_pass_decorator(State, ensure=True)


def verbosity_option(f):
    def callback(ctx, param, value):
        state = ctx.ensure_object(State)
        state.verbose = value
        return value
    return click.option("-v", "--verbose", count=True,
                        expose_value=False,
                        help="Enables verbosity.",
                        callback=callback)(f)


def debug_option(f):
    def callback(ctx, param, value):
        state = ctx.ensure_object(State)
        state.debug = value
        return value
    return click.option('--debug/--no-debug',
                        expose_value=False,
                        help='Enables or disables debug mode.',
                        callback=callback)(f)


def common_options(f):
    f = verbosity_option(f)
    f = debug_option(f)
    return f


def output_and_quit(r):
    if r != 0:
        click.echo("[AKE]: ❌  😥  😨  Install failed!")
    else:
        click.echo("[AKE]: ✅  👏  👍  🎉  🌹  Install successfully! Have fun!")
    sys.exit(r)


@click.group()
def ake():
    '''
    Deploy a kubernetes cluster in one command.

    Please run "ake man" first after you install ake.

    Then you can run "man ake" to detail usage.
    '''
    pass


@click.command(short_help="Deploy kubernetes in the given nodes",
               context_settings=dict(ignore_unknown_options=True, allow_extra_args=True))
@click.option("--masters", prompt=True,
              help="Kubernetes master ip list, separate with comma")
@click.option("--etcds",
              help=("Etcd ip list, separate with comma. "
                    "If not specify they will be deployed in masters"))
@click.option("--nodes", help="Kubernetes node ip list, separate with comma.")
@click.option("--kube-pods-subnet", default=DEFAULT_KUBERNETES_PODS_SUBNET,
              help="The cidr of kubernetes pods.")
@click.option("--network", type=click.Choice(KUBERNETES_NETWORKS),
              default=KUBERNETES_NETWORK_FLANNEL,
              help="Network mode for kubernetes cluster.")
@click.option("--network-opt", multiple=True, help="Network options")
@click.option("--registry", default=DEFAULT_REGISTRY,
              help="The url of registry which contains k8s releated repos.")
@click.option("--cert-sans", type=click.STRING,
              help="Kubernetes api-server cert SANs, separate multi domains with comma")
@click.option("--kubernetes-version", "-kv",
              default=Components.get_default_version("kubernetes"),
              help="Default kubernetes version")
@click.option("--docker-version",
              default=Components.get_default_version("docker"),
              help="Docker version")
@click.option("--etcd-version",
              default=Components.get_default_version("etcd"),
              help="Etcd version")
@click.option("--ssh-username", "-su", help="Default username of the node")
@click.option("--ssh-password", "-sp", hide_input=True, help="Default password of the node")
@click.option("--ssh-key-path", "-sk", help="Default path of ssh secret key")
@click.option("--ssh-port", "-p", default=22, help="Default ssh port of the node")
@click.option("--enabled-features", "-ef", type=click.STRING, help="enabled features")
@click.option("--token", type=click.STRING, default=DEFAULT_KUBERNETES_TOKEN,
              help=("Kubernetes bootstrap token. "
                    f"If not specify '{DEFAULT_KUBERNETES_TOKEN}' will be used."))
@click.option('--dockercfg', type=click.STRING, help="specify docker config file")
@click.option('--pkg-repo', type=click.STRING, default=DEFAULT_PKG_REPO,
              help="specify package repository url.")
@common_options
@pass_state
@click.pass_context
@ake_exception
def deploy(ctx, state, masters, etcds, nodes, kube_pods_subnet, network, network_opt, registry,
           cert_sans, kubernetes_version, docker_version, etcd_version,
           ssh_username, ssh_password, ssh_key_path,
           ssh_port, enabled_features, token, dockercfg, pkg_repo):  # NOSONAR
    args = {
        "masters": masters,
        "etcds": etcds if etcds else masters,
        "nodes": nodes,
        "kube_secret_key": _random_secret_key(),
        "kube_pods_subnet": kube_pods_subnet,
        "network": network,
        "network_opt": network_opt,
        "registry": registry,
        "cert_sans": cert_sans,
        "kubernetes_version": kubernetes_version,
        "docker_version": docker_version,
        "etcd_version": etcd_version,
        "ssh_username": ssh_username,
        "ssh_password": ssh_password,
        "ssh_key_path": ssh_key_path,
        "ssh_port": ssh_port,
        "verbose": state.verbose,
        "debug": state.debug,
        "enabled_features": enabled_features,
        "token": token,
        "dockercfg": dockercfg,
        "pkg_repo": pkg_repo
    }
    extra_args = parse_extra_arguments(ctx.args)
    r = KubernetesOperation.deploy(**{**args, **extra_args})
    output_and_quit(r)


@click.command(short_help="Deploy kubernetes in current host",
               context_settings=dict(ignore_unknown_options=True, allow_extra_args=True))
@click.option("--kube-pods-subnet", default=DEFAULT_KUBERNETES_PODS_SUBNET,
              help="The cidr of kubernetes pods.")
@click.option("--network", type=click.Choice(KUBERNETES_NETWORKS),
              default=KUBERNETES_NETWORK_FLANNEL,
              help="Network mode for kubernetes cluster")
@click.option("--network-opt", multiple=True, help="Network options")
@click.option("--registry", default=DEFAULT_REGISTRY,
              help="The url of registry which contains k8s releated repos.")
@click.option("--cert-sans", type=click.STRING,
              help="Kubernetes api-server cert SANs, separate multi domains with comma")
@click.option("--kubernetes-version", "-kv",
              default=Components.get_default_version("kubernetes"),
              help="Kubernetes version")
@click.option("--docker-version",
              default=Components.get_default_version("docker"),
              help="Docker version")
@click.option("--etcd-version",
              default=Components.get_default_version("etcd"),
              help="Etcd version")
@click.option("--enabled-features", "-ef", type=click.STRING, help="enabled features")
@click.option("--token", type=click.STRING, default=DEFAULT_KUBERNETES_TOKEN,
              help=("Kubernetes bootstrap token. "
                    f"If not specify '{DEFAULT_KUBERNETES_TOKEN}' will be used."))
@click.option('--alauda-devops', is_flag=True, help="enable alauda devops feature")
@click.option('--alauda-k8s', is_flag=True, help="enable alauda dashboard feature")
@click.option('--acp', is_flag=True, help="enable alauda portal feature")
@click.option('--dockercfg', type=click.STRING, help="specify docker config file")
@click.option('--pkg-repo', type=click.STRING, default=DEFAULT_PKG_REPO,
              help="specify package repository url.")
@common_options
@pass_state
@click.pass_context
@ake_exception
def up(ctx, state, kube_pods_subnet, network, network_opt, registry, cert_sans,
       kubernetes_version, docker_version, etcd_version,
       enabled_features, token, alauda_devops, alauda_k8s, acp,
       dockercfg, pkg_repo):  # NOSONAR
    args = {
        "etcds": "localhost",
        "nodes": "localhost",
        "masters": "localhost",
        "kube_secret_key": _random_secret_key(),
        "kube_pods_subnet": kube_pods_subnet,
        "network": network,
        "network_opt": network_opt,
        "registry": registry,
        "cert_sans": cert_sans,
        "kubernetes_version": kubernetes_version,
        "docker_version": docker_version,
        "etcd_version": etcd_version,
        "ssh_username": None,
        "ssh_password": None,
        "ssh_key_path": None,
        "ssh_port": 22,
        "verbose": state.verbose,
        "debug": state.debug,
        "enabled_features": "" if enabled_features is None else enabled_features,
        "token": token,
        "dockercfg": dockercfg,
        "pkg_repo": pkg_repo
    }
    extra_args = parse_extra_arguments(ctx.args)
    if alauda_devops:
        args['enabled_features'] = ("helm,alauda-base,alauda-ingress,alauda-dashboard," +
                                    "alauda-devops,alauda-portal,alauda-appcore," +
                                    args['enabled_features'].strip(","))
    if alauda_k8s:
        args['enabled_features'] = ("helm,alauda-base,alauda-ingress,alauda-dashboard," +
                                    args['enabled_features'].strip(","))
    if acp:
        args['enabled_features'] = ("helm,alauda-base,alauda-ingress,alauda-dashboard," +
                                    "alauda-devops,alauda-portal,alauda-appcore," +
                                    args['enabled_features'].strip(","))
    args['enabled_features'] = args['enabled_features'].strip(",")
    r = KubernetesOperation.deploy(**{**args, **extra_args})
    output_and_quit(r)


@click.command(short_help="Add the given nodes to an existing cluster",
               context_settings=dict(ignore_unknown_options=True, allow_extra_args=True))
@click.option("--nodes", prompt=True, help="Kubernetes node ip list, separate with comma.")
@click.option("--apiserver", prompt=True,
              help="The kubernetes apiserver address. format: ip:port or domain:port")
@click.option("--token", type=click.STRING, default=DEFAULT_KUBERNETES_TOKEN,
              help=("Kubernetes bootstrap token. "
                    f"If not specify '{DEFAULT_KUBERNETES_TOKEN}' will be used."))
@click.option("--registry", default=DEFAULT_REGISTRY,
              help="The url of registry which contains k8s releated repos.")
@click.option('--pkg-repo', type=click.STRING, default=DEFAULT_PKG_REPO,
              help="specify package repository url.")
@click.option("--kubernetes-version", "-kv",
              default=Components.get_default_version("kubernetes"),
              help="Kubernetes version")
@click.option("--ssh-username", "-su", help="Default username of the node")
@click.option("--ssh-password", "-sp", hide_input=True, help="Default password of the node")
@click.option("--ssh-key-path", "-sk", help="Default path of ssh secret key")
@click.option("--ssh-port", "-p", default=22, help="Default ssh port of the node")
@click.option("--enabled-features", "-ef", type=click.STRING, help="enabled features")
@click.option("--docker-version",
              default=Components.get_default_version("docker"),
              help="Docker version")
@click.option('--dockercfg', type=click.STRING, help="customer docker config file")
@common_options
@pass_state
@click.pass_context
@ake_exception
def addnodes(ctx, state, nodes, apiserver, token, registry, pkg_repo, kubernetes_version,
             ssh_username, ssh_password, ssh_key_path, ssh_port, enabled_features,
             docker_version, dockercfg):
    args = {
        "nodes": nodes,
        "apiserver": apiserver,
        "token": token,
        "registry": registry,
        "pkg_repo": pkg_repo,
        "kubernetes_version": kubernetes_version,
        "ssh_username": ssh_username,
        "ssh_password": ssh_password,
        "ssh_key_path": ssh_key_path,
        "ssh_port": ssh_port,
        "verbose": state.verbose,
        "debug": state.debug,
        "enabled_features": enabled_features,
        "docker_version": docker_version,
        "dockercfg": dockercfg
    }
    extra_args = parse_extra_arguments(ctx.args)
    r = KubernetesOperation.add_nodes(**{**args, **extra_args})
    output_and_quit(r)


@click.command(short_help="Add current host to an existing cluster",
               context_settings=dict(ignore_unknown_options=True, allow_extra_args=True))
@click.option("--apiserver", prompt=True,
              help="The kubernetes apiserver address. format: ip:port or domain:port")
@click.option("--token", type=click.STRING, default=DEFAULT_KUBERNETES_TOKEN,
              help=("Kubernetes bootstrap token. "
                    f"If not specify '{DEFAULT_KUBERNETES_TOKEN}' will be used."))
@click.option("--registry", default=DEFAULT_REGISTRY,
              help="The url of registry which contains k8s releated repos.")
@click.option('--pkg-repo', type=click.STRING, default=DEFAULT_PKG_REPO,
              help="specify package repository url.")
@click.option("--kubernetes-version", "-kv",
              default=Components.get_default_version("kubernetes"),
              help="Kubernetes version")
@click.option("--enabled-features", "-ef", type=click.STRING, help="enabled features")
@click.option("--docker-version",
              default=Components.get_default_version("docker"),
              help="Docker version")
@click.option('--dockercfg', type=click.STRING, help="customer docker config file")
@common_options
@pass_state
@click.pass_context
@ake_exception
def join(ctx, state, apiserver, token, registry, pkg_repo, kubernetes_version,
         enabled_features, docker_version, dockercfg):
    args = {
        "nodes": "localhost",
        "apiserver": apiserver,
        "token": token,
        "registry": registry,
        "pkg_repo": pkg_repo,
        "kubernetes_version": kubernetes_version,
        "ssh_username": None,
        "ssh_password": None,
        "ssh_key_path": None,
        "ssh_port": None,
        "verbose": state.verbose,
        "debug": state.debug,
        "enabled_features": enabled_features,
        "docker_version": docker_version,
        "dockercfg": dockercfg
    }
    r = KubernetesOperation.add_nodes(**args)
    output_and_quit(r)


@click.command(short_help="Upgrade the existing kubernetes cluster to a higher version.")
@click.option("--masters", prompt=True,
              help="Kubernetes master ip list, separate with comma")
@click.option("--nodes", help="Kubernetes node ip list, separate with comma.")
@click.option("--etcds",
              help=("Etcd ip list, separate with comma. "
                    "If not specify they will be deployed in masters"))
@click.option("--kubernetes-version",
              default=Components.get_default_version("kubernetes"),
              help="Target kubernetes version")
@click.option("--kube-config", default=DEFAULT_KUBE_CONFIG,
              help=f"Specify kube config path, default is: {DEFAULT_KUBE_CONFIG}")
@click.option("--ssh-username", help="Default username of the node")
@click.option("--ssh-password", hide_input=True, help="Default password of the node")
@click.option("--ssh-key-path", help="Default path of ssh secret key")
@click.option("--ssh-port", "-p", default=22, help="Default ssh port of the node")
@click.option("--registry", default=DEFAULT_REGISTRY,
              help="The url of registry which contains k8s releated repos.")
@click.option('--pkg-repo', type=click.STRING, default=DEFAULT_PKG_REPO,
              help="specify package repository url.")
@common_options
@pass_state
@click.pass_context
@ake_exception
def upgrade(ctx, state, masters, nodes, etcds, kubernetes_version, kube_config,
            ssh_username, ssh_password, ssh_key_path, ssh_port, registry, pkg_repo):
    args = {
        "masters": masters,
        "nodes": nodes,
        "etcds": etcds,
        "kubernetes_version": kubernetes_version,
        "kube_config": kube_config,
        "ssh_username": ssh_username,
        "ssh_password": ssh_password,
        "ssh_key_path": ssh_key_path,
        "ssh_port": ssh_port,
        "registry": registry,
        "pkg_repo": pkg_repo,
        "verbose": state.verbose,
        "debug": state.debug,
    }
    r = KubernetesOperation.upgrade(**args)
    output_and_quit(r)


@click.command(short_help="Create ake manpage")
@ake_exception
def man():
    create_manpage()


@click.command(short_help="Show ake version information")
def version():
    click.echo(__version__)
    headers = ["Components", "Supported Versions",
               "Default Version", "Upgradeable Versions"]
    table = [
        (comp["name"],
         "\n".join(comp["supported_versions"]),
         comp["default_version"] or "-",
         "\n".join(comp["upgradeable_versions"]) or "-")
        for comp in Components.all()
    ]
    click.echo(tabulate(table, headers, tablefmt="fancy_grid"))


@click.group()
def feature():
    '''
    Feature operations
    '''
    pass


@click.command(short_help="List all supported features")
def ls():
    features = [
        (x["name"], x["version"], x["dependencies"])
        for x in list_features()
    ]
    headers = ["NAME", "VERSION", "DEPENDENCIES"]
    click.echo(tabulate(features, headers, tablefmt="rst"))


feature.add_command(ls)

ake.add_command(deploy)
ake.add_command(up)
ake.add_command(addnodes)
ake.add_command(join)
ake.add_command(upgrade)
ake.add_command(man)
ake.add_command(version)
ake.add_command(feature)
