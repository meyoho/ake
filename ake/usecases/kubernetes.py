import collections.abc

from semantic_version import Version

from ake import utils
from ake.operators import kubernetes, kubeadm
from ake.usecases.arguments import (DeployArgumentsModel, ArgumentsValidationError,
                                    AddNodeArgumentsModel, UpgradeArgumentsModel)
from ake.usecases.ansible.ansible_api import AnsibleApiExecutor
from ake.usecases.utils import toposortdict
from ake.usecases.features import get_features
from ake.usecases.settings import (PLAYBOOK_DEPLOY_PATH, PLAYBOOK_ADD_NODE_PATH,
                                   PLAYBOOK_UPGRADE_PATH, INVENTORY_PATH,
                                   LOCALHOST, KEY_PREFIX,
                                   AKE_FEATURES, KUBERNETES_NETWORK_FLANNEL,
                                   KUBERNETES_NETWORK_CALICO,
                                   KUBERNETES_NETWORK_ALAUDA_CALICO,
                                   KUBERNETES_NETWORK_KUBE_OVN,
                                   KUBERNETES_NETWORK_NONE,
                                   KUBERNETES_NETWORK_STRATEGY_FLANNEL,
                                   KUBERNETES_NETWORK_STRATEGY_CANAL,
                                   KUBERNETES_NETWORK_STRATEGY_CALICO,
                                   KUBERNETES_NETWORK_STRATEGY_ALAUDA_CALICO,
                                   KUBERNETES_NETWORK_STRATEGY_KUBE_OVN,
                                   KUBERNETES_NETWORK_STRATEGY_NONE,
                                   Components)
import json
import os


class KubernetesOperation(object):
    '''
    This class define all behavior of kubernetes commands.
    '''

    @classmethod
    def extra_error_msg(cls, e, debug):
        error_msg = ""
        if "errors_dict" in dir(e):
            for k, v in e.errors_dict.items():
                error_msg += "{}\n".format(k)
                if isinstance(v, collections.abc.Sequence):
                    error_lst = v
                else:
                    error_lst = [v]
                for err in error_lst:
                    details = err.get("error_details")
                    if not details:
                        error_msg += "{}\n".format(err["error_msg"])
                        continue
                    for m, n in details.items():
                        error_msg += "\t{}:\t{}\n".format(m, n['error_msg'])
        else:
            error_msg = str(e)
        print(error_msg)
        if debug:
            raise e
        return -1

    @classmethod
    def deploy(cls, **kwargs):
        debug = kwargs.pop("debug")
        kwargs.pop("verbose")
        try:
            args_obj = DeployArgumentsModel(**kwargs)
        except ArgumentsValidationError as e:
            return cls.extra_error_msg(e, debug)
        args_dict = dict(args_obj)
        cls.get_extra_parameters(args_dict)
        try:
            cls.get_features(args_dict)
        except Exception as e:
            return cls.extra_error_msg(e, debug)
        cls.create_inventory(args_dict)
        executor = AnsibleApiExecutor(task_path=PLAYBOOK_DEPLOY_PATH,
                                      debug_mode=debug,
                                      **args_dict)
        executor.init_playbook_executor()
        return executor.executor.run()

    @classmethod
    def add_nodes(cls, **kwargs):
        debug = kwargs.pop("debug")
        kwargs.pop("verbose")
        try:
            args_obj = AddNodeArgumentsModel(**kwargs)
        except ArgumentsValidationError as e:
            return cls.extra_error_msg(e, debug)
        args_dict = dict(args_obj)
        cls.create_inventory(args_dict)
        cls.get_extra_parameters(args_dict)
        try:
            cls.get_features(args_dict)
        except Exception as e:
            return cls.extra_error_msg(e, debug)
        executor = AnsibleApiExecutor(task_path=PLAYBOOK_ADD_NODE_PATH,
                                      debug_mode=debug,
                                      **args_dict)
        executor.init_playbook_executor()
        return executor.executor.run()

    @classmethod
    def upgrade(cls, **kwargs):
        debug = kwargs.pop("debug")
        kwargs.pop("verbose")
        try:
            args_obj = UpgradeArgumentsModel(**kwargs)
        except ArgumentsValidationError as e:
            return cls.extra_error_msg(e, debug)

        # Check if cluster is healthy
        k8s = kubernetes.KubernetesManager(args_obj.kube_config)
        healthy, err = k8s.check_health()
        if not healthy:
            return cls.extra_error_msg(f"Target cluster is not healthy: {err}", debug)

        # Check if version is upgradeable
        target_ver = kwargs["kubernetes_version"]
        current_ver = k8s.get_version()
        upgradeable, err = Components.is_upgradeable("kubernetes", current_ver)
        if not upgradeable:
            return cls.extra_error_msg(err, debug)
        if utils.compare_version(target_ver, current_ver) != utils.CompareResult.GT:
            return cls.extra_error_msg(
                f"Current cluster's version {current_ver} is already equal or higher than {target_ver}.",
                debug)

        # Check if kubeadm config is valid
        try:
            configmap = k8s.get_kubeadm_config()
        except kubernetes.KubernetesApiError as err:
            msg = ("Failed to retrieve kubeadm config, inorder to upgrade, "
                   "a ConfigMap called [x] in the [x] namespace must exist. "
                   f"Reason: {err}")
            return cls.extra_error_msg(msg, debug)
        try:
            kop = kubeadm.KubeadmOperator.init_from_configmap(configmap)
        except kubeadm.InitFailed as err:
            return cls.extra_error_msg(err, debug)
        try:
            revised, revised_cfg = kop.revise_config()
        except kubeadm.ConfigInvalid as err:
            return cls.extra_error_msg(err, debug)
        if revised:
            new_configmap = kubeadm.KubeadmOperator.convert_to_configmap(
                configmap, revised_cfg)
            try:
                k8s.update_kubeadm_config(new_configmap)
            except kubernetes.KubernetesApiError as err:
                return cls.extra_error_msg(f"Failed to update kubeadm-config: {err}", debug)

        args_dict = dict(args_obj)
        cls.create_inventory(args_dict)
        executor = AnsibleApiExecutor(task_path=PLAYBOOK_UPGRADE_PATH,
                                      debug_mode=debug,
                                      **args_dict)
        executor.init_playbook_executor()
        return executor.executor.run()

    @classmethod
    def create_inventory(cls, args_dict):
        masters = args_dict.pop("masters", {"masters": []})
        etcds = args_dict.pop("etcds", {"etcds": []})
        first_master = [] if len(masters["masters"]) == 0 else masters["masters"][0:1]
        other_master = [] if len(masters["masters"]) == 0 else masters["masters"][1:]
        first_etcd = [] if len(etcds["etcds"]) == 0 else etcds["etcds"][0:1]
        other_etcd = [] if len(etcds["etcds"]) == 0 else etcds["etcds"][1:]
        nodes = args_dict.pop("nodes")
        default_vars = {}
        if args_dict.get("ssh_username"):
            default_vars["ansible_user"] = args_dict.pop("ssh_username")
        if args_dict.get("ssh_port"):
            default_vars["ansible_port"] = args_dict.pop("ssh_port")
        if args_dict.get("ssh_password"):
            default_vars["ansible_ssh_pass"] = args_dict.pop("ssh_password")
        if args_dict.get("ssh_key_path"):
            default_vars["ansible_ssh_private_key_file"] = args_dict.pop("ssh_key_path")
        with open(INVENTORY_PATH, "w") as f:
            # f.write("localhost ansible_connection=local\n")
            f.write(cls.generate_inventory_group("first-master", first_master))
            f.write(cls.generate_inventory_group("other-master", other_master))
            f.write(cls.generate_inventory_group("first-etcd", first_etcd))
            f.write(cls.generate_inventory_group("other-etcd", other_etcd))
            f.write(cls.generate_inventory_group("node", nodes["nodes"]))
            f.write("[master:children]\nfirst-master\nother-master\n"
                    "[etcd:children]\nfirst-etcd\nother-etcd\n"
                    "[k8s-cluster:children]\nfirst-master\nother-master\nnode\n")
            f.write(cls.generate_default_vars(default_vars))

    @classmethod
    def get_extra_parameters(cls, args_dict):
        if "apiserver" in args_dict:
            args_dict["kube_apiserver_addr"] = args_dict["apiserver"]
        cls.build_network_options_parameters(args_dict)
        cls.set_network_strategy_parameter(args_dict)
        for comp in Components.all():
            key = comp["name"] + "_supported_versions"
            args_dict[key] = comp["supported_versions"]

    @classmethod
    def build_network_options_parameters(cls, args_dict):
        '''
        This method for ansible variable_manager.extra_vars.
        After this, all vars will be simple key(str) value(str).
        Such as network_opts:
        before:
        {
          "network": "flannel",
          "network_opt": {
                  "options": {
                      "backend_type": "vxlan",
                  }
              }
        }
        After:
        {
            "flannel_backend_type": vxlan,
        }
        '''
        network = args_dict.get("network")
        if not network:
            return
        network_opts = args_dict.get("network_opt")
        if not network_opts:
            return
        for k, v in network_opts['options'].items():
            args_dict["{}_{}".format(network, k)] = v

    @classmethod
    def set_network_strategy_parameter(cls, args_dict):
        network = args_dict.get("network")
        network_opts = args_dict.get("network_opt")
        network_policy = None
        if network_opts:
            network_policy = network_opts["options"].get("network_policy")
        args_dict["network_strategy"] = KUBERNETES_NETWORK_STRATEGY_FLANNEL
        if network == KUBERNETES_NETWORK_NONE:
            args_dict["network_strategy"] = KUBERNETES_NETWORK_STRATEGY_NONE
        elif network == KUBERNETES_NETWORK_FLANNEL and network_policy:
            args_dict["network_strategy"] = KUBERNETES_NETWORK_STRATEGY_CANAL
        elif network == KUBERNETES_NETWORK_FLANNEL:
            args_dict["network_strategy"] = KUBERNETES_NETWORK_STRATEGY_FLANNEL
        elif network == KUBERNETES_NETWORK_CALICO:
            args_dict["network_strategy"] = KUBERNETES_NETWORK_STRATEGY_CALICO
        elif network == KUBERNETES_NETWORK_ALAUDA_CALICO:
            args_dict["network_strategy"] = KUBERNETES_NETWORK_STRATEGY_ALAUDA_CALICO
        elif network == KUBERNETES_NETWORK_KUBE_OVN:
            args_dict["network_strategy"] = KUBERNETES_NETWORK_STRATEGY_KUBE_OVN
        return

    @classmethod
    def get_features(cls, args_dict):
        supported_features = {}
        topo_dict = {}
        for support_feature in get_features():
            supported_features[support_feature.name] = support_feature
        for specify_feature in args_dict["enabled_features"]:
            if specify_feature not in supported_features.keys():
                raise ValueError("{} is an unsupported features".format(specify_feature))
            feature = supported_features[specify_feature]
            d_f = feature.get_dependent_features()
            if not set(d_f).issubset(set(args_dict["enabled_features"])):
                raise ValueError("Please specify feature: {}'s dependent features: {}".format(
                    specify_feature,
                    set(d_f).symmetric_difference(set(args_dict["enabled_features"]).intersection(set(d_f)))))
            topo_dict[specify_feature] = d_f
            feature.parse_args(args_dict)
        args_dict["enabled_features"] = toposortdict(topo_dict)

    @classmethod
    def generate_inventory_group(cls, name, nodes):
        content = f"[{name}]\n"
        if not nodes:
            return content
        for n in nodes:
            node = dict(n)
            ip = node.pop("ip")
            content += ip
            if ip == LOCALHOST:
                content += "\t ansible_connection=local\n"
            else:
                for k, v in dict(node).items():
                    if v:
                        content += f"\t{KEY_PREFIX}{k}={v}"
            content += "\n"
        return content

    @classmethod
    def generate_default_vars(cls, vars):
        if not vars:
            return ""

        content = "[all:vars]\n"
        for k, v in vars.items():
            content += f"{k}={v}\n"
        return content
