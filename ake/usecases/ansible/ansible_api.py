import os
import sys
from collections import namedtuple

from ansible.executor.playbook_executor import PlaybookExecutor
from ansible.parsing.dataloader import DataLoader
from ansible.inventory.manager import InventoryManager
from ansible.vars.manager import VariableManager

from ake.usecases.settings import INVENTORY_PATH, PLAYBOOK_DIR
from ake.usecases.ansible.ansible_logs import CallbackForLog


class AnsibleApiExecutor(object):
    def __init__(self, task_path, debug_mode, **kwargs):
        self.args = kwargs
        self.debug_mode = debug_mode
        self.loader = DataLoader()
        self.variable_manager = None
        self.hosts_path = INVENTORY_PATH
        self.tasks_path = task_path
        self.options = None
        self.executor = None
        self.inventory = None
        self.password = {}
        self.callback = CallbackForLog()
        if debug_mode:
            print("args: {}".format(kwargs))

    def set_options(self):
        opts = ['listtags', 'listtasks', 'listhosts', 'syntax', 'connection',
                'module_path', 'forks', 'ssh_common_args', 'ssh_extra_args',
                'sftp_extra_args', 'scp_extra_args', 'become', 'become_method',
                'become_user', 'verbosity', 'check', 'diff', 'basedir']
        Options = namedtuple('Options', opts)
        self.options = Options(
            listtags=False,
            listtasks=False,
            listhosts=False,
            syntax=False,
            connection='ssh',
            module_path=None,
            forks=100,
            ssh_common_args="-o StrictHostKeyChecking=no",
            ssh_extra_args=None,
            sftp_extra_args=None,
            scp_extra_args=None,
            become=True,
            become_method='sudo',
            become_user='root',
            verbosity=self.args.get("verbosity", "3"),
            check=False,
            diff=False,
            basedir=PLAYBOOK_DIR
        )

    def set_inventory(self):
        self.inventory = InventoryManager(loader=self.loader, sources=[self.hosts_path])

    def set_variable_manager(self):
        self.variable_manager = VariableManager(loader=self.loader, inventory=self.inventory)

    def set_extra_vars(self):
        self.variable_manager.extra_vars = self.args.copy()

    def init_playbook_executor(self):
        self.set_inventory()
        self.set_variable_manager()
        self.set_options()
        self.set_extra_vars()
        self.executor = PlaybookExecutor([self.tasks_path],
                                         inventory=self.inventory,
                                         variable_manager=self.variable_manager,
                                         loader=self.loader,
                                         options=self.options,
                                         passwords=self.password)
        if not self.debug_mode:
            self.executor._tqm._stdout_callback = self.callback
