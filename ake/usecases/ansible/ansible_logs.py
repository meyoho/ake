from ansible.plugins.callback import CallbackBase
from ansible.executor.task_result import TaskResult
from ansible.playbook.task import Task
from ansible.playbook.play import Play
from ansible.playbook import Playbook
from ake.usecases.settings import IGNORE_TASKS, SINGLE_LINE_LENGTH
from tqdm import tqdm
import time


class CallbackForLog(CallbackBase):
    def __init__(self):
        super(CallbackForLog, self).__init__()
        self.results = []
        self.milestones = []
        self.ignore_tasks = []
        self.step = 0
        self.is_output = None

    def stderr(self, result):
        msg = ""
        if not result.get("results"):
            msg += "{}\t".format(result.get("msg")) if result.get("msg") else ""
            msg += "{}\t".format(result.get("module_stderr")) if result.get("module_stderr") else ""
            msg += "{}\t".format(result.get("stderr")) if result.get("stderr") else ""
            msg += "{}\t".format(result.get("module_stdout")) if result.get("module_stdout") else ""
        else:
            for res in result["results"]:
                if res.get("failed") and res.get('msg'):
                    msg += f"{res['msg']}\n"
        if not msg:
            msg = result
        print("[AKE]: {}".format(msg), end="", flush=True)
        print("😱")

    def stdout(self, task_name, emoji):
        if len(task_name) == 0 or task_name in IGNORE_TASKS:
            return
        if not self.is_output:
            print(emoji)
            self.is_output = True

    def v2_runner_on_ok(self, result, **kwargs):
        self.stdout(result.task_name, "✅")

    def v2_runner_on_failed(self, result, ignore_errors=False):
        emoji = "❌" if result.task_name not in self.ignore_tasks else "✅"
        self.stdout(result.task_name, emoji)
        if result.task_name not in self.ignore_tasks:
            self.stderr(result._result)

    def v2_runner_on_unreachable(self, result):
        self.stdout(result.task_name, "❌")
        self.stderr(result._result)

    def v2_on_any(self, *args, **kwargs):
        # hide the error log when task failed and it is ignoreable
        if isinstance(args, tuple) and len(args) > 0:
            obj = args[0]
            if isinstance(obj, Playbook):
                self.milestones = [x.name for x in obj.get_plays()]
            if isinstance(obj, Play):
                pos = self.milestones.index(obj.name) + 1
                print("Processing: {} ...\t{}/{}".format(obj.name, pos, len(self.milestones)), flush=True)
                self.is_output = True
            if isinstance(obj, Task):
                if len(obj.name) > 0 and obj.name not in IGNORE_TASKS:
                    print("\t{}".format(obj.name).ljust(SINGLE_LINE_LENGTH), end="\t", flush=True)
                    self.is_output = False
                if obj.ignore_errors:
                    self.ignore_tasks.append(obj.name)
            if isinstance(obj, TaskResult) and (not self.is_output) and len(obj.task_name) > 0:
                print("✅")
                self.is_output = True
        self.on_any(args, kwargs)