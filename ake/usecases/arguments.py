from pydantic import BaseModel, ValidationError, validator
from urllib.parse import urlparse
from typing import List, Dict
import os
import re
import json
import ipaddress
from ake import utils
from ake.usecases.settings import (KUBERNETES_NETWORK_FLANNEL,
                                   KUBERNETES_NETWORKS,
                                   KUBERNETES_NETWORK_FLANNEL_SUPPORTED_BACKEND_TYPES,
                                   LOCALHOST, INVALID_IP, DEFAULT_FEATURES, DEFAULT_KUBERNETES_TOKEN,
                                   DEFAULT_CERT_SANS,
                                   DEFAULT_PKG_REPO,
                                   DEFAULT_KUBE_CONFIG,
                                   KUBERNETES_NETWORK_CALICO, KUBERNETES_NETWORK_ALAUDA_CALICO,
                                   KUBERNETES_NETWORK_FLANNEL_SUPPORTED_NETWORK_POLICIES,
                                   KUBERNETES_NETWORK_FLANNEL_DEFAULT_BACKEND_TYPE,
                                   DEFAULT_KUBERNETES_PODS_SUBNET,
                                   KUBERNETES_NETWORK_CALICO_DEFAULT_BACKEND_TYPE,
                                   KUBERNETES_NETWORK_CALICO_SUPPORTED_BACKEND_TYPES,
                                   Components)


ArgumentsValidationError = ValidationError


def is_valid_ip(ip, raise_error=True):
    p = re.compile('^((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)$')
    if p.match(ip) is None:
        if raise_error:
            raise ValueError("{} is not a valid ip".format(ip))
        return False
    return True


def is_valid_domain(domain, raise_error=True):
    p = re.compile(r'^(?=.{4,255}$)([a-zA-Z0-9][a-zA-Z0-9-]{,61}[a-zA-Z0-9]\.)+[a-zA-Z0-9]{2,5}$')
    if p.match(domain) is None:
        if raise_error:
            raise ValueError("{} is not a valid domain".format(domain))
        return False
    return True


def is_file_exist(path, raise_error=True):
    if not os.path.exists(path):
        if raise_error:
            raise ValueError("{} not exist.".format(path))
        return False
    return True


def is_valid_port(port, raise_error=True):
    if not str(port).isnumeric() or int(port) <= 0 or int(port) > 65535:
        if raise_error:
            raise ValueError("{} is not a valid port.".format(port))
        return False
    return True


def is_valid_token(token):
    p = re.compile('^([a-z0-9]{6})\.([a-z0-9]{16})')
    if p.match(token) is None:
        raise ValueError(f"{token} is not form of {p.pattern}.")
    return True


def is_valid_cidr(cidr):
    if '/' in cidr:
        try:
            ipaddress.IPv4Network(cidr)
        except ipaddress.AddressValueError as e:
            raise ValueError(str(e)) from None
        except ipaddress.NetmaskValueError as e:
            raise ValueError(str(e)) from None
    else:
        raise ValueError(f"cidr is not form of xx.xx.xx.xx/xx.")


def is_valid_jsonfile(jsonfile, raise_error=True):
    try:
        json.load(jsonfile)
    except ValueError:
        if raise_error:
            raise ValueError("{} is not a valid json file".format(jsonfile))
        return False
    return True


def is_valid_dockercfg(jsonfile):
    is_file_exist(jsonfile)
    with open(jsonfile, 'r') as f:
        is_valid_jsonfile(f)


def is_valid_apiserver(v):
    elems = v.split(":")
    if len(elems) != 2:
        raise ValueError("The format of apiserver should be: ip:port or domain:port")
    if not (is_valid_ip(elems[0], False) or is_valid_domain(elems[0], False)):
        raise ValueError("Invalid apiserver address. It should be an valid ip or domain: [alpha, number, minus]")
    if not(elems[1].isdigit() and int(elems[1]) > 0 and int(elems[1]) < 65535):
        raise ValueError("Invalid apiserver port: {}.".format(elems[1]))


def format_feature(v):
    features = []
    if v:
        features = v.split(",")
    features.extend(DEFAULT_FEATURES)
    return list(set(features))


def format_cert_sans(v):
    cert_sans = []
    if v:
        cert_sans = v.split(",")
    cert_sans.extend(DEFAULT_CERT_SANS)
    return list(set(cert_sans))


def format_node(v):
    '''
    Parse the string of node info to Node obj.
    Support format examples:
    1. localhost
    2. localhost;192.168.1.1
    3. 192.168.1.1(user=alauda,port=2222)
    '''
    formated_nodes = []
    duplicated_ips = []
    nodes = v.split(";")
    for node in nodes:
        ip = node
        pos = node.find("(")
        if pos == -1:
            duplicated_ips.append(ip)
            formated_nodes.append(NodeModel(**{"ip": ip}))
            continue
        ip = node[:pos].strip()
        duplicated_ips.append(ip)
        formated_node = {"ip": ip}
        extra_str = node[pos + 1: len(node) - 1]
        extras = extra_str.split(",")
        for extra in extras:
            s = extra.split("=", 1)
            if len(s) != 2 or len(s[1]) == 0:
                raise ValueError("The format of extra info of node should be: xxx=xxx,xxx=xxx")
            # Hide the ansible key word from the cli
            formated_node[s[0].strip()] = s[1].strip()
        formated_nodes.append(NodeModel(**formated_node))
    if len(nodes) != len(set(duplicated_ips)):
        raise ValueError("Duplicated node info. Please make sure node info is unique in each parameter.")
    return formated_nodes


def _format_nodes(v, node_cls, node_name):
    if not v:
        return node_cls()
    elif not isinstance(v, str):
        return node_cls(**{node_name: v})
    else:
        return node_cls(**{node_name: format_node(v)})


def convert_list_to_dict(nodes):
    result = {}
    for node in nodes:
        ip = node["ip"]
        result[ip] = node
    return result


def is_sameip_has_samecfg(src_obj, src_name, target_obj, target_name):
    '''
    This method is use to confirm the same ip has the same config.
    if user specify the same ip in different arguments(eg: masters and nodes).
    eg:
      --master=192.168.1.1(user=alauda) --nodes=192.168.1.1(user=ubuntu)
    The format of obj in arguments should be:
    {
        "obj_name": node_list=[
            node-1,
            node-2
        ]
    }
    '''
    targets = target_obj[target_name]
    target_nodes = convert_list_to_dict(targets)
    srcs = src_obj[src_name]
    src_nodes = convert_list_to_dict(srcs)
    for ip, info in src_nodes.items():
        if ip in target_nodes.keys() and info != target_nodes[ip]:
            raise ValueError("Please specify the same config for {} in {} and {}".format(
                ip, src_name, target_name))


class DockerCfgModel(BaseModel):
    dockercfg: str = None

    @validator("dockercfg")
    def valid_dockercfg(cls, v):
        v = os.path.expanduser(v)
        is_valid_dockercfg(v)
        return v


class NodeModel(DockerCfgModel):
    '''
    This mode defines the vars which we will write to inventory.
    Eventually we can support all vars in ansible inventory.
    See details: http://docs.ansible.com/ansible/latest/intro_inventory.html#list-of-behavioral-inventory-parameters
    '''

    ip: str
    port: int = None
    user: str = None
    ssh_private_key_file: str = None
    ssh_pass: str = None

    class Config:
        allow_extra = True

    @validator("ip")
    def valid_ip(cls, v):
        if v == INVALID_IP:
            raise ValueError("Please use localhost instead of 127.0.0.1")
        if v == LOCALHOST:
            return v
        is_valid_ip(v)
        return v

    @validator("port")
    def valid_port_range(cls, v):
        is_valid_port(v)
        return v

    @validator("ssh_private_key_file")
    def valid_file_exist(cls, v):
        v = os.path.expanduser(v)
        is_file_exist(v)
        return v

    @validator("ssh_pass")
    def valid_ssh_pass(cls, v, values, **kwargs):
        filepath = values.get("ssh_private_key_file")
        if filepath:
            raise ValueError("Can't specify ssh_pass and ssh_private_key_file at same time.")
        return v


class Masters(BaseModel):
    masters: List[NodeModel] = []


class Etcds(BaseModel):
    etcds: List[NodeModel] = []

    @validator("etcds", whole=True)
    def valid_odd_number(cls, v):
        if v and len(v) % 2 == 0:
            raise ValueError("The number of etcd nodes should be odd")
        return v


class Nodes(BaseModel):
    nodes: List[NodeModel] = []


class NetworkOpt(BaseModel):
    options: Dict = {}


class NodeSshModel(DockerCfgModel):
    ssh_username: str = None
    ssh_password: str = None
    ssh_key_path: str = None
    ssh_port: str = None

    class Config:
        allow_extra = True

    @validator("ssh_username")
    def valid_ssh_username(cls, v):
        # TODO valid username
        return v

    @validator("ssh_key_path")
    def valid_ssh_key_path(cls, v, values, **kwargs):
        pwd = values.get("ssh_password")
        if pwd:
            raise ValueError("Can't specify the default ssh password and default ssh key path at the same time")
        v = os.path.expanduser(v)
        is_file_exist(v)
        return v

    @validator("ssh_port")
    def valid_ssh_port(cls, v):
        is_valid_port(v)
        return v


class AddNodeArgumentsModel(NodeSshModel):
    '''
    parse and valid all argument for the ake addnode command.
    '''

    nodes: Nodes
    apiserver: str
    token: str = DEFAULT_KUBERNETES_TOKEN
    registry: str
    pkg_repo: str = DEFAULT_PKG_REPO
    kubernetes_version: str
    enabled_features: List[str] = []
    docker_version: str

    class Config:
        allow_extra = True

    @validator("kubernetes_version")
    def valid_kubernetes_version(cls, v):
        supported, err = Components.is_supported("kubernetes", v)
        if not supported:
            raise ValueError(err)
        return v

    @validator("docker_version")
    def valid_docker_version(cls, v):
        supported, err = Components.is_supported("docker", v)
        if not supported:
            raise ValueError(err)
        return v

    @validator("nodes", pre=True, whole=True)
    def format_nodes(cls, v):
        return _format_nodes(v, Nodes, "nodes")

    @validator("apiserver")
    def valid_apiserver(cls, v):
        is_valid_apiserver(v)
        return v

    @validator("token")
    def valid_token(cls, v):
        is_valid_token(v)
        return v

    @validator("enabled_features", pre=True, whole=True, always=True)
    def format_enabled_features(cls, v):
        return format_feature(v)


class DeployArgumentsModel(NodeSshModel):
    '''
    parse and valid all arguments from the ake deploy command.
    '''

    # TODO
    # 1. valid masters/etcds/nodes pass the same ip.
    # 2. valid one node at least has two role, but specify different info
    #     (masters=10.1.1.1(ansible_user=root) etcds=101.1.1(ansible_user=ubuntu))
    # 3. valid ansible_user must specify when ssh_username is none.
    #    ansible_pass must specify when ssh_password is none.
    # 4. ssh_key_path and ssh_password can't specify at the same time.

    masters: Masters
    etcds: Etcds
    nodes: Nodes
    kube_secret_key: str
    kube_pods_subnet: str = DEFAULT_KUBERNETES_PODS_SUBNET
    network: str
    network_opt: NetworkOpt = None
    cert_sans: List[str] = []
    registry: str = None
    kubernetes_version: str
    docker_version: str
    etcd_version: str
    enabled_features: List[str] = []
    token: str = DEFAULT_KUBERNETES_TOKEN
    pkg_repo: str = DEFAULT_PKG_REPO

    class Config:
        allow_extra = True

    @validator("kubernetes_version")
    def valid_kubernetes_version(cls, v):
        supported, err = Components.is_supported("kubernetes", v)
        if not supported:
            raise ValueError(err)
        return v

    @validator("docker_version")
    def valid_docker_version(cls, v):
        supported, err = Components.is_supported("docker", v)
        if not supported:
            raise ValueError(err)
        return v

    @validator("etcd_version")
    def valid_etcd_version(cls, v):
        supported, err = Components.is_supported("etcd", v)
        if not supported:
            raise ValueError(err)
        return v

    @validator("masters", pre=True, whole=True)
    def format_masters(cls, v):
        return _format_nodes(v, Masters, "masters")

    @validator("etcds", pre=True, whole=True)
    def format_etcds(cls, v, values, **kwargs):
        etcds_obj = _format_nodes(v, Etcds, "etcds")
        if "masters" in values:
            is_sameip_has_samecfg(dict(etcds_obj), "etcds", dict(values["masters"]), "masters")
        return etcds_obj

    @validator("nodes", pre=True, whole=True)
    def format_nodes(cls, v, values, **kwargs):
        nodes_obj = _format_nodes(v, Nodes, "nodes")
        if "masters" in values:
            is_sameip_has_samecfg(dict(nodes_obj), "nodes", dict(values["masters"]), "masters")
        if "etcds" in values:
            is_sameip_has_samecfg(dict(nodes_obj), "nodes", dict(values["etcds"]), "etcds")
        return nodes_obj

    @validator("kube_pods_subnet")
    def valid_kube_pods_subnet(cls, v):
        is_valid_cidr(v)
        return v

    @validator("network")
    def valid_network(cls, v):
        if v not in KUBERNETES_NETWORKS:
            raise ValueError("network: {} is not supported yet".format(v))
        return v

    @validator("network_opt", pre=True, whole=True, always=True)
    def format_network_opts(cls, v):
        opts = {}
        for opt in v:
            elements = opt.split("=")
            if len(elements) != 2:
                raise ValueError("The format of network_opt should be: xxx=xxx")
            opts[elements[0]] = elements[1]
        return NetworkOpt(options=opts)

    @validator("network_opt")
    def valid_network_opt(cls, v, values, **kwargs):
        current_network = values["network"]
        if current_network == KUBERNETES_NETWORK_FLANNEL:
            if "backend_type" not in v.options:
                v.options["backend_type"] = KUBERNETES_NETWORK_FLANNEL_DEFAULT_BACKEND_TYPE
            if v.options["backend_type"] not in KUBERNETES_NETWORK_FLANNEL_SUPPORTED_BACKEND_TYPES:
                raise ValueError("Unsupported flannel backend_type: {}. It should be one of {}".format(
                    v.options["backend_type"], KUBERNETES_NETWORK_FLANNEL_SUPPORTED_BACKEND_TYPES))
            if ("network_policy" in v.options and
                    v.options["network_policy"] not in KUBERNETES_NETWORK_FLANNEL_SUPPORTED_NETWORK_POLICIES):
                raise ValueError("Unsupported flannel network_policy: {}. It should be one of {}".format(
                    v.options["network_policy"], KUBERNETES_NETWORK_FLANNEL_SUPPORTED_NETWORK_POLICIES))
        elif current_network in [KUBERNETES_NETWORK_CALICO, KUBERNETES_NETWORK_ALAUDA_CALICO]:
            if "backend_type" not in v.options:
                v.options["backend_type"] = KUBERNETES_NETWORK_CALICO_DEFAULT_BACKEND_TYPE
            if v.options["backend_type"] not in KUBERNETES_NETWORK_CALICO_SUPPORTED_BACKEND_TYPES:
                raise ValueError("Unsupported calico backend_type: {}. It should be one of {}".format(
                    v.options["backend_type"], KUBERNETES_NETWORK_CALICO_SUPPORTED_BACKEND_TYPES))

        return v

    @validator("cert_sans", pre=True, whole=True, always=True)
    def format_cert_sans(cls, v):
        return format_cert_sans(v)

    @validator("cert_sans")
    def valid_cert_sans(cls, v):
        if not (is_valid_domain(v, raise_error=False) or is_valid_ip(v, raise_error=False)):
            raise ValueError("{} is not a valid domain or ip")
        return v

    @validator("enabled_features", pre=True, whole=True, always=True)
    def format_enabled_features(cls, v):
        return format_feature(v)

    @validator("token")
    def valid_token(cls, v):
        is_valid_token(v)
        return v

    @validator("pkg_repo")
    def validate_pkg_repo(cls, v):
        try:
            urlparse(v)
        except ValueError:
            raise ValueError(f"{v} is not a valid url.")
        return v.strip("/")


class UpgradeArgumentsModel(NodeSshModel):
    '''
    parse and validiate all arguments from the ake upgrade command.
    '''

    masters: Masters
    nodes: Nodes
    etcds: Etcds
    kubernetes_version: str
    kube_config: str = DEFAULT_KUBE_CONFIG
    registry: str = None
    pkg_repo: str = DEFAULT_PKG_REPO

    class Config:
        allow_extra = True

    @validator("masters", pre=True, whole=True)
    def format_masters(cls, v):
        return _format_nodes(v, Masters, "masters")

    @validator("nodes", pre=True, whole=True)
    def format_nodes(cls, v, values, **kwargs):
        return _format_nodes(v, Nodes, "nodes")

    @validator("etcds", pre=True, whole=True)
    def format_etcds(cls, v, values, **kwargs):
        return _format_nodes(v, Etcds, "etcds")

    @validator("kube_config")
    def validate_kube_config(v):
        v = os.path.expanduser(v)
        is_file_exist(v)
        return v

    @validator("kubernetes_version")
    def valid_kubernetes_version(cls, v):
        # TODO: enable specify blured version, and get the pricise version from online.
        if not utils.is_patch_version(v):
            raise ValueError(f"version {v} need to accurate to the patch version.")

        upgradeable, err = Components.is_supported("kubernetes", v)
        if not upgradeable:
            raise ValueError(err)
        return v

    @validator("pkg_repo")
    def validate_pkg_repo(cls, v):
        try:
            urlparse(v)
        except ValueError:
            raise ValueError(f"{v} is not a valid url.")
        return v.strip("/")
