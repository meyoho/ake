def indegree0(v, e):
    if not v:
        return None
    tmp = v[:]
    for i in e:
        if i[1] in tmp:
            tmp.remove(i[1])
    if not tmp:
        return -1

    for t in tmp:
        for i in range(len(e)):
            if t in e[i]:
                e[i] = 'toDel'
    if e:
        eset = set(e)
        eset.remove('toDel')
        e[:] = list(eset)
    if v:
        for t in tmp:
            v.remove(t)
    return tmp


def toposort(v, e):
    result = []
    while True:
        nodes = indegree0(v, e)
        if nodes is None:
            break
        if nodes == -1:
            return None
        result.extend(nodes)
    return result


def toposortdict(src):
    vertex = list(src.keys())
    edge = []
    for k, v in src.items():
        for e in v:
            edge.append((k, e))
    result = toposort(vertex, edge)
    if result is None:
        raise ValueError("Their is a circle in your feature dependency. Please check.")
    result.reverse()
    return result