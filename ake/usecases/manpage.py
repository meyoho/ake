import shutil
from pathlib import Path
import os
import sys


MAN_PAGE_PATH = "/usr/share/man/man1"
MAN_PAGE_FILENAME = "ake.1.gz"
MAN_PATH_FILEMODE = 644
PERMISSION_ERROR_MSG = "Permission denied"


def create_manpage():
    if not os.path.exists(MAN_PAGE_PATH):
        print("[AKE] {} not exist. Create man page fail.".format(MAN_PAGE_PATH))
        sys.exit(2)
    current_path = os.path.abspath(os.path.dirname(__file__))
    parent_path = str(Path(current_path).parent.parent)
    man_page_path = os.path.join(parent_path, "man", MAN_PAGE_FILENAME)
    try:
        shutil.copy(man_page_path, "{}/{}".format(MAN_PAGE_PATH, MAN_PAGE_FILENAME))
        os.chmod("{}/{}".format(MAN_PAGE_PATH, MAN_PAGE_FILENAME), MAN_PATH_FILEMODE)
    except OSError as e:
        if e.strerror == PERMISSION_ERROR_MSG:
            print("{}. Please try: sudo ake man. If still not work. Please contact the author".format(
                PERMISSION_ERROR_MSG))
            sys.exit(2)
        raise e