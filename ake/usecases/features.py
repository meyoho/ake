import pkg_resources
from ake.usecases.settings import AKE_FEATURES


def get_features():
    return [
        entry.load() for entry in pkg_resources.iter_entry_points(AKE_FEATURES)
    ]


def list_features():
    return [
        {
            "name": x.get_feature_name(),
            "version": x.get_feature_version(),
            "dependencies": x.get_dependent_features()
        }
        for x in get_features()
    ]