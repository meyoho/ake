import os

from ake import utils


class Components:
    kubernetes = {
        "name": "kubernetes",
        "supported_versions": ["v1.15"],
        "default_version": "v1.15.3",
        "upgradeable_versions": ["v1.14", "v1.15"],
    }
    etcd = {
        "name": "etcd",
        "supported_versions": ["v3"],
        "default_version": "v3.3.10",
        "upgradeable_versions": []
    }
    docker = {
        "name": "docker",
        "supported_versions": ["1.12", "18"],
        "default_version": "18.09.5",
        "upgradeable_versions": []
    }
    kube_ovn = {
        "name": "kube-ovn",
        "supported_versions": ["v0.8.0"],
        "default_version": "v0.8.0",
        "upgradeable_versions": []
    }
    flannel = {
        "name": "flannel",
        "supported_versions": ["v0.9"],
        "default_version": "v0.9.1",
        "upgradeable_versions": []
    }
    calico = {
        "name": "calico",
        "supported_versions": ["v3"],
        "default_version": "v3.2.1",
        "upgradeable_versions": []
    }
    cannal = {
        "name": "cannal",
        "supported_versions": ["v3"],
        "default_version": "v3.2.1",
        "upgradeable_versions": []
    }

    @classmethod
    def _assert_valid_comp(cls, comp_name):
        assert comp_name in [
            "kubernetes", "etcd", "docker", "flannel", "calico", "cannal"
        ], f"{comp_name} is not valid component."

    @classmethod
    def all(cls):
        return [
            cls.kubernetes, cls.etcd, cls.docker,
            cls.kube_ovn, cls.flannel, cls.calico,
            cls.cannal
        ]

    @classmethod
    def get_default_version(cls, comp_name):
        cls._assert_valid_comp(comp_name)
        return getattr(cls, comp_name)["default_version"]

    @classmethod
    def is_supported(cls, comp_name, version):
        cls._assert_valid_comp(comp_name)
        if not version and not getattr(cls, comp_name)["default_version"]:
            return

        supported, err = utils.is_version_supported(
            version,
            [v for v in getattr(cls, comp_name)["supported_versions"]])
        if not supported:
            err = f"{comp_name} does not support {version}: {err}"
        return supported, err

    @classmethod
    def is_upgradeable(cls, comp_name, version):
        cls._assert_valid_comp(comp_name)

        supported, err = utils.is_version_supported(
            version,
            [v for v in getattr(cls, comp_name)["upgradeable_versions"]]
        )
        if not supported:
            err = f"{comp_name} does not support upgrade to {version}: {err}"
        return supported, err


DEFAULT_PKG_REPO = "http://kaldr.alauda.cn"
DEFAULT_REGISTRY = "index.alauda.cn"
DEFAULT_KUBERNETES_TOKEN = "abcdef.abcdefghij123456"
DEFAULT_KUBE_CONFIG = "~/.kube/config"
DEFAULT_CERT_SANS = ["alauda.cn", "alauda.io"]

DEFAULT_KUBERNETES_PODS_SUBNET = "10.222.0.0/16"  # NOSONAR

KUBERNETES_NETWORK_NONE = "none"
KUBERNETES_NETWORK_FLANNEL = "flannel"
KUBERNETES_NETWORK_CALICO = "calico"
KUBERNETES_NETWORK_ALAUDA_CALICO = "alauda-calico"
KUBERNETES_NETWORK_KUBE_OVN = "kube_ovn"
KUBERNETES_NETWORKS = [
    KUBERNETES_NETWORK_NONE,
    KUBERNETES_NETWORK_FLANNEL,
    # TODO: comment out in next version
    # KUBERNETES_NETWORK_CALICO,
    KUBERNETES_NETWORK_ALAUDA_CALICO,
    KUBERNETES_NETWORK_KUBE_OVN,
]

KUBERNETES_NETWORK_FLANNEL_DEFAULT_BACKEND_TYPE = "vxlan"
KUBERNETES_NETWORK_FLANNEL_SUPPORTED_NETWORK_POLICIES = ["calico"]
KUBERNETES_NETWORK_FLANNEL_SUPPORTED_BACKEND_TYPES = ['vxlan', 'host-gw']

KUBERNETES_NETWORK_CALICO_DEFAULT_BACKEND_TYPE = "bird"
KUBERNETES_NETWORK_CALICO_SUPPORTED_BACKEND_TYPES = ["bird"]

KUBERNETES_NETWORK_STRATEGY_FLANNEL = "flannel"
KUBERNETES_NETWORK_STRATEGY_CALICO = "calico"
KUBERNETES_NETWORK_STRATEGY_ALAUDA_CALICO = "alauda-calico"
KUBERNETES_NETWORK_STRATEGY_CANAL = "canal"
KUBERNETES_NETWORK_STRATEGY_KUBE_OVN = "kube-ovn"
KUBERNETES_NETWORK_STRATEGY_NONE = "none"

LOCALHOST = "localhost"
INVALID_IP = "127.0.0.1"  # NOSONAR
KEY_PREFIX = "ansible_"

CURRENT_DIR = os.path.abspath(os.path.dirname(__file__))
PROJECT_DIR = os.path.dirname(os.path.dirname(CURRENT_DIR))
PLAYBOOK_DIR = os.path.join(PROJECT_DIR, "ake-playbook")
INVENTORY_PATH = os.path.join(PROJECT_DIR, "inventory")
PLAYBOOK_DEPLOY_PATH = os.path.join(PLAYBOOK_DIR, "deploy.yml")
PLAYBOOK_ADD_NODE_PATH = os.path.join(PLAYBOOK_DIR, "add_node.yml")
PLAYBOOK_UPGRADE_PATH = os.path.join(PLAYBOOK_DIR, "upgrade.yml")

IGNORE_TASKS = ["Gathering Facts"]

SINGLE_LINE_LENGTH = 64

DEFAULT_FEATURES = ["proxy", "oidc"]

AKE_FEATURES = "ake_features"
