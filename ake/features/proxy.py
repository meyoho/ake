from ake.features.features import Feature


FEATURE_NAME = "proxy"
CURRENT_VERSION = "1.0.0"
DEFAULT_PROXY_MODE = "ipvs"
ARG_PROXY_MODE = "proxy_mode"
SUPPORT_MODES = ["ipvs", "iptables"]


class Proxy(Feature):
    name = FEATURE_NAME
    version = CURRENT_VERSION

    @classmethod
    def parse_args(cls, args_dict):
        if ARG_PROXY_MODE not in args_dict.keys():
            args_dict[ARG_PROXY_MODE] = DEFAULT_PROXY_MODE
            return
        mode = args_dict[ARG_PROXY_MODE]
        cls.valid_args(FEATURE_NAME, mode, SUPPORT_MODES)