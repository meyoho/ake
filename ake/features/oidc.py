import re
import os

from ake.features.features import Feature


ARG_OIDC_ISSUER_URL = "oidc_issuer_url"
ARG_OIDC_CLIENT_ID = "oidc_client_id"
ARG_OIDC_USERNAME_CLAIM = "oidc_username_claim"
ARG_OIDC_GROUPS_CLAIM = "oidc_groups_claim"
ARG_OIDC_CA_FILE = "oidc_ca_file"
ARG_OIDC_USERNAME_PREFIX = "oidc_username_prefix"
ARG_OIDC_GROUPS_PREFIX = "oidc-groups-prefix"
ARG_OIDC_SIGNING_ALGS = "oidc-signing-algs"
ARG_OIDC_REQUIRED_CLAIM = "oidc-required-claim"

IP_PATTERN = re.compile(
    r"^((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)$")
DOMAIN_PATTERN = re.compile(
    r"""^([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?""")


class OIDC(Feature):
    name = "oidc"
    version = "1.0.0"
    dependent_features = []

    @classmethod
    def parse_args(cls, args_dict):
        if ARG_OIDC_ISSUER_URL in args_dict.keys():
            if not args_dict[ARG_OIDC_ISSUER_URL].startswith("https://"):
                raise ValueError(f"the value of --{ARG_OIDC_ISSUER_URL} option must be a https address.")
            oidc_host = args_dict[ARG_OIDC_ISSUER_URL].strip("https://")
            if (not IP_PATTERN.match(oidc_host)) and (not DOMAIN_PATTERN.match(oidc_host)):
                raise ValueError(f"the value of --{ARG_OIDC_ISSUER_URL} option must "
                                 "be a valid ip or domain name")

            if not args_dict.get(ARG_OIDC_CLIENT_ID):
                raise ValueError(f"--{ARG_OIDC_CLIENT_ID} must be set when --{ARG_OIDC_ISSUER_URL} is set.")

        if ARG_OIDC_CA_FILE in args_dict.keys():
            ca_file = args_dict[ARG_OIDC_CA_FILE]
            args_dict[ARG_OIDC_CA_FILE] = os.path.abspath(ca_file)
            if not os.path.isfile(args_dict[ARG_OIDC_CA_FILE]):
                raise ValueError(f"{ca_file} does not exist.")
