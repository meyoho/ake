from ake.features.features import Feature


FEATURE_NAME = "dashboard"
CURRENT_VERSION = "0.7.1"


class Dashboard(Feature):
    name = FEATURE_NAME
    version = CURRENT_VERSION
    dependent_features = ["helm", "nginx-ingress"]
