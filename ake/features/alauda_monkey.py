import re
import ipaddress

from ake.features.features import Feature


FEATURE_NAME = "alauda-monkey"
CURRENT_VERSION = "1.0.0"
DEFAULT_MONKEY_IMAGE_TAG = "78bd251712bd2dd24cef31bfe24e67b932e48b4c"
ARG_MONKEY_IMAGE_TAG = "monkey_image_tag"
ARG_MONKEY_DISCOVERY_MODE = "monkey_discovery_mode"
ARG_MONKEY_ROUTE_MODE = "monkey_route_mode"
ARG_MONKEY_NETWORK_TYPE = "monkey_network_type"
ARG_MONKEY_DRIVER = "monkey_driver"
ARG_MONKEY_REGION = "monkey_region"
ARG_MONKEY_SECRET_ID = "monkey_secret_id"
ARG_MONKEY_SECRET_KEY = "monkey_secret_key"
ARG_MONKEY_GATEWAY = "monkey_gateway"
ARG_MONKEY_SUBNET_NAME = "monkey_subnet_name"
ARG_MONKEY_SUBNET_RANGE_START = "monkey_subnet_range_start"
ARG_MONKEY_SUBNET_RANGE_END = "monkey_subnet_range_end"
ARG_MONKEY_CIDR = "kube_pods_subnet"


class AlaudaMonkey(Feature):
    name = FEATURE_NAME
    version = CURRENT_VERSION

    @classmethod
    def parse_args(cls, args_dict):
        if args_dict.get("network") != "none":
            raise ValueError("alauda-monkey feature only works when network-type is 'none'.")

        required_args = [
            ARG_MONKEY_GATEWAY,
            ARG_MONKEY_CIDR,
            ARG_MONKEY_SUBNET_NAME,
            ARG_MONKEY_SUBNET_RANGE_START,
            ARG_MONKEY_SUBNET_RANGE_END
        ]
        for name in required_args:
            if name not in args_dict:
                raise ValueError(f"--{name} option is required by feature {cls.name}")

        # will raise exception when invalid
        try:
            cidr = ipaddress.IPv4Network(args_dict[ARG_MONKEY_CIDR])
        except Exception:
            raise ValueError(f"the value of --{ARG_MONKEY_CIDR} option must be valid cidr.")

        try:
            gateway = ipaddress.IPv4Address(args_dict[ARG_MONKEY_GATEWAY])
        except ipaddress.AddressValueError:
            raise ValueError(f"the value of --{ARG_MONKEY_GATEWAY} option must be valid ip.")
        else:
            if gateway not in cidr:
                raise ValueError(f"the value of --{ARG_MONKEY_GATEWAY} option must be in cidr range.")

        try:
            ip_start = ipaddress.IPv4Address(args_dict[ARG_MONKEY_SUBNET_RANGE_START])
        except ipaddress.AddressValueError:
            raise ValueError(f"the value of --{ARG_MONKEY_SUBNET_RANGE_START} option must be valid ip.")
        if ip_start not in cidr:
            raise ValueError(f"the value of --{ARG_MONKEY_SUBNET_RANGE_START} option must be in cidr range.")

        try:
            ip_end = ipaddress.IPv4Address(args_dict[ARG_MONKEY_SUBNET_RANGE_END])
        except ipaddress.AddressValueError:
            raise ValueError(f"the value of --{ARG_MONKEY_SUBNET_RANGE_END} option must be valid ip.")
        if ip_end not in cidr:
            raise ValueError(f"the value of --{ARG_MONKEY_SUBNET_RANGE_END} option must be in cidr range.")

        if ip_start > ip_end:
            raise ValueError(f"the value of --{ARG_MONKEY_SUBNET_RANGE_START} option"
                             f"must be less than --{ARG_MONKEY_SUBNET_RANGE_END}.")

        cidrs = ipaddress.summarize_address_range(ip_start, ip_end)
        total = 0
        for network in cidrs:
            total = total + len(list(network))
            if total > 65535:
                raise ValueError(
                    f"the ip range between {args_dict[ARG_MONKEY_SUBNET_RANGE_START]} "
                    f"and {args_dict[ARG_MONKEY_SUBNET_RANGE_END]} "
                    f"is too large.(large than 65535)")
