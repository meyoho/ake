from ake.features.features import Feature


ARG_GPU_NODES = "gpu_nodes"
ARG_GPU_DRIVER_DOWNLOAD_URL = "gpu_driver_download_url"


class GPUAccelerator(Feature):
    name = "gpu-accelerator"
    version = "1.0.0"
    dependent_features = []

    @classmethod
    def parse_args(cls, args_dict):
        if ARG_GPU_DRIVER_DOWNLOAD_URL not in args_dict.keys():
            raise ValueError(f"--{ARG_GPU_DRIVER_DOWNLOAD_URL} option is required by feature {cls.name}.")
        if ARG_GPU_NODES not in args_dict.keys():
            raise ValueError(f"--{ARG_GPU_NODES} option is required by feature {cls.name}.")

        gpu_ips = [
            n[0:n.find("(")] if n.find("(") > -1 else n
            for n in args_dict[ARG_GPU_NODES].split(";")
        ]
        master_ips = [n["ip"] for n in args_dict["masters"]["masters"]]
        node_ips = [n["ip"] for n in args_dict["nodes"]["nodes"]]
        unknown_ips = set(gpu_ips) - set(master_ips + node_ips)
        if unknown_ips:
            raise ValueError(f"gpu nodes {','.join(unknown_ips)} is not in cluster masters and nodes.")