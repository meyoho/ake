from ake.features.features import Feature

ARG_ALAUDA_APPCORE_VERSION = "alauda_appcore_version"
ARG_ALAUDA_APPCORE_HOST = "alauda_appcore_host"
ARG_ALAUDA_CHART_REPO = "alauda_chart_repo"
ARG_ALAUDA_APPCORE_REPO_ADDR = "alauda_appcore_chart_repo"
DEFAULT_ALAUDA_CHART_REPO = "http://7DO9QoLtDx1g:2wb4E1iydRj7@alauda-kubernetes-charts.alauda.cn"


class AlaudaAppCore(Feature):
    name = "alauda-appcore"
    version = "0.1.0"
    dependent_features = ["helm", "alauda-base", "alauda-ingress", "alauda-portal", "alauda-dashboard"]

    @classmethod
    def parse_args(cls, args_dict):
        optional_args = (
            (ARG_ALAUDA_CHART_REPO, DEFAULT_ALAUDA_CHART_REPO),
        )
        for arg in optional_args:
            if arg[0] not in args_dict.keys():
                args_dict[arg[0]] = arg[1]