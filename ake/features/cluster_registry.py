from ake.features.features import Feature


FEATURE_NAME = "cluster-registry"
CURRENT_VERSION = "1.0.0"


class ClusterRegistry(Feature):
    name = FEATURE_NAME
    version = CURRENT_VERSION