from ake.features.features import Feature


ARG_CERT_MANAGER_VERSION = "cert_manager_version"


class CertManager(Feature):
    name = "cert-manager"
    version = "1.0.0"
    dependent_features = []