import re
import os

from ake.features.features import Feature


ARG_OIDC_ISSUER_URL = "oidc_issuer_url"
ARG_OIDC_CA_FILE = "oidc_ca_file"
ARG_OIDC_CA_KEY = "oidc_ca_key"
ARG_DEX_HOST = "dex_host"
ARG_ALAUDA_OIDC_VERSION = "alauda_oidc_version"
ARG_ALAUDA_CHART_REPO = "alauda_chart_repo"
ARG_ALAUDA_OIDC_CHART_REPO = "alauda_oidc_chart_repo"
DEFAULT_ALAUDA_CHART_REPO = "http://7DO9QoLtDx1g:2wb4E1iydRj7@alauda-kubernetes-charts.alauda.cn"


class AlaudaOIDC(Feature):
    name = "alauda-oidc"
    version = "1.0.0"
    dependent_features = ["helm", "alauda-base", "alauda-ingress", "alauda-portal", "oidc"]

    @classmethod
    def parse_args(cls, args_dict):
        for opt in (ARG_OIDC_ISSUER_URL, ARG_OIDC_CA_FILE, ARG_OIDC_CA_KEY):
            if opt not in args_dict.keys():
                raise ValueError(f"--{opt} option is required by feature {cls.name}.")

        ca_key = args_dict[ARG_OIDC_CA_KEY]
        args_dict[ARG_OIDC_CA_KEY] = os.path.abspath(ca_key)
        if not os.path.isfile(args_dict[ARG_OIDC_CA_KEY]):
            raise ValueError(f"{ca_key} does not exist.")

        optional_args = (
            (ARG_ALAUDA_CHART_REPO, DEFAULT_ALAUDA_CHART_REPO),
        )
        for arg in optional_args:
            if arg[0] not in args_dict.keys():
                args_dict[arg[0]] = arg[1]
