from ake.features.features import Feature


class AlaudaCertManager(Feature):
    name = "alauda-cert-manager"
    version = "1.0.0"
    dependent_features = ["alauda-base", "cert-manager"]