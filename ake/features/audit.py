from ake.features.features import Feature


FEATURE_NAME = "audit"


class Audit(Feature):
    name = FEATURE_NAME
    version = ""
    dependent_features = []
