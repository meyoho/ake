from ake.features.features import Feature


ARG_NGINX_INGRESS_VERSION = "nginx_ingress_version"


class Ingress(Feature):
    name = "nginx-ingress"
    version = "1.0.0"
    dependent_features = ["helm"]