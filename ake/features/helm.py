from ake.features.features import Feature


FEATURE_NAME = "helm"
CURRENT_VERSION = "1.0.0"
ARG_CHART_REPO_ADDR = ""
ARG_CHART_REPO_USERNAME = ""
ARG_CHART_REPO_PASSWORD = ""


class Helm(Feature):
    name = FEATURE_NAME
    version = CURRENT_VERSION