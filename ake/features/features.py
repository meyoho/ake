class Feature(object):
    name = None
    version = None
    dependent_features = []
    playbook_path = None

    @classmethod
    def get_dependent_features(cls):
        return cls.dependent_features

    @classmethod
    def get_feature_name(cls):
        return cls.name

    @classmethod
    def get_feature_version(cls):
        return cls.version

    @classmethod
    def parse_args(cls, args_dict):
        pass

    @classmethod
    def valid_args(cls, feat_name, spec, supported):
        if spec not in supported:
            raise ValueError("{} is not a supported {}".format(spec, feat_name))
