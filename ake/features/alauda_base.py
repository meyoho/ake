from ake.features.features import Feature


FEATURE_NAME = "alauda-base"
CURRENT_VERSION = "1.0.0"
ARG_ALAUDAK8S_ALAUDA_USERNAME = ""
ARG_ALAUDAK8S_ALAUDA_PASSWORD = ""
ARG_ALAUDAK8S_ALAUDA_EMAIL = ""


class AlaudaBase(Feature):
    name = FEATURE_NAME
    version = CURRENT_VERSION