import sys
import os
import logging
import shutil
import json
import pytest
import pytest_mock
from ake.interfaces.cli import deploy, up, addnodes, join, man, ake, ls, version
from click.testing import CliRunner
from ake.usecases.utils import toposortdict
from ake.__version__ import __version__


logging.basicConfig(level=logging.DEBUG)


class TestDeploy(object):

    def get_basic_deploy_args(self):
        result = {
            "masters": "10.1.1.1",
            "nodes": None,
            "etcds": None,
            "registry": "index.alauda.cn",
            "kubernetes-version": "1.15.3",
            "ssh-username": "ubuntu",
            "ssh-password": None,
            "ssh-key-path": "./tox.ini",
            "ssh-port": 33
        }
        return result

    def get_basic_join_args(self):
        result = {
            "nodes": "192.168.1.1",
            "apiserver": "10.1.1.1:6443",
            "token": "abcdef.0123456789abcdef",
            "ssh-username": "ubuntu",
            "ssh-password": "123456",
            "ssh-key-path": None,
            "ssh-port": 33
        }
        return result

    def _generate_dockercfg(self, filename):
        with open(filename, 'w') as f:
            json.dump({}, f)

    def format_args(self, args, debug=True):
        result = []
        if debug:
            result.append("--debug")
        for k, v in args.items():
            if v is None:
                continue
            if isinstance(v, list):
                for m in v:
                    result.append('--{}={}'.format(k, m))
                continue
            result.append('--{}={}'.format(k, v))
        return result

    def run(self, method, args):
        runner = CliRunner()
        return runner.invoke(method, args)

    def test_node_with_right_format(self):
        node_formats = {
            "just_one_ip": "192.168.1.1",
            "multi_ips": "192.168.1.1;192.168.2.2;192.168.3.3",
            "one_ip_with_ansible_args": "192.168.1.1(user=centos,port=22)",
            "multi_ip_with_ansible_args": "192.168.1.1(user=centos,port=22);\
                                           192.168.2.2(user=ubuntu,port=233);\
                                           192.168.3.3(user=redhat,port=233)",
            "localhost": "localhost",
            "mix_format": "192.168.1.1;192.168.2.2(user=ubuntu,port=233);localhost",
            "with_space": "192.168.1.1; 192.168.2.2(user=ubuntu, port=233);localhost",
            "password_with_equal": "23.97.66.162(user=alauda,ssh_pass=qE0bdzYXC/hBNR+SWMM=)"
        }
        args = self.get_basic_deploy_args()
        for k, v in node_formats.items():
            args['masters'] = v
            f_args = self.format_args(args)
            result = self.run(deploy, f_args)
            assert "extra fields not permitted" not in result.output
            assert result.exit_code == 0

    def test_with_invalid_ip_format(self):
        node_formats = {
            "invalid_ip_1": "172.168.256.1",
            "invalid_ip_2": "This_is_not_a_ip"
        }
        args = self.get_basic_deploy_args()
        for k, v in node_formats.items():
            args['masters'] = v
            f_args = self.format_args(args)
            result = self.run(deploy, f_args)
            assert "{} is not a valid ip".format(v) in result.output
            assert result.exit_code == -1

    def test_with_invalid_ssh_port(self):
        node_formats = {
            "invalid_port": "192.168.2.2(user=ubuntu,port=66666)"
        }
        args = self.get_basic_deploy_args()
        for k, v in node_formats.items():
            args['masters'] = v
            f_args = self.format_args(args)
            result = self.run(deploy, f_args)
            assert "is not a valid port" in result.output
            assert result.exit_code == -1

    def test_invalid_password_format(self):
        node_formats = {
            "invalid_password_format": "23.97.66.162(user=alauda,ssh_pass=)"
        }
        args = self.get_basic_deploy_args()
        for k, v in node_formats.items():
            args['masters'] = v
            f_args = self.format_args(args)
            result = self.run(deploy, f_args)
            assert "The format of extra info of node should be" in result.output
            assert result.exit_code == -1

    def test_with_not_exist_key_path(self):
        node_formats = {
            "not_exist_key_path": "192.168.2.2(ssh_private_key_file=/abc.pem)"
        }
        args = self.get_basic_deploy_args()
        for k, v in node_formats.items():
            args['masters'] = v
            f_args = self.format_args(args)
            result = self.run(deploy, f_args)
            assert "/abc.pem not exist." in result.output
            assert result.exit_code == -1

    def test_specify_pwd_and_keypath_in_node(self):
        node_formats = {
            "pwd_and_keypath": "192.168.2.2(ssh_private_key_file=./deploy.yml,ssh_pass=123456)"
        }
        args = self.get_basic_deploy_args()
        for k, v in node_formats.items():
            args['masters'] = v
            f_args = self.format_args(args)
            result = self.run(deploy, f_args)
            assert "Can't specify ssh_pass and ssh_private_key_file at same time" in result.output
            assert result.exit_code == -1

    def test_with_invalid_token(self):
        node_formats = {
            "invalid_token": "asdasd.asdasd",
        }
        args = self.get_basic_deploy_args()
        for k, v in node_formats.items():
            args['token'] = v
            f_args = self.format_args(args)
            result = self.run(deploy, f_args)
            assert "{} is not form of".format(v) in result.output
            assert result.exit_code == -1

    def test_with_valid_token(self):
        node_formats = {
            "token": "acdefg.hijklm1234567890",
        }
        args = self.get_basic_deploy_args()
        for k, v in node_formats.items():
            args['token'] = v
            f_args = self.format_args(args)
            result = self.run(deploy, f_args)
            assert result.exit_code == 0

    def test_with_invalid_sans(self):
        cert_sans = {
            "invalid_single_domain": "test-io",
            "invalid_multi_domain": "test-io,foo-io"
        }
        args = self.get_basic_deploy_args()
        for k, v in cert_sans.items():
            args["cert-sans"] = v
            f_args = self.format_args(args)
            result = self.run(deploy, f_args)
            assert "is not a valid domain" in result.output
            assert result.exit_code == -1

    def test_with_valid_sans(self):
        cert_sans = {
            "single_domain": "test.io",
            "multi_domain": "test.io,foo.io",
            "ip": "127.0.0.1",
            "domain_and_ip": "test.io,127.0.0.1"
        }
        args = self.get_basic_deploy_args()
        for k, v in cert_sans.items():
            args["cert-sans"] = v
            f_args = self.format_args(args)
            result = self.run(deploy, f_args)
            assert result.exit_code == 0

    def test_with_unrecognized_args(self):
        args = self.get_basic_deploy_args()
        args["unrecognized-key-1"] = "unrecognized_value_1"
        f_args = self.format_args(args)
        result = self.run(deploy, f_args)
        assert result.exit_code == 0

    def test_with_invalid_etcd_num(self):
        args = self.get_basic_deploy_args()
        args["etcds"] = "192.168.1.1;192.168.2.2"
        f_args = self.format_args(args)
        result = self.run(deploy, f_args)
        assert result.exit_code == -1
        assert "The number of etcd nodes should be odd" in result.output

    # remove in next version when calico is enabled
    def test_with_disabled_calico_network(self):
        args = self.get_basic_deploy_args()
        args["network"] = "calico"
        f_args = self.format_args(args)
        result = self.run(deploy, f_args)
        assert result.exit_code == 2
        assert r'"--network": invalid choice' in result.output

    def test_invalid_network(self):
        args = self.get_basic_deploy_args()
        args["network"] = "not-exist-network"
        f_args = self.format_args(args)
        result = self.run(deploy, f_args)
        assert result.exit_code == 2
        assert r'"--network": invalid choice' in result.output

    def test_invalid_flannel_network_opts_1(self):
        # specify wrong network_policy
        args = self.get_basic_deploy_args()
        args["network-opt"] = ["network_policy=noexist"]
        f_args = self.format_args(args)
        result = self.run(deploy, f_args)
        assert result.exit_code == -1
        assert r"Unsupported flannel network_policy" in result.output

    def test_invalid_flannel_network_opts_2(self):
        # specify wrong backend_type
        args = self.get_basic_deploy_args()
        args["network-opt"] = ["backend_type=noexist"]
        f_args = self.format_args(args)
        result = self.run(deploy, f_args)
        assert result.exit_code == -1
        assert r"Unsupported flannel backend_type" in result.output

    def test_invalid_flannel_network_opts_3(self):
        # specify with wrong format
        args = self.get_basic_deploy_args()
        args["network-opt"] = ["backend_type=vlan", "invalid_format"]
        f_args = self.format_args(args)
        result = self.run(deploy, f_args)
        assert result.exit_code == -1
        assert r"The format of network_opt should be: xxx=xxx" in result.output

    def test_invalid_flannel_network_opts_4(self):
        # specify not support backend_type
        args = self.get_basic_deploy_args()
        args["network-opt"] = ["backend_type=not-support"]
        f_args = self.format_args(args)
        result = self.run(deploy, f_args)
        assert result.exit_code == -1
        assert r"Unsupported flannel backend_type: not-support" in result.output

    def test_invalid_kube_pods_subnet(self):
        # specify invalid cidr
        args = self.get_basic_deploy_args()
        args["kube-pods-subnet"] = "10.1.1.1/24"
        f_args = self.format_args(args)
        result = self.run(deploy, f_args)
        assert result.exit_code == -1
        assert r"has host bits set" in result.output

    def test_command_deploy(self):
        args = self.get_basic_deploy_args()
        f_args = self.format_args(args)
        result = self.run(deploy, f_args)
        assert "extra fields not permitted" not in result.output
        assert result.exit_code == 0

    def test_command_up(self):
        result = self.run(up, [])
        assert "extra fields not permitted" not in result.output
        assert result.exit_code == 0

    def test_valid_apiserver(self):
        apiserver_formats = {
            "ip": "192.168.1.1:6443",
            "domain": "alauda.cn:6443"
        }
        args = self.get_basic_join_args()
        for k, v in apiserver_formats.items():
            args['apiserver'] = v
            f_args = self.format_args(args)
            result = self.run(addnodes, f_args)
            assert "extra fields not permitted" not in result.output
            assert result.exit_code == 0

    def test_invalid_apiserver(self):
        apiserver_formats = {
            "no_colon": "no-colon-apiserver",
            "multi_colon": "http://multi-colons:8080",
            "invalid_domain": r"i-have-invalid-#-symbol:6443",
            "invalid_ip": "10.1.0.256:6443",
            "invalid_port": "127.0.0.1:65536"
        }
        args = self.get_basic_join_args()
        for k, v in apiserver_formats.items():
            args['apiserver'] = v
            f_args = self.format_args(args)
            result = self.run(addnodes, f_args)
            assert "The format of apiserver should be" in result.output or \
                   "Invalid apiserver address. It should be an valid ip or" in result.output or \
                   "Invalid apiserver port" in result.output or \
                   "is not a valid ip" in result.output
            assert result.exit_code == -1

    def test_invalid_bootstrap_token(self):
        token_formats = {
            "invalid_length": "12345.aaaabbbbccc",
            "invalid_format": "noperiodinthistoken",
            "invalid_chart": "123456.Abcdef0123456789"
        }
        args = self.get_basic_join_args()
        for k, v in token_formats.items():
            args['token'] = v
            f_args = self.format_args(args)
            result = self.run(addnodes, f_args)
            assert "{} is not form of".format(v) in result.output
            assert result.exit_code == -1

    def test_command_addnodes(self):
        args = self.get_basic_join_args()
        f_args = self.format_args(args)
        result = self.run(addnodes, f_args)
        assert "extra fields not permitted" not in result.output
        assert result.exit_code == 0

    def test_command_addnodes_with_dockercfg(self):
        args = self.get_basic_join_args()
        args['dockercfg'] = './test.json'
        self._generate_dockercfg(args['dockercfg'])
        f_args = self.format_args(args)
        result = self.run(addnodes, f_args)
        assert "extra fields not permitted" not in result.output
        assert result.exit_code == 0

    def test_command_join(self):
        result = self.run(join, ["--token=abcdef.0123456789abcdef", "--apiserver=127.0.0.1:6443"])
        assert "extra fields not permitted" not in result.output
        assert result.exit_code == 0

    def test_command_join_with_dockercfg(self):
        args = self.get_basic_join_args()
        args['dockercfg'] = './test.json'
        self._generate_dockercfg(args['dockercfg'])
        result = self.run(join, ["--token=abcdef.0123456789abcdef", "--apiserver=127.0.0.1:6443"])
        assert "extra fields not permitted" not in result.output
        assert result.exit_code == 0

    def test_errorlog_msg(self):
        args = self.get_basic_deploy_args()
        args["masters"] = "localhost"
        args["nodes"] = "localhost"
        args["raise_error"] = "yes"
        f_args = self.format_args(args, False)
        result = self.run(deploy, f_args)
        assert "No such file or directory" in result.output
        assert result.exit_code == 2

    def test_errorlog_stderr(self):
        args = self.get_basic_deploy_args()
        args["masters"] = "localhost"
        args["nodes"] = "localhost"
        args["raise_error"] = "yes"
        f_args = self.format_args(args, False)
        result = self.run(deploy, f_args)
        assert "No such file or directory" in result.output
        assert result.exit_code == 2

    def test_invalid_ip(self):
        args = self.get_basic_deploy_args()
        args["masters"] = "127.0.0.1"
        f_args = self.format_args(args)
        result = self.run(deploy, f_args)
        assert "Please use localhost instead of 127.0.0.1" in result.output
        assert result.exit_code == -1

    def test_input_same_ip_in_one_argument(self):
        node_formats = [
            {"masters": "localhost;192.168.1.1;192.168.1.1"},
            {"masters": "localhost", "etcds": "192.168.1.1;192.168.1.1;localhost"},
            {"nodes": "localhost;10.1.1.1(user=alauda,port=22);10.1.1.1"}
        ]
        args = self.get_basic_deploy_args()
        for k in node_formats:
            args.update(k)
            f_args = self.format_args(args)
            result = self.run(deploy, f_args)
            assert "Duplicated node info. Please make sure node info is unique in each parameter" in result.output
            assert result.exit_code == -1

    def test_same_ip_has_same_config(self):
        args = self.get_basic_deploy_args()
        args["masters"] = "192.168.1.1(user=alauda)"
        args["nodes"] = "192.168.1.1(user=ubuntu)"
        f_args = self.format_args(args)
        result = self.run(deploy, f_args)
        assert "Please specify the same config for" in result.output
        assert result.exit_code == -1

    def test_specify_both_pwd_keypath(self):
        args = self.get_basic_deploy_args()
        args["ssh-password"] = "123456"
        f_args = self.format_args(args)
        result = self.run(deploy, f_args)
        assert "Can't specify the default ssh password and default ssh key path at the same time" in result.output
        assert result.exit_code == -1

    def test_alauda_monkey_with_default_network(self):
        args = {
            "enabled-features": "alauda-monkey",
            "monkey_gateway": "192.168.0.1",
            "kube-pods-subnet": "192.168.0.0/16",
            "monkey_subnet_name": "test",
            "monkey_subnet_range_start": "192.168.1.1",
            "monkey_subnet_range_end": "192.168.1.200"
        }
        f_args = self.format_args(args)
        result = self.run(up, f_args)
        assert result.exit_code == -1
        assert "alauda-monkey feature only works when network-type is 'none'." in result.output

    # TODO: add test test_alauda_monkey_with_calico_network

    def test_alauda_monkey_with_alauda_calico_network(self):
        args = {
            "network": "alauda-calico",
            "enabled-features": "alauda-monkey",
            "monkey_gateway": "192.168.0.1",
            "kube-pods-subnet": "192.168.0.0/16",
            "monkey_subnet_name": "test",
            "monkey_subnet_range_start": "192.168.1.1",
            "monkey_subnet_range_end": "192.168.1.200"
        }
        f_args = self.format_args(args)
        result = self.run(up, f_args)
        assert result.exit_code == -1
        assert "alauda-monkey feature only works when network-type is 'none'." in result.output

    def test_alauda_monkey_with_flannel_network(self):
        args = {
            "network": "flannel",
            "enabled-features": "alauda-monkey",
            "monkey_gateway": "192.168.0.1",
            "kube-pods-subnet": "192.168.0.0/16",
            "monkey_subnet_name": "test",
            "monkey_subnet_range_start": "192.168.1.1",
            "monkey_subnet_range_end": "192.168.1.200"
        }
        f_args = self.format_args(args)
        result = self.run(up, f_args)
        assert result.exit_code == -1
        assert "alauda-monkey feature only works when network-type is 'none'." in result.output

    def test_alauda_monkey_with_large_subnet_range(self):
        args = {
            "network": "none",
            "enabled-features": "alauda-monkey",
            "monkey_gateway": "10.0.0.1",
            "kube-pods-subnet": "10.0.0.0/8",
            "monkey_subnet_name": "test",
            "monkey_subnet_range_start": "10.168.0.1",
            "monkey_subnet_range_end": "10.169.0.1"
        }
        f_args = self.format_args(args)
        result = self.run(up, f_args)
        assert result.exit_code == -1
        assert "the ip range between 10.168.0.1 and 10.169.0.1 is too large." in result.output

    def test_alauda_monkey_with_none_network(self):
        args = {
            "network": "none",
            "enabled-features": "alauda-monkey",
            "monkey_gateway": "192.168.0.1",
            "kube-pods-subnet": "192.168.0.0/16",
            "monkey_subnet_name": "test",
            "monkey_subnet_range_start": "192.168.1.1",
            "monkey_subnet_range_end": "192.168.1.200"
        }
        f_args = self.format_args(args)
        result = self.run(up, f_args)
        assert result.exit_code == 0

    def test_alauda_monkey_with_none_network_default_cidr(self):
        args = {
            "network": "none",
            "enabled-features": "alauda-monkey",
            "monkey_gateway": "192.168.0.1",
            "monkey_subnet_name": "test",
            "monkey_subnet_range_start": "192.168.1.1",
            "monkey_subnet_range_end": "192.168.1.200"
        }
        f_args = self.format_args(args)
        result = self.run(up, f_args)
        assert result.exit_code == -1
        # default cidr is 10.222.0.0/16
        assert 'must be in cidr range' in result.output

    def test_alauda_calico_network(self):
        args = {
            "network": "alauda-calico",
            "kube-pods-subnet": "192.168.0.0/16",
            "ipip_mode": "Always",
        }
        f_args = self.format_args(args)
        result = self.run(up, f_args)
        assert result.exit_code == 0

    def test_command_man_path_not_exist(self, mocker):
        mocker.patch.object(os.path, "exists")
        os.path.exists.return_value = False
        result = self.run(man, [])
        assert "/usr/share/man/man1 not exist. Create man page fail" in result.output
        assert result.exit_code == 2

    def test_command_man_permission_error(self, mocker):
        mocker.patch.object(shutil, "copy")
        shutil.copy.side_effect = OSError(13, "Permission denied")
        result = self.run(man, [])
        assert "Permission denied" in result.output
        assert result.exit_code == 2

    def test_command_man_other_exception(self, mocker):
        mocker.patch.object(shutil, "copy")
        shutil.copy.side_effect = OSError(21, "Is a directory")
        result = self.run(man, [])
        assert "Some terrible error occur. Please contact us." in result.output
        assert result.exit_code == -1

    def test_command_man(self, mocker):
        mocker.patch.object(shutil, "copy")
        shutil.copy.return_value = None
        mocker.patch.object(os, "chmod")
        os.chmod.return_value = None
        result = self.run(man, [])
        assert result.exit_code == 0

    def test_command_version(self):
        result = self.run(version, [])
        assert result.exit_code == 0
        assert __version__ in result.output.strip()

    def test_parse_extra_arguments(self):
        invalid_args = {
            "a": ["--a=b", "--b", "c", "d=e"],
            "b": ["--a=b", "--b", "--c=e"],
            "c": ["--a=b", "b", "--c", "d=e"],
            "d": ["--a=b", "--b", "c", "--d"],
            "e": {"a": 1, "b": 2}
        }
        for k, v in invalid_args.items():
            result = self.run(up, v)
            assert "Args should start with --" in result.output or \
                   "Please specify value" in result.output

    def test_toposort(self):
        topos = {
            "a": {
                "v1": ["v2", "v3"],
                "v2": ["v3"],
                "v3": []
            },
            "b": {
                "v1": [],
                "v2": []
            },
            "c": {
                "v1": ["v2", "v3"],
                "v2": [],
                "v3": [],
                "v4": []
            }
        }
        result = {
            "a": ["v3", "v2", "v1"],
            "b": ["v2", "v1"],
            "c": ["v3", "v2", "v4", "v1"],
        }
        for k, v in topos.items():
            r = toposortdict(v)
            assert r == result[k]

    def test_topo_circle(self):
        topo = {
            "v1": ["v2", "v3"],
            "v2": ["v3"],
            "v3": ["v4"],
            "v4": ["v2"],
            "v5": []
        }
        try:
            toposortdict(topo)
        except Exception as e:
            assert "Their is a circle in your feature dependency" in str(e)

    def test_command_list_features(self):
        result = self.run(ls, [])
        assert "NAME" in result.output
        assert "VERSION" in result.output
        assert "DEPENDENCIES" in result.output
        assert "proxy" in result.output

    def test_specify_features(self):
        args = {
            "enabled-features": "proxy",
            "proxy_mode": "ipvs"
        }
        f_args = self.format_args(args, True)
        result = self.run(up, f_args)
        assert "'proxy_mode': 'ipvs'" in result.output

    def test_feature_with_dependency(self):
        args = {
            "enabled-features": "alauda-dashboard,alauda-oidc"
        }
        f_args = self.format_args(args, True)
        result = self.run(up, f_args)
        assert result.exit_code == -1
        assert "Please specify feature" in result.output

    def test_enable_all_core_features(self):
        args = {
            "enabled-features": "proxy,helm,alauda-base,alauda-dashboard,alauda-oidc,alauda-ingress,alauda-portal",
            "oidc_issuer_url": "https://alauda.com",
            "oidc_ca_file": "README.md",
            "oidc_ca_key": "README.md",
            "oidc_client_id": "test",
        }
        f_args = self.format_args(args, True)
        result = self.run(up, f_args)
        assert result.exit_code == 0

    def test_not_support_features(self):
        args = {
            "enabled-features": "not_support"
        }
        f_args = self.format_args(args)
        result = self.run(up, f_args)
        assert "is an unsupported features" in result.output

    def test_right_gpu_accelerator_feature(self):
        args = {
            "enabled-features": "gpu-accelerator",
            "masters": "198.16.2.1;198.16.2.2",
            "nodes": "198.16.2.3",
            "gpu-nodes": "198.16.2.3",
            "gpu-driver-download-url": "http://test.com/driver"
        }
        f_args = self.format_args(args)
        result = self.run(up, f_args)
        assert result.exit_code == 0

    def test_right_gpu_accelerator_feature2(self):
        args = {
            "enabled-features": "gpu-accelerator",
            "masters": "198.16.2.1;198.16.2.2(ssh_username=test)",
            "nodes": "198.16.2.3(ssh_username=ubuntu)",
            "gpu-nodes": "198.16.2.3;198.16.2.2",
            "gpu-driver-download-url": "http://test.com/driver"
        }
        f_args = self.format_args(args)
        result = self.run(up, f_args)
        assert result.exit_code == 0

    def test_gpu_accelerator_feature_without_gpu_driver_download_url(self):
        args = {
            "enabled-features": "gpu-accelerator",
            "masters": "198.16.2.1;198.16.2.2",
            "nodes": "198.16.2.3",
            "gpu-nodes": "198.16.2.3",
        }
        f_args = self.format_args(args)
        result = self.run(up, f_args)
        assert "--gpu_driver_download_url option is required by feature gpu-accelerator" in result.output

    def test_gpu_accelerator_feature_without_gpu_nodes(self):
        args = {
            "enabled-features": "gpu-accelerator",
            "masters": "198.16.2.1;198.16.2.2",
            "nodes": "198.16.2.3",
            "gpu-driver-download-url": "http://test.com/driver"
        }
        f_args = self.format_args(args)
        result = self.run(up, f_args)
        assert "--gpu_nodes option is required by feature gpu-accelerator" in result.output

    def test_gpu_accelerator_feature_with_wrong_gpu_nodes(self):
        args = {
            "enabled-features": "gpu-accelerator",
            "masters": "198.16.2.1;198.16.2.2",
            "nodes": "198.16.2.3",
            "gpu-nodes": "198.16.2.4",
            "gpu-driver-download-url": "http://test.com/driver"
        }
        f_args = self.format_args(args)
        result = self.run(up, f_args)
        assert "gpu nodes 198.16.2.4 is not in cluster masters and nodes" in result.output

    def test_gpu_accelerator_feature_with_wrong_gpu_nodes2(self):
        args = {
            "enabled-features": "gpu-accelerator",
            "masters": "198.16.2.1;198.16.2.2(ssh_username=ubuntu)",
            "nodes": "198.16.2.3",
            "gpu-nodes": "198.16.2.4;198.16.2.5;198.16.2.1",
            "gpu-driver-download-url": "http://test.com/driver"
        }
        f_args = self.format_args(args)
        result = self.run(up, f_args)
        assert (
            "gpu nodes 198.16.2.4,198.16.2.5 is not in cluster masters and nodes" in result.output or
            "gpu nodes 198.16.2.5,198.16.2.4 is not in cluster masters and nodes" in result.output
        )

    def test_specift_docker_config(self):
        args = self.get_basic_deploy_args()
        args['dockercfg'] = 'test.json'
        self._generate_dockercfg(args['dockercfg'])
        f_args = self.format_args(args)
        result = self.run(up, f_args)
        assert result.exit_code == 0

    def test_specify_invalid_docker_config(self):
        args = {
            "dockercfg": "./test.json"
        }
        fakejson = {}
        with open('test.json', 'w') as f:
            json.dump(fakejson, f)
            f.write("break json.")
        f_args = self.format_args(args)
        result = self.run(up, f_args)
        assert "is not a valid json file" in result.output
        assert result.exit_code == -1

    def test_specify_not_exists_docker_config(self):
        args = {
            "dockercfg": "not-exist-config"
        }
        f_args = self.format_args(args)
        result = self.run(up, f_args)
        assert "not-exist-config not exist." in result.output
        assert result.exit_code == -1

    def test_exception(self):
        '''
        ***************** WARNING ***************
        This test must be the last test of the whole test case.
        Because it will change the test file: deploy.yml.
        '''
        with open("./ake-playbook/deploy.yml", "w+") as f:
            f.write("    - name: test exception\n")
            f.write("      command: never mind\n")
        result = self.run(up, [])
        assert "Some terrible error occur. Please contact us." in result.output
        assert result.exit_code == -1
