import base64
import yaml
from urllib3.exceptions import MaxRetryError

from kubernetes import client, config
from kubernetes.client import CoreV1Api, VersionApi, V1ConfigMap, V1ObjectMeta
from kubernetes.client.rest import ApiException


# define call apiserver timeout.
TIMEOUT = 5


class KubernetesApiError(Exception):

    def __init__(self, reason):
        self.reason = reason

    def __str__(self):
        return self.reason


class KubernetesUnauthorized(KubernetesApiError):

    def __init__(self):
        self.reason = "Current user's certificate is invalid."


class KubernetesForbidden(KubernetesApiError):

    def __init__(self):
        self.reason = "Current user does not have permission to perform this action."


class KubernetesResNotFound(KubernetesApiError):

    def __init__(self):
        self.reason = "Specified resource not found."


class KubernetesManager(object):
    def __init__(self, config_file: str):
        """Initialize a kubernetes api operator. """
        config.load_kube_config(config_file=config_file)
        self.raw_client = client.ApiClient()
        self.v1_client = client.CoreV1Api()
        self.version_client = client.VersionApi()

    def check_health(self):
        try:
            self._call(self.raw_client, "call_api", "/healthz", "GET")
        except KubernetesApiError as err:
            return (False, err.reason)
        else:
            return (True, None)

    def get_version(self):
        res = self._call(self.version_client, "get_code")
        return res.git_version[1:]

    def get_kubeadm_config(self):
        body = self._call(self.v1_client, "read_namespaced_config_map",
                          "kubeadm-config", "kube-system")
        return body.data

    def update_kubeadm_config(self, configmap: dict):
        self._call(self.v1_client, "replace_namespaced_config_map", "kubeadm-config", "kube-system",
                   V1ConfigMap(metadata=V1ObjectMeta(name="kubeadm-config", namespace="kube-system"),
                               kind="ConfigMap", data=configmap))

    def _call(self, client, func_name, *args, **kwargs):
        kwargs["_request_timeout"] = TIMEOUT
        func = getattr(client, func_name)
        try:
            return func(*args, **kwargs)
        except ApiException as err:
            if err.status == 401:
                raise KubernetesUnauthorized()
            elif err.status == 403:
                raise KubernetesForbidden()
            elif err.status == 404:
                raise KubernetesResNotFound()
            raise KubernetesApiError("{}: {}".format(err.reason, err.body))
        except MaxRetryError:
            msg = "can not connect to kubernetes api server."
            raise KubernetesApiError(msg)
