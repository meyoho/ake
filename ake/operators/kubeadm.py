import yaml


class InitFailed(Exception):

    def __init__(self, reason):
        self.reason = str(reason)

    def __str__(self):
        return f"Init KubeadmOperator failed: {self.reason}"


class ConfigInvalid(Exception):

    def __init__(self, reason):
        self.reason = str(reason)

    def __str__(self):
        return f"Kubeadm Config is invalid: {self.reason}"


def _load_yaml_data(content: str, exc_cls: Exception=InitFailed):
    try:
        data = yaml.load(content)
    except yaml.YAMLError as err:
        raise exc_cls(err)
    return data


class KubeadmOperator:
    init_features = {}
    graduated_features = ["CoreDNS", "Auditing"]
    removed_features = ["StoreCertsInSecrets", "HighAvailability", "SelfHosting", "DynamicKubeletConfig"]

    @staticmethod
    def init_from_configmap(configmap: dict):
        if "MasterConfiguration" in configmap:
            data = _load_yaml_data(configmap["MasterConfiguration"])
        elif "ClusterConfiguration" in configmap:
            data = _load_yaml_data(configmap["ClusterConfiguration"])
        else:
            raise InitFailed(
                "not found either 'MasterConfiguration' or ' ClusterConfiguration' field "
                "inside kubeadm configmap.")
        return KubeadmOperator(data)

    @staticmethod
    def convert_to_configmap(configmap: dict, cfg: dict):
        if "MasterConfiguration" in configmap:
            return {
                **configmap,
                **{"MasterConfiguration": yaml.dump(cfg)}
            }
        elif "ClusterConfiguration" in configmap:
            return {
                **configmap,
                **{"ClusterConfiguration": yaml.dump(cfg)}
            }

    def __init__(self, cluster_cfg: dict):
        self.init_cluster_cfg = cluster_cfg
        self.feature_gates = cluster_cfg.get("featureGates", {})

    def revise_config(self):
        revised = False
        revised_config = self.init_cluster_cfg

        for k, enabled in self.feature_gates.items():
            if enabled and (k in self.graduated_features):
                fgs = self.feature_gates.copy()
                fgs.pop(k)
                revised = True
                revised_config = {**revised_config,
                                  **{"featureGates": fgs}}
                continue
            elif not enabled and (k in self.graduated_features):
                raise ConfigInvalid(f"featureGates '{k}' is graduated, can't be disabled anymore.")
            elif enabled and (k in self.removed_features):
                raise ConfigInvalid(f"featureGates '{k}' is removed.")
            elif not enabled and (k in self.removed_features):
                fgs = self.feature_gates.copy()
                fgs.pop(k)
                revised = True
                revised_config = {**revised_config,
                                  **{"featureGates": fgs}}
                continue
            elif k not in self.init_features:
                raise ConfigInvalid(f"featureGates '{k}' is not supported.")
            elif enabled and self.init_features[k]["deprecated"]:
                raise ConfigInvalid(f"featureGates '{k}' is deprecated.")

        return revised, revised_config