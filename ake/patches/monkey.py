from ansible.plugins.connection import local
from ansible.executor import playbook_executor
from ansible.playbook import included_file
from ansible.plugins.strategy import free

from .connection_local import Connection
from .executor_playbook_executor import PlaybookExecutor
from .included_file import IncludedFile
from .free import StrategyModule


def patch_ansible():
    setattr(local, "Connection", Connection)
    setattr(playbook_executor, "PlaybookExecutor", PlaybookExecutor)
    setattr(included_file, "IncludedFile", IncludedFile)
    setattr(free, "StrategyModule", StrategyModule)
