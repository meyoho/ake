# coding=utf-8
from e2e.test_qcloud.qcloud import create_instance, destroy_instance
import e2e.settings
from e2e.test_common.common_func import assert_successful, deploy_ha_ake, set_vm_passwd, add_nodes_by_passwd, \
    remote_exec, check_cluster, copy_kubectl_config, deploy_haproxy
from e2e.test_common.common_test import CommonTest


class TestAkeDeployHa(CommonTest):
    def setup_class(self):
        create_instance(2)
        self.get_instances(self)
        assert_successful(self.result)
        self.token = e2e.settings.GIT_COMMIT + "-gaokeyong"
        for public_ip in self.public_ips:
            remote_exec(public_ip, e2e.settings.USER, e2e.settings.KEY,
                        '"sudo hostname {}"'.format(public_ip))
            remote_exec(public_ip, e2e.settings.USER, e2e.settings.KEY,
                        '"echo {}|sudo tee /etc/hostname"'.format(public_ip))

    def teardown_class(self):
        for public_ip in self.public_ips:
            remote_exec(public_ip, e2e.settings.USER, e2e.settings.KEY,
                        '"curl http://get.alauda.cn/deploy/ake/cleanup | sudo sh"')
            remote_exec(public_ip, e2e.settings.USER, e2e.settings.KEY,
                        '"sudo apt-get purge -y docker*"')
        destroy_instance()

    def test_deploy_ha(self):
        status, output = deploy_haproxy(self.public_ips, e2e.settings.USER)
        if status != 0:
            result = {"success": False, "message": "deploy haproxy failed:{}".format(output)}
        else:
            result = {"success": True, "message": "deploy haproxy success"}
        assert_successful(result)
        status, output = deploy_ha_ake(self.public_ips[0:-1], e2e.settings.USER, e2e.settings.KEY, self.public_ips[-1])
        if status != 0 or "Install successfully" not in output:
            result = {"success": False, "message": "deploy k8s failed:{}".format(output)}
        else:
            result = {"success": True, "message": "deploy k8s success"}
        print("使用ake deploy部署k8s结果：{}".format(result))
        assert_successful(result)
        copy_ret = copy_kubectl_config(self.public_ips[0], e2e.settings.USER, e2e.settings.KEY)
        assert_successful(copy_ret)
        check_ret = check_cluster()
        assert_successful(check_ret)

    def test_sonobuoy(self):
        self.verify_sonobuoy(self.token)
