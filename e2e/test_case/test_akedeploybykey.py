# coding=utf-8
from e2e.test_qcloud.qcloud import create_instance
import e2e.settings
from e2e.test_common.common_func import assert_successful, deploy_by_key, set_vm_passwd, add_nodes_by_passwd, \
    remote_exec, check_cluster, copy_kubectl_config, set_kubectl_config
from e2e.test_common.common_test import CommonTest
from time import sleep


class TestAkeDeployByKey(CommonTest):
    def setup_class(self):
        create_instance(2)
        self.get_instances(self)
        assert_successful(self.result)
        for public_ip in self.public_ips:
            remote_exec(public_ip, e2e.settings.USER, e2e.settings.KEY,
                        '"sudo hostname {}"'.format(public_ip))
            remote_exec(public_ip, e2e.settings.USER, e2e.settings.KEY,
                        '"echo {}|sudo tee /etc/hostname"'.format(public_ip))

    def teardown_class(self):
        for public_ip in self.public_ips:
            remote_exec(public_ip, e2e.settings.USER, e2e.settings.KEY,
                        '"curl http://get.alauda.cn/deploy/ake/cleanup | sudo sh"')
            remote_exec(public_ip, e2e.settings.USER, e2e.settings.KEY,
                        '"sudo apt-get purge -y docker*"')

    def test_deploy_by_key(self):
        status, output = deploy_by_key(self.public_ips[0], e2e.settings.USER, e2e.settings.KEY)
        if status != 0 or "Install successfully" not in output:
            result = {"success": False, "message": "deploy k8s failed:{}".format(output)}
        else:
            result = {"success": True, "message": "deploy k8s success"}
        print("使用ake deploy部署k8s结果：{}".format(result))
        assert_successful(result)
        copy_ret = copy_kubectl_config(self.public_ips[0], e2e.settings.USER, e2e.settings.KEY)
        assert_successful(copy_ret)
        result = set_kubectl_config(self.public_ips[0], self.private_ips[0])
        assert_successful(result)
        check_ret = check_cluster()
        assert_successful(check_ret)

    def test_reboot(self):
        remote_exec(self.public_ips[0], e2e.settings.USER, e2e.settings.KEY, "sudo reboot")
        sleep(120)
        check_ret = check_cluster()
        assert_successful(check_ret)

    def test_create_pod(self):
        self.verify_pod()

    def test_add_nodes_by_passwd(self):
        if len(self.public_ips) == 2:
            set_vm_passwd(self.public_ips[1], e2e.settings.USER, e2e.settings.KEY)
            status, output = add_nodes_by_passwd(self.public_ips, e2e.settings.USER)
            if status != 0 or "Install successfully" not in output:
                result = {"success": False, "message": "addnodes failed:{}".format(output)}
            else:
                result = {"success": True, "message": "addnodes success"}
            print("使用ake addnodes添加节点结果：{}".format(result))
            assert_successful(result)
            check_ret = check_cluster()
            assert_successful(check_ret)

    def test_verify_add_nodes(self):
        self.verify_daemonset()
