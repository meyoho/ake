# coding=utf-8
from subprocess import getstatusoutput
from time import sleep
import e2e.settings
from e2e.test_common.common_func import assert_successful, add_nodes_by_key, remote_exec, check_cluster, \
    copy_kubectl_config, set_kubectl_config
from e2e.test_common.common_test import CommonTest


def ake_up(public_ip, user, key):
    copy_ake_cmd = "scp -o StrictHostKeyChecking=no -i {} ./ake {}@{}:/home/{}".format(key, user, public_ip, user)
    status, output = getstatusoutput(copy_ake_cmd)
    if status != 0:
        return status, output
    status, output = remote_exec(
        public_ip, user, key, "sudo /home/{}/ake up --alauda-k8s --cert-sans={} --network=flannel "
                              "--network-opt=network_policy=calico --kube-pods-subnet=10.33.0.0/16 "
                              "--registry=index.alauda.cn --kube-service-subnet=192.96.0.0/12 "
                              "--kube-cluster-dns-ip=192.96.0.10 --debug".format(user, public_ip))
    return status, output


class TestAkeUpByPara(CommonTest):
    def setup_class(self):
        self.get_instances(self)
        assert_successful(self.result)

    def teardown_class(self):
        for public_ip in self.public_ips:
            remote_exec(public_ip, e2e.settings.USER, e2e.settings.KEY,
                        '"curl http://get.alauda.cn/deploy/ake/cleanup | sudo sh"')
            remote_exec(public_ip, e2e.settings.USER, e2e.settings.KEY,
                        '"sudo apt-get purge -y docker*"')
            remote_exec(public_ip, e2e.settings.USER, e2e.settings.KEY,
                        '"sudo rm /home/{}/.kube/config;sudo rm /root/.kube/config"'.format(e2e.settings.USER))

    def test_ake_up_by_para(self):
        status, output = ake_up(self.public_ips[0], e2e.settings.USER, e2e.settings.KEY)
        if status != 0 or "Install successfully" not in output:
            result = {"success": False, "message": "deploy k8s failed:{}".format(output)}
        else:
            result = {"success": True, "message": "deploy k8s success"}
        print("使用ake up部署k8s结果：{}".format(result))
        assert_successful(result)
        copy_ret = copy_kubectl_config(self.public_ips[0], e2e.settings.USER, e2e.settings.KEY)
        assert_successful(copy_ret)
        result = set_kubectl_config(self.public_ips[0], self.private_ips[0])
        assert_successful(result)
        check_ret = check_cluster()
        assert_successful(check_ret)

    def test_reboot(self):
        remote_exec(self.public_ips[0], e2e.settings.USER, e2e.settings.KEY, "sudo reboot")
        sleep(120)
        check_ret = check_cluster()
        assert_successful(check_ret)

    def test_verify_flannel(self):
        self.verify_pod()

    def test_add_nodes_bys_key(self):
        if len(self.public_ips) == 2:
            status, output = add_nodes_by_key(self.public_ips, e2e.settings.USER, e2e.settings.KEY, use_sk=True)
            if status != 0 or "Install successfully" not in output:
                result = {"success": False, "message": "addnodes failed:{}".format(output)}
            else:
                result = {"success": True, "message": "addnodes success"}
            print("使用ake addnodes添加节点结果：{}".format(result))
            assert_successful(result)
            check_ret = check_cluster()
            assert_successful(check_ret)

    def test_verify_add_nodes(self):
        self.verify_daemonset()
