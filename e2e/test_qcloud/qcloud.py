from time import sleep, time
from QcloudApi.qcloudapi import QcloudApi
import random
import base64
import hashlib
import hmac
import requests
import e2e.settings

secret_id = e2e.settings.SECRET_ID
secret_key = e2e.settings.SECRET_KEY
endpoint = "cvm.tencentcloudapi.com"
instances_id = []


def get_string_to_sign(method, endpoint, params):
    s = method + endpoint + "/?"
    query_str = "&".join("%s=%s" % (k, params[k]) for k in sorted(params))
    return s + query_str


def sign_str(key, s, method):
    key = str(base64.b64decode(key), encoding="utf-8")
    hmac_str = hmac.new(key.encode("utf8"), s.encode("utf8"), method).digest()
    return base64.b64encode(hmac_str)


def create_instance(num):
    params = {
        "Action": "RunInstances",
        "Placement.Zone": "ap-chongqing-1",
        "Placement.ProjectId": 1126842,
        "Region": "ap-chongqing",
        "ImageId": "img-pyqx34y1",
        "InstanceType": "SA1.LARGE8",
        "InstanceCount": num,
        "SystemDisk.DiskType": "CLOUD_PREMIUM",
        "SystemDisk.DiskSize": 50,
        "DataDisks.0.DiskType": "CLOUD_PREMIUM",
        "DataDisks.0.DiskSize": 50,
        "InstanceName": "AkePublicDeploy-houchao",
        "LoginSettings.KeyIds.0": e2e.settings.KEYID,
        "SecurityGroupIds.0": "sg-f9whhvbu",
        "Version": "2017-03-12",
        "HostName": "node",
        "InternetAccessible.InternetChargeType": "TRAFFIC_POSTPAID_BY_HOUR",
        "InternetAccessible.InternetMaxBandwidthOut": 1,
        "InternetAccessible.PublicIpAssigned": True,
        "TagSpecification.0.ResourceType": "instance",
        "TagSpecification.0.Tags.0.Key": "Group",
        "TagSpecification.0.Tags.0.Value": "QA",
        "Timestamp": int(time()),
        "Nonce": random.randint(0, 1000),
        "SecretId": secret_id
    }
    s = get_string_to_sign("GET", endpoint, params)
    params["Signature"] = sign_str(secret_key, s, hashlib.sha1)
    res = requests.get("https://" + endpoint, params=params)
    global instances_id
    instances_id += res.json()['Response'].get("InstanceIdSet")
    if not instances_id:
        return {"success": False, "message": "create qcloud vm failed: {}".format(res.text)}
    sleep(120)
    return {"success": True, "instances_id": instances_id, "message": "create qcloud vm over"}


def get_instance():
    params = {
        "Action": "DescribeInstances",
        "Version": "2017-03-12",
        "Region": "ap-chongqing",
        "SecretId": secret_id,
        "Timestamp": int(time()),
        "Nonce": random.randint(0, 1000),
    }
    for i in range(0, len(instances_id)):
        params.update({"InstanceIds." + str(i): str(instances_id[i])})
    s = get_string_to_sign("GET", endpoint, params)
    params["Signature"] = sign_str(secret_key, s, hashlib.sha1)
    res = requests.get("https://" + endpoint, params=params)
    if "InstanceSet" in res.json()['Response']:
        private_ips = []
        public_ips = []
        instances_info = res.json()['Response']['InstanceSet']
        for instance_info in instances_info:
            private_ips.append(str(instance_info['PrivateIpAddresses'][0]))
            public_ips.append(str(instance_info['PublicIpAddresses'][0]))
        return {"success": True, "private_ips": private_ips, "public_ips": public_ips,
                "message": "describe qcloud vm over"}
    return {"success": False, "message": "get qcloud vm info failed:{}".format(res.text)}


def destroy_instance():
    params = {
        "Action": "TerminateInstances",
        "Version": "2017-03-12",
        "Region": "ap-chongqing",
        "SecretId": secret_id,
        "Timestamp": int(time()),
        "Nonce": random.randint(0, 1000),
    }
    for i in range(0, len(instances_id)):
        params.update({"InstanceIds." + str(i): str(instances_id[i])})
    s = get_string_to_sign("GET", endpoint, params)
    params["Signature"] = sign_str(secret_key, s, hashlib.sha1)
    res = requests.get("https://" + endpoint, params=params)
    if "TaskId" not in res.json()['Response']:
        return {"success": False, "message": "delete qcloud vm failed,please check it"}
    return {"success": True, "message": "delete qcloud vm over"}
