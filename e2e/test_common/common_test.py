from e2e.test_qcloud.qcloud import get_instance
from e2e.test_common.common_func import make_file, kubectl_create, assert_successful, monitor_sonobuoy, \
    kubectl_delete, delete_file, is_running, get_pod_num
import e2e.settings


class CommonTest(object):
    def get_instances(self):
        self.pod_data = {"podName": "e2etestcreatepod", "deamonsetName": "e2etestdaemonset"}
        self.result = get_instance()
        if self.result["success"]:
            self.public_ips = self.result["public_ips"]
            self.private_ips = self.result["private_ips"]
        else:
            self.result = {"success": False, "message": "create qcloud vm failed:{}".format(self.result["message"])}

    def verify_sonobuoy(self, token):
        if e2e.settings.GIT_BRANCH == "release":
            file = "test_data/sonobuoy.yaml"
            file = make_file(file, {"%TOKEN%": token, "%CLOUD_URL%": e2e.settings.SONOBUOY_URL})
            status, output_create_pod = kubectl_create(file)
            if status != 0:
                result = {"success": False, "message": "部署sonobuoy失败：{}".format(output_create_pod)}
                kubectl_delete(file)
            else:
                result = {"success": True, "message": "部署sonobuoy成功"}
            assert_successful(result)
            result = monitor_sonobuoy(token)
            assert_successful(result)
            kubectl_delete(file)
            delete_file(file)

    def verify_pod(self):
        file = "test_data/pods/create_pod.yaml"
        file = make_file(file, self.pod_data)
        # 创建pod
        status, output_create_pod = kubectl_create(file)
        expect_string = "pod \"{}\" created".format("e2etestcreatepod")
        if expect_string != output_create_pod or status != 0:
            result = {"success": False, "message": "部署pod失败：{}".format(output_create_pod)}
            kubectl_delete(file)
        else:
            result = {"success": True, "message": "本地部署pod成功"}
        assert_successful(result)
        result = is_running(self.public_ips[0], self.pod_data["podName"], is_access=True)
        if not result['success']:
            kubectl_delete(file)
        assert_successful(result)
        status, output_delete_pod = kubectl_delete(file)
        expect_string = "pod \"{}\" deleted".format("e2etestcreatepod")
        if expect_string != output_delete_pod or status != 0:
            result = {"success": False, "message": "部署pod失败：{}".format(output_delete_pod)}
            kubectl_delete(file)
        else:
            result = {"success": True, "message": "本地部署pod成功"}
        assert_successful(result)
        delete_file(file)

    def verify_daemonset(self, num=2):
        file = "test_data/pods/daemonset.yaml"
        file = make_file(file, self.pod_data)
        status, output_create_pod = kubectl_create(file)
        expect_string = "\"{}\" created".format("e2etestdaemonset")
        if output_create_pod.find(expect_string) < 0 or status != 0:
            result = {"success": False, "message": "部署pod失败：{}".format(output_create_pod)}
            kubectl_delete(file)
        else:
            result = {"success": True, "message": "本地部署pod成功"}
        assert_successful(result)
        result = is_running(self.public_ips[0], self.pod_data["deamonsetName"])
        if not result['success']:
            kubectl_delete(file)
        status, output = get_pod_num(self.pod_data["deamonsetName"])
        if output != num:
            assert_successful(
                {"success": False, "message": "daemonset should deploy {} pod,now is {}".format(num, output)})
            kubectl_delete(file)
        assert_successful(result)
        kubectl_delete(file)
        delete_file(file)
