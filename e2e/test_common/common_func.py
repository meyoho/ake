# coding=utf-8
import json
import os
from subprocess import getstatusoutput
from time import sleep
import e2e.settings
import requests


def assert_successful(response):
    msg = response.get('message', json.dumps(response))
    assert response["success"], msg


def kubectl_create(file):
    status, output = getstatusoutput("kubectl create -f {}".format(file))
    return status, output


def kubectl_delete(file):
    status, output = getstatusoutput("kubectl delete -f {}".format(file))
    return status, output


def set_kubectl_config(public_ip, private_ip):
    replace_cmd = "sed -i \"s/{}/{}/\" {}/.kube/config".format(private_ip, public_ip, os.getenv("HOME"))
    status, output = getstatusoutput(replace_cmd)
    if status != 0:
        return {"success": False, "message": "修改本地配置文件失败：{}".format(output)}
    return {"success": True, "message": "设置配置成功"}


def copy_kubectl_config(public_ip, user, key):
    change_mode_cmd = "ssh -i {} {}@{} \"sudo chmod 644 /etc/kubernetes/admin.conf\"".format(key, user, public_ip)
    status, output = getstatusoutput(change_mode_cmd)
    if status != 0:
        return {"success": False, "message": "修改远程的Kubernetes config权限失败：{}".format(output)}
    getstatusoutput("mkdir {}/.kube".format(os.getenv("HOME")))
    copy_config_cmd = "scp -i {} {}@{}:/etc/kubernetes/admin.conf {}/.kube/config".format(
        key, user, public_ip, os.getenv("HOME"))
    status, output = getstatusoutput(copy_config_cmd)
    if status != 0:
        return {"success": False, "message": "拷贝远程的k8s配置文件失败：{}".format(output)}
    return {"success": True, "message": "拷贝配置成功"}


def make_file(file, data):
    with open(file, "r") as f:
        content = f.read()
    for key in data:
        content = content.replace(key, data[key])
    filename = file.split("/")[-1]
    if not os.path.exists("data"):
        os.mkdir("data")
    to_file = "data/" + filename
    with open(to_file, "w") as f:
        f.write(content)
    return to_file


def delete_file(file):
    if os.path.exists(file):
        os.remove(file)


def remote_exec(public_ip, user, key, cmd):
    commands = 'ssh -o StrictHostKeyChecking=no -i {} {}@{} {}'.format(key, user, public_ip, cmd)
    print("excute cmd :{}".format(commands))
    status, output = getstatusoutput(commands)
    return status, output


def get_pod(public_ip, user, key, podip):
    cmd = "curl --connect-timeout 2 {}".format(podip)
    status, output = remote_exec(public_ip, user, key, cmd)
    return output


def is_running(public_ip, podname, is_access=False):
    cnt = 0
    flag = False
    message = "pod deploy failed"
    while cnt < 100 and not flag:
        pod_num = 0
        cnt += 1
        status, output = getstatusoutput("kubectl get pod -n default -o json")
        if status != 0:
            return {"success": False, "message": "kubectl get pod failed: {}".format(output)}
        contents = json.loads(output)["items"]
        for content in contents:
            name = content['metadata'].get("name", "")
            status = content['status'].get("phase", "")
            pod_ip = content["status"].get("podIP", "")
            if name.find(podname) < 0:
                sleep(2)
                message = "get pod name failed"
                continue
            if status != 'Running':
                sleep(2)
                message = "deploy timeout"
                continue
            if is_access and "Hello World" not in get_pod(public_ip, e2e.settings.USER, e2e.settings.KEY, pod_ip):
                sleep(3)
                message = "access pod failed"
                continue
            pod_num += 1
            if pod_num == len(contents):
                flag = True
                message = "deploy success"
                break
    return {"success": flag, "message": message}


def check_cluster():
    cnt = 0
    flag = False
    message = "kubernetes pods deploy failed"
    while cnt < 100 and not flag:
        cnt += 1
        status, output = getstatusoutput("kubectl get pod --all-namespaces -o json")
        if status != 0:
            return {"success": False, "message": "kubectl get pod failed: {}".format(output)}
        contents = json.loads(output)["items"]
        for content in contents:
            name = content['metadata'].get("name", "")
            status = content['status'].get("phase", "")
            if status != 'Running':
                flag = False
                sleep(5)
                message = "kubernetes pod: {} deploy timeout".format(name)
                break
            flag = True
            message = "kubernetes pods deploy success"
    return {"success": flag, "message": message}


def get_pod_num(podname):
    status, output = getstatusoutput("kubectl get pod -n default |grep {}|wc -l".format(podname))
    return status, int(output)


def add_nodes_by_key(public_ips, user, key, use_sk=False):
    print("Start to add node to cluster")
    if use_sk:
        cmd = "./ake addnodes --nodes={} --apiserver={}:6443 " \
              "--token={} --ssh-username={} --ssh-key-path={} --ssh-port=22 --debug".format(public_ips[1],
                                                                                            public_ips[0],
                                                                                            e2e.settings.TOKEN, user,
                                                                                            key)
    else:
        cmd = "./ake addnodes --nodes={}\"(user={},port=22,ssh_private_key_file={})\" " \
              "--apiserver={}:6443 --token={} --debug".format(public_ips[1], user, key, public_ips[0],
                                                              e2e.settings.TOKEN)
    status, output = getstatusoutput(cmd)
    return status, output


def add_nodes_by_passwd(public_ips, user):
    print("Start to add node to cluster")
    cmd = "./ake addnodes --nodes={} --apiserver={}:6443 --token={} -su={} --ssh-password={} --debug".format(
        public_ips[1], public_ips[0], e2e.settings.TOKEN, user, e2e.settings.SSH_PASSWORD)
    print("excute cmd :{}".format(cmd))
    status, output = getstatusoutput(cmd)
    return status, output


def set_vm_passwd(public_ip, user, key):
    cmd = "\"sudo sed -ri 's/^#?(PasswordAuthentication)\s+(yes|no)/\\1 yes/' /etc/ssh/sshd_config\""
    remote_exec(public_ip, user, key, cmd)
    getstatusoutput("echo {}:{} > pass.log".format(user, e2e.settings.SSH_PASSWORD))
    remote_exec(public_ip, user, key, "sudo chpasswd < pass.log")
    remote_exec(public_ip, user, key, "sudo service sshd restart")


def deploy_haproxy(ips, user):
    if len(ips) != 4:
        return -1, "vm num shoud be 4,but is {}".format(len(ips))
    data = {"$master_ip1": ips[0], "$master_ip2": ips[1], "$master_ip3": ips[2]}
    file = make_file('./haproxy.cfg', data)
    copy_haproxy_cmd = "scp -o StrictHostKeyChecking=no -i {} {} {}@{}:/home/{}".format(e2e.settings.KEY, file, user,
                                                                                        ips[-1], user)
    status, output = getstatusoutput(copy_haproxy_cmd)
    if status != 0:
        return status, output
    cmd = '"sudo apt-get update;sudo apt-get install -y docker-ce*;sudo docker run -d --net host --name controller -v' \
          ' /home/{}/haproxy.cfg:/etc/haproxy/haproxy.cfg index.alauda.cn/alaudaorg/qaimages:haproxy"'.format(user)
    status, output = remote_exec(ips[-1], user, e2e.settings.KEY, cmd)
    return status, output


def generate_ca_file(lb_addr):
    status, output = getstatusoutput("bash generate_ca.sh {}".format(lb_addr))
    if status == 0:
        return ["ssl/cert.pem", "ssl/key.pem"]
    else:
        return []


def deploy_ha_ake(ips, user, key, lb_addr):
    if len(ips) < 3:
        return -1, "vm is less than 3"
    print("Start to deploy high available ake by key at {}".format(ips))
    ca_files = generate_ca_file(lb_addr)
    if not ca_files:
        return -1, "ca generate failed"
    nodes = ""
    for ip in ips:
        if nodes:
            nodes += ";"
        nodes += "{}(user={},ssh_private_key_file={})".format(ip, user, key)
    network_cmd = "--network-opt=backend_type=vxlan --kube-pods-subnet=10.33.0.0/16 " \
                  "--kube_controlplane_endpoint={} --oidc_issuer_url=https://{}/dex".format(lb_addr, lb_addr)
    enable_features = "helm,alauda-ingress,alauda-portal,alauda-dashboard,alauda-devops,alauda-oidc,alauda-base," \
                      "alauda-appcore,aml-core"
    deploy_cmd = "./ake deploy --masters=\"{}\" --etcds=\"{}\" --nodes=\"{}\"" \
                 " {} --enabled-features={} --oidc_client_id=alauda-auth --oidc_ca_file={} --oidc_ca_key={} " \
                 "--debug".format(nodes, nodes, nodes,
                                  network_cmd,
                                  enable_features,
                                  ca_files[0],
                                  ca_files[1])
    print("excute cmd :{}".format(deploy_cmd))
    status, output = getstatusoutput(deploy_cmd)
    return status, output


def deploy_by_key(ip, user, key, use_sk=False):
    print("Start to deploy ake by key at {}".format(ip))
    if use_sk:
        deploy_node_cmd = "{} -su={} -sk={} -p 22".format(ip, user, key)
    else:
        deploy_node_cmd = "\"{}(user={},port=22,ssh_private_key_file={})\"".format(ip, user, key)
    network_cmd = "--network=alauda-calico --network-opt=backend_type=bird --kube-pods-subnet=10.33.0.0/16" \
                  " --kube-service-subnet=192.96.0.0/12 --kube-cluster-dns-ip=192.96.0.10"
    deploy_cmd = "./ake deploy --cert-sans={} --masters={} {} --nodes={} " \
                 "--enabled-features=proxy,helm,alauda-base,alauda-ingress,alauda-portal,alauda-dashboard " \
                 "--proxy_mode=ipvs --debug".format(ip, deploy_node_cmd, network_cmd, deploy_node_cmd)
    print("excute cmd :{}".format(deploy_cmd))
    status, output = getstatusoutput(deploy_cmd)

    return status, output


def deploy_by_passwd(ip, user):
    print("Start to deploy ake by password at {}".format(ip))
    deploy_node_cmd = "\"{}(user={},ssh_pass={})\"".format(ip, user, e2e.settings.SSH_PASSWORD)
    deploy_cmd = "./ake deploy --masters={} --cert-sans={} --network-opt=backend_type=vxlan" \
                 " --kube-pods-subnet=10.33.0.0/16 -kv v1.15.3 --nodes={} --enabled-features=proxy,helm,audit" \
                 " --proxy_mode=iptables --debug".format(deploy_node_cmd, ip, deploy_node_cmd)
    print("excute cmd :{}".format(deploy_cmd))
    status, output = getstatusoutput(deploy_cmd)

    return status, output


# macvlan现在有bug 暂时不部署macvlan集群
def deploy_macvlan(ip, user):
    print("Start to deploy macvlan by password at {}".format(ip))
    deploy_node_cmd = "\"{}(user={},ssh_pass={})\"".format(ip, user, e2e.settings.SSH_PASSWORD)
    deploy_cmd = "./ake deploy --masters={} --cert-sans={} --network=none" \
                 " --kube-pods-subnet=172.30.0.0/20 --nodes={} --enabled-features=alauda-monkey " \
                 "--monkey_gateway=172.30.0.1 --monkey_subnet_name=mymonkey --monkey_subnet_range_start=172.30.15.1 " \
                 "--monkey_subnet_range_end=172.30.15.25 --monkey_driver=empty --monkey_network_type=macvlan " \
                 "--proxy_mode=iptables".format(deploy_node_cmd, ip, deploy_node_cmd)
    print("excute cmd :{}".format(deploy_cmd))
    status, output = getstatusoutput(deploy_cmd)

    return status, output


def join_nodes(public_ips, user):
    copy_ake_cmd = "scp -o StrictHostKeyChecking=no -i {} ./ake {}@{}:/home/{}".format(e2e.settings.KEY, user,
                                                                                       public_ips[1], user)
    status, output = getstatusoutput(copy_ake_cmd)
    if status != 0:
        return status, output
    status, output = remote_exec(public_ips[1], user, e2e.settings.KEY,
                                 "sudo /home/{}/ake join --apiserver={}:6443 --token={} --debug".format(
                                     user, public_ips[0], e2e.settings.TOKEN))
    return status, output


def get_status(token):
    headers = {"Authorization": "Token {}".format(token)}
    r = requests.get("{}/status".format(e2e.settings.SONOBUOY_URL), headers=headers)
    return json.loads(r.text)


def is_success(data):
    if "report" in data:
        if "success" in data["report"]:
            if "aketest" in data["report"]["success"]:
                return data["report"]["success"]["aketest"]
    return False


def monitor_sonobuoy(token):
    cnt = 0
    while cnt <= 100:
        data = None
        try:
            data = get_status(token)
        except Exception as ex:
            print("get sonobouy log error:{}".format(ex))
        if data is not None:
            if data["status"] == "completed":
                print("completed")
                return {"success": is_success(data), "message": "Test Failed{}".format(data["report"]["tests"])}
        sleep(60)
    return {"success": False, "message": "get plugin test result timeout"}


def verify_version(expect_version):
    status, output = getstatusoutput("kubectl version|awk '{print $5}'")
    print("kubernetes version is {}".format(output))
    if status != 0:
        return {"success": False, "message": "kubernetes version is {}".format(output)}
    if expect_version not in output:
        return {"success": False, "message": "kubernetes version is {}".format(output)}
    return {"success": True, "message": "verify verison success"}


def upgrade_by_passwd(ip, user):
    print("Start to upgrade by password at {}".format(ip))
    deploy_node_cmd = "\"{}(user={},ssh_pass={})\"".format(ip, user, e2e.settings.SSH_PASSWORD)
    deploy_cmd = "./ake upgrade --masters={} --kubernetes-version v1.15.3 --nodes={}" \
                 " --debug".format(deploy_node_cmd, ip, deploy_node_cmd)
    print("excute cmd :{}".format(deploy_cmd))
    status, output = getstatusoutput(deploy_cmd)

    return status, output
