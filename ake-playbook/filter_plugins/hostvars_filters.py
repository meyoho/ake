from datetime import timedelta

from dateutil.parser import parse


class FilterModule(object):

    def filters(self):
        return {
            'assert_no_duplicated_hostname': self.assert_no_duplicated_hostname,
            'assert_time_in_sync': self.assert_time_in_sync,
            'build_etcd_access_address': self.build_etcd_access_address,
        }

    def assert_no_duplicated_hostname(self, hostvars):
        assert str(type(hostvars)) == "<class 'ansible.vars.hostvars.HostVars'>"

        hostnames = []
        for h in hostvars:
            if hostvars[h]["ansible_nodename"].lower() not in hostnames:
                hostnames.append(hostvars[h]["ansible_nodename"].lower())
            else:
                return False
        return True

    def assert_time_in_sync(self, hostvars):
        assert str(type(hostvars)) == "<class 'ansible.vars.hostvars.HostVars'>"

        node_time_lst = []
        node_tz_lst = []
        for h in hostvars:
            node_time_lst.append(parse(hostvars[h]["ansible_date_time"]["iso8601"]))
            node_tz_lst.append(hostvars[h]["ansible_date_time"]["tz"])

        if len(set(node_tz_lst)) != 1:
            return False

        node_time_lst = sorted(node_time_lst)
        if node_time_lst[-1] - node_time_lst[0] > timedelta(minutes=1):
            return False

        return True

    def build_etcd_access_address(self, hostvars, etcd_group, etcd_listen_client_port, escape_comma=False):
        joiner = "," if not escape_comma else "\\,"
        return joiner.join([
            "https://{}:{}".format(hostvars[item]["ansible_default_ipv4"]["address"], etcd_listen_client_port)
            for item in etcd_group
        ])