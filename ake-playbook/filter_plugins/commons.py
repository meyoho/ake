from semantic_version import Version
from ipaddress import IPv4Network


def _format_version(ver):
    return ".".join([
        v.lstrip("0") if len(v) > 1 else v
        for v in ver.strip().strip("v").strip("-ce").split(".")
    ])


def is_version_supported(ver, supported_versions):
    try:
        target_ver = Version(_format_version(ver), partial=True)
    except ValueError:
        return False
    supported_vers = [
        Version(_format_version(ver), partial=True)
        for ver in supported_versions
    ]
    for ver in supported_vers:
        if target_ver == ver:
            return True
    else:
        return False


def cidr_nth_ipaddress(cidr, nth):
    try:
        cidr = IPv4Network(cidr)
    except ValueError as e:
        raise e
    return str(cidr.network_address + nth)


class FilterModule(object):

    def filters(self):
        return {
            'is_version_supported': is_version_supported,
            'cidr_nth_ipaddress': cidr_nth_ipaddress,
        }
