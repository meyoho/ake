#!/usr/bin/python
# (c) 2018, YongSong You <ysyou@alauda.io>

from __future__ import absolute_import, division, print_function
import os
import tempfile
import shutil
__metaclass__ = type


ANSIBLE_METADATA = {'metadata_version': '1.0',
                    'status': ['preview'],
                    'supported_by': 'alauda'}

DOCUMENTATION = '''
---
module: helm
short_description: Manages Kubernetes packages with the Helm package manager
author: "YongSong You"
description:
   - Install, upgrade, delete and list packages with the Helm package manager
options:
  namespace:
    description:
      - Kubernetes namespace where the chart should be installed
    default: "default"
  name:
    description:
      - Release name to manage
  state:
    description:
      - Whether to install C(present), remove C(absent), or purge C(purged) a package.
    choices: ['absent', 'purged', 'present']
    default: "present"
  chart:
    description: |
      A map describing the chart to install. there are three types of chart: repo, local and url.
      The default type of chart is repo. For example:
      chart:
        type: repo
        name: memcached
        version: 0.4.0
        location: https://kubernetes-charts.storage.googleapis.com
    default: {}
  values:
    description:
      - A map of value options for the chart.
    default: {}
  values_file:
    description: |
      A map describing values_file options for the chart. For example:
      values_file:
        content: |
          object:
            sub_object: foo
          object2: bar
  disable_hooks:
    description:
      - Whether to disable hooks during the uninstall process
    type: bool
    default: 'no'
'''

RETURN = ''' # '''

EXAMPLES = '''
- name: Install helm chart
  helm:
    chart:
      type: repo
      name: memcached
      version: 0.4.0
      location: https://kubernetes-charts.storage.googleapis.com
    state: present
    name: my-memcached
    namespace: default
    values_file:
      content: |
        imagePullPolicy: Always
        memcached:
          verbosity: vvv
          maxItemMemory: 128
        
- name: Install helm chart from url
  helm:
    chart:
      type: url
      location: https://kubernetes-charts.storage.googleapis.com/memcached-0.4.0.tgz
    state: present
    name: my-memcached
    namespace: default

- name: Install helm chart from local path
  helm:
    chart:
      type: local
      location: /var/charts/memcached-0.4.0.tgz
    state: present
    name: my-memcached
    namespace: default

- name: Uninstall helm chart
  helm:
    state: absent
    name: my-memcached
'''

CHART_TYPES = ["repo", "local", "url"]
DEFAULT_CHART_TYPE = "repo"


class Helm():

    def __init__(self, module):
        self.module = module
        self.helm_path = module.get_bin_path("helm", required=True)

    def update_repo(self):
        status, stdout, stderr = self.module.run_command([self.helm_path, "repo", "update"])
        if status != 0:
            msg = "Failed to update helm repo: {}".format(stderr)
            self.module.fail_json(msg=msg)

    def list_releases(self):
        status, stdout, stderr = self.module.run_command([self.helm_path, "ls"])
        if status != 0:
            msg = "Failed to list helm releases: {}".format(stderr)
            self.module.fail_json(msg=msg)
        releases = []
        for line in stdout.split("\n")[1:]:
            if not line.strip():
                continue
            fields = line.split("\t")
            releases.append({
                "name": fields[0].strip(),
                "revision": int(fields[1].strip()),
                "updated": fields[2].strip(),
                "status": fields[3].strip(),
                "chart": fields[4].strip(),
                "version": fields[4].strip().split("-")[-1],
                "namespace": fields[5].strip(),
            })
        return releases

    def install_release(self, chart, namespace=None, name=None, values=None, values_file=None):
        # need to update repo first before install release from chart repo
        self.update_repo()

        install_options = []
        tempdir = None
        if namespace:
            install_options += ["--namespace", namespace]
        if name:
            install_options += ["--name", name]
        if values:
            for k, v in values.items():
                install_options += ["--set", "{}={}".format(k, v)]
        if values_file:
            vf_options, tempdir = self._build_values_file_options(values_file)
            install_options += vf_options
        install_options += self._build_chart_options(chart)

        command_str = " ".join([self.helm_path, "install"] + install_options)
        status, stdout, stderr = self.module.run_command(
            [self.helm_path, "install"] + install_options
        )
        if tempdir and os.path.exists(tempdir):
            shutil.rmtree(tempdir)
        if status != 0:
            msg="helm package install failed: {}.\ninstall_command: {}".format(stderr, command_str)
            self.module.fail_json(msg=msg)

    def upgrade_release(self, chart, name, namespace=None, values=None, values_file=None):
        # need to update repo first before upgrade release from chart repo
        self.update_repo()

        upgrade_options = []
        tempdir = None
        if namespace:
            upgrade_options += ["--namespace", namespace]
        if values:
            for k, v in values.items():
                upgrade_options += ["--set", "{}={}".format(k, v)]
        if values_file:
            vf_options, tempdir = self._build_values_file_options(values_file)
            upgrade_options += vf_options
        upgrade_options += self._build_chart_options(chart)
        upgrade_options.insert(-1, name)

        status, stdout, stderr = self.module.run_command(
            [self.helm_path, "upgrade"] + upgrade_options
        )
        if tempdir and os.path.exists(tempdir):
            shutil.rmtree(tempdir)
        if status != 0:
            self.module.fail_json(msg="helm package upgrade failed: {}".format(stderr))

    def delete_release(self, name, purge=False):
        delete_options = []
        if purge:
            delete_options += ["--purge"]
        delete_options += [name]

        status, stdout, stderr = self.module.run_command(
            [self.helm_path, "delete"] + delete_options
        )
        if (status != 0) and ("not found" not in stdout):
            self.module.fail_json(msg="helm package delete failed: {}".format(stderr))

    def _build_values_file_options(self, values_file):
        tempdir = tempfile.mkdtemp()
        options = []
        if "content" in values_file:
            tf = os.path.join(tempdir, "values.yaml")
            with open(tf, "w") as f:
                f.write(values_file["content"])
            options += ["-f", tf]
        return options, tempdir

    def _build_chart_options(self, chart):
        chart_type = chart.get("type", DEFAULT_CHART_TYPE)
        chart_options = []
        if chart_type == "repo":
            if not chart.get("name"):
                self.module.fail_json(msg="helm chart name not specified.")
            if chart.get("username") and chart.get("password"):
                chart_options += ["--username", chart["username"], "--password", chart["password"]]
            if chart.get("location"):
                chart_options += ["--repo", chart["location"]]
            if chart.get("version"):
                chart_options += ["--version", chart["version"]]
            chart_options.append(chart["name"])
        else:
            if chart.get("location"):
                chart_options += [chart["location"]]
            else:
                pass  # use offical chart repo
        return chart_options

def install(module, helm):
    changed = False
    params = module.params
    name = params['name']
    values = params['values']
    values_file = params["values_file"]
    chart = module.params['chart']
    namespace = module.params['namespace']

    r_matches = (x for x in helm.list_releases()
                 if x["name"] == name and x["namespace"] == namespace)
    installed_release = next(r_matches, None)
    if installed_release:
        if chart.get("version") and (installed_release["version"] != chart["version"]):
            helm.upgrade_release(chart, name, namespace=namespace, values=values, values_file=values_file)
            changed = True
    else:
        helm.install_release(chart, namespace=namespace, name=name,
                             values=values, values_file=values_file)
        changed = True

    return dict(changed=changed)


def delete(module, helm, purge=False):
    changed = False
    params = module.params

    if not module.params['name']:
        module.fail_json(msg='Missing required field name')

    name = module.params['name']
    disable_hooks = params['disable_hooks']

    helm.delete_release(name, purge)
    changed = True

    return dict(changed=changed)


def main():
    """The main function."""
    module = AnsibleModule(
        argument_spec=dict(
            name=dict(type='str', default=''),
            chart=dict(type='dict'),
            state=dict(
                choices=['absent', 'purged', 'present'],
                default='present'
            ),
            # Install options
            values=dict(type='dict'),
            values_file=dict(type='dict', default={}),
            namespace=dict(type='str', default='default'),

            # Uninstall options
            disable_hooks=dict(type='bool', default=False),
        ),
        supports_check_mode=True)

    state = module.params['state']
    helm = Helm(module)

    if state == 'present':
        rst = install(module, helm)

    if state in 'absent':
        rst = delete(module, helm)

    if state in 'purged':
        rst = delete(module, helm, True)

    module.exit_json(**rst)


from ansible.module_utils.basic import *  # flake8: noqa
main()