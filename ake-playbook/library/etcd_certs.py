#!/usr/bin/python
# (c) 2018, YongSong You <youyongsong@gmail.com>

from __future__ import absolute_import, division, print_function
import os

__metaclass__ = type


ANSIBLE_METADATA = {'metadata_version': '1.0',
                    'status': ['preview'],
                    'supported_by': 'ysyou'}

ETCD_PKI_DIR = "/etc/kubernetes/pki/etcd"


def _read_cert_pair(cert_name):
    cert_key_path = "{}/{}.key".format(ETCD_PKI_DIR, cert_name)
    cert_path = "{}/{}.crt".format(ETCD_PKI_DIR, cert_name)
    with open(cert_key_path, "r") as f:
        key = f.read()
    with open(cert_path, "r") as f:
        crt = f.read()
    return key, crt

def _write_cert_pair(cert_name, cert_pair):
    cert_key_path = "{}/{}.key".format(ETCD_PKI_DIR, cert_name)
    cert_path = "{}/{}.crt".format(ETCD_PKI_DIR, cert_name)
    with open(cert_key_path, "w") as f:
        f.write(cert_pair[0])
    with open(cert_path, "w") as f:
        f.write(cert_pair[1])

def _remove_cert_pair(cert_name):
    cert_key_path = "{}/{}.key".format(ETCD_PKI_DIR, cert_name)
    cert_path = "{}/{}.crt".format(ETCD_PKI_DIR, cert_name)
    if os.path.exists(cert_key_path):
        os.remove(cert_key_path)
    if os.path.exists(cert_path):
        os.remove(cert_path)

def overwrite_certs(module, certs, etcdcfg_path):
    kubeadm_path = module.get_bin_path("kubeadm", required=True)
    for cert in certs:
        key, crt = _read_cert_pair(cert)
        _remove_cert_pair(cert)
        status, stdout, stderr = module.run_command([
            kubeadm_path, "init", "phase", "certs", "etcd-{}".format(cert), "--config", etcdcfg_path
        ])
        if status != 0:
            _write_cert_pair(key, crt)
            module.fail_json(msg="Failed to create etcd-{} cert: {}".format(cert, stderr))
    return dict(changed=True)


def main():
    module = AnsibleModule(
        argument_spec=dict(
            status=dict(choices=["overwrite"]),
            certs=dict(type="list"),
            etcdcfg_path=dict(type="str")
        ),
        supports_check_mode=True)

    status = module.params["status"]
    certs = module.params["certs"]
    etcdcfg_path = module.params["etcdcfg_path"]
    if not os.path.exists(etcdcfg_path):
        module.fail_json(msg="path {} not found.".format(etcdcfg_path))

    if status == "overwrite":
        rst = overwrite_certs(module, certs, etcdcfg_path)
    else:
        module.exit_json(changed=False)

    module.exit_json(**rst)


from ansible.module_utils.basic import *  # flake8: noqa
main()