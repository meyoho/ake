#!/usr/bin/python
# (c) 2018, YongSong You <youyongsong@gmail.com>

from __future__ import absolute_import, division, print_function
import os

__metaclass__ = type


ANSIBLE_METADATA = {'metadata_version': '1.0',
                    'status': ['preview'],
                    'supported_by': 'ysyou'}

ETCD_PKI_DIR = "/etc/kubernetes/pki/etcd"
ETCD_CA_CERT_PATH = "{}/ca.crt".format(ETCD_PKI_DIR)
ETCD_CA_KEY_PATH = "{}/ca.key".format(ETCD_PKI_DIR)


def write_ca_pair(module, content):
    if not os.path.exists(ETCD_PKI_DIR):
        os.makedirs(ETCD_PKI_DIR)
    if "cert" in content:
        with open(ETCD_CA_CERT_PATH, "w") as f:
            f.write(content["cert"])
    if "key" in content:
        with open(ETCD_CA_KEY_PATH, "w") as f:
            f.write(content["key"])

    return dict(changed=True)


def generate_ca_pair(module):
    for p in (ETCD_CA_KEY_PATH, ETCD_CA_CERT_PATH):
        if os.path.exists(p):
            os.remove(p)

    kubeadm_path = module.get_bin_path("kubeadm", required=True)
    status, stdout, stderr = module.run_command([
        "kubeadm", "init", "phase", "certs", "etcd-ca", "--config", "/etc/kubeadm/etcdcfg.yaml"
    ])
    if status != 0:
        msg = "Failed to generate etcd ca pair: {}".format(stderr)
        module.fail_json(msg=msg)

    ca_cert, ca_key = "", ""
    with open(ETCD_CA_KEY_PATH, "r") as f:
        ca_key = f.read()
    with open(ETCD_CA_CERT_PATH, "r") as f:
        ca_cert = f.read()

    return {
        "changed": True,
        "ansible_facts": {
            "etcd_ca_key": ca_key,
            "etcd_ca_cert": ca_cert
        }
    }
    

def main():
    module = AnsibleModule(
        argument_spec=dict(
            content=dict(type='dict', default={})
        ),
        supports_check_mode=True)

    content = module.params['content']
    if content:
        rst = write_ca_pair(module, content)
    else:
        rst = generate_ca_pair(module)

    module.exit_json(**rst)


from ansible.module_utils.basic import *  # flake8: noqa
main()
