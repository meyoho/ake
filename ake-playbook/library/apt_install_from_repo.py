import os
import re
import shutil
import contextlib

APT_SOURCE = ['/etc/apt/sources.list',
              '/etc/apt/sources.list.d']

def backup_file(file_path):
    backup_path = []
    for path in file_path:
        if os.path.exists(path):
            shutil.move(path, path + '.bak')
            backup_path.append(path + '.bak')
    return backup_path

def restore_file(backup_path):
    for path in backup_path:
        if os.path.exists(path):
            shutil.move(path, path.strip('.bak'))

def write_apt_source(repo_list):
    with open('/etc/apt/sources.list', "w") as f:
        for repo in repo_list:
            f.write(repo + "\n")

@contextlib.contextmanager
def pre_install(module, need_backup_files, repo_list):
    backup_path = backup_file(need_backup_files)
    write_apt_source(repo_list)
    module.run_command('apt-get clean && rm -rf /var/lib/apt/lists')
    yield
    restore_file(backup_path)

def install_packages(module, repo_list, package):
    with pre_install(module, APT_SOURCE, repo_list):
        status, stdout, stderr = module.run_command('apt-get update')
        if status != 0:
            return status, stdout, stderr
        new_package = []
        for pkg in package:
            if len(pkg.split("=")) > 1:
                name, version = pkg.split("=")
                if version:
                    status, stdout, stderr = module.run_command(["apt","policy", name])
                    if status != 0:
                        return status, stdout, stderr
                    version_reg = r"\s*(?P<version>\d?:?{}\S*)\s".format(version.replace(".", r"\."))
                    version_lst = re.findall(version_reg, stdout)
                    if not version_lst:
                        return 1, "", "no candidate version found!"
                    pkg = "{}={}".format(name, version_lst[0])
                else:
                    pkg = name
            new_package.append(pkg)
        apt_command = ["apt-get", "install", "-y"] + new_package
        status, stdout, stderr = module.run_command(apt_command)
        return status, stdout+str(apt_command), stderr+str(apt_command)


def main():
    module = AnsibleModule(  # flake8: noqa
        argument_spec=dict(
            repo=dict(required=True, type="list"),
            package=dict(required=True, type="list")
        ),
        supports_check_mode=True
    )

    if module.check_mode:
        module.exit_json(changed=False)

    repo = module.params["repo"]
    package = module.params["package"]

    status, stdout, stderr = install_packages(module, repo, package)
    if status == 0:
        module.exit_json(changed=True)
    else:
        msg = "Failed to install packages: {}".format(stderr)
        module.fail_json(msg=msg)


from ansible.module_utils.basic import *  # flake8: noqa
main()
