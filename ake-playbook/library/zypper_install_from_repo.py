import os
import shutil
import contextlib
import re

@contextlib.contextmanager
def enable_repo(module, repos):
    temp_repo_name = ["temp_repo_{}".format(i) for i in range(len(repos))]
    zypper_option = []
    for name, url in zip(temp_repo_name, repos):
        module.run_command(['zypper', 'addrepo', '-G', '-e', url, name])
        zypper_option.append('-r')
        zypper_option.append(name)
    yield zypper_option
    for name in temp_repo_name:
        module.run_command(['zypper', 'removerepo', name])

def install_packages(module, repos, packages):
    with enable_repo(module, repos) as er:
        new_package = []
        for pkg in packages:
            if len(pkg.split("=")) > 1:
                name, version = pkg.split("=")
                zypper_se_command = ['zypper','se','-s','-t','package'] + er
                zypper_se_command.append(name)
                status, stdout, stderr = module.run_command(zypper_se_command)
                if status != 0:
                    return status, stdout, stderr
                version_reg = r"\s*(?P<version>\d?:?{}\S*)\s".format(version.replace(".", r"\."))
                version_lst = re.findall(version_reg, stdout)
                if not version_lst:
                    return 1, "", "no candidate version found!"
                new_package.append("{}={}".format(name, version_lst[0].split(':')[-1]))
            else:
                new_package.append(pkg)
        status, stdout, stderr = module.run_command(['zypper','install', '-y'] + er + new_package)
    return status, stdout, stderr

def main():
    module = AnsibleModule(  # flake8: noqa
        argument_spec=dict(
            repo=dict(required=True, type="list"),
            package=dict(required=True, type="list")
        ),
        supports_check_mode=True
    )

    if module.check_mode:
        module.exit_json(changed=True)

    repos = module.params["repo"]
    packages = module.params["package"]

    status, stdout, stderr = install_packages(module, repos, packages)
    if status == 0:
        module.exit_json(changed=True)
    else:
        msg = "Failed to install packages: {}".format(stderr)
        module.fail_json(msg=msg)

from ansible.module_utils.basic import *  # flake8: noqa
main()
