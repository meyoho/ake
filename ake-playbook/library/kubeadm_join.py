#!/usr/bin/python
# (c) 2018, YongSong You <ysyou@alauda.io>

__metaclass__ = type


ANSIBLE_METADATA = {'metadata_version': '1.0',
                    'status': ['preview'],
                    'supported_by': 'alauda'}

DOCUMENTATION = '''
---
module: kubeadm_join
short_description: Wrapping kubeadm join function
author: "YongSong You"
description:
  - Join kubernetes master or node.
options:
  node_type:
    description:
      - Whether to join a master or node.
    choices: ["master", "node"]
  token:
    description:
      - A string of kubeadm bootstrap token
  discovery_addr:
    description:
      - A string of kubeadm discovery address which points to k8s apiserver
  ignore_preflight_errors:
    description:
      - A stringSlice of checks whose errors will be shown as warnings
  # feature_gates:
  #   description: |
  #     A map of feature-gates options for kubeadm. Options are:
  #     Auditing=true|false (ALPHA - default=false)
  #     DynamicKubeletConfig=true|false (ALPHA - default=false)
  #     SelfHosting=true|false (ALPHA - default=false)
  #     StoreCertsInSecrets=true|false (ALPHA - default=false)k
  #   default: {}
  # node_name:
  #   description:
  #     - A string of node name
  # cri_socket:
  #   description:
  #     - A String of CRI socket path
  #   default: "/var/run/dockershim.sock"
'''

RETURN = """ # """

EXAMPLES = """
- name: Join a k8s master
  kubeadm_join:
    node_type: master
    token: abcdef.abcdefghij123456
    discovery_addr: 172.31.42.169:6443
    ignore_preflight_errors: IsPrivilegedUser,Swap

- name: Join a k8s node
  kubeadm_join:
    node_type: node
    token: abcdef.abcdefghij123456
    discovery_addr: 172.31.42.169:6443
"""


class Kubeadm():

    def __init__(self, module, token):
        self.module = module
        self.token = token
        self.kubeadm_path = module.get_bin_path("kubeadm", required=True)

    def join_master(self, discovery_addr, ignore_preflight_errors=None):
        command = [
            self.kubeadm_path, "join", "--experimental-control-plane", "--token",
            self.token, "--discovery-token-unsafe-skip-ca-verification", discovery_addr
        ]
        if ignore_preflight_errors:
            command += ["--ignore-preflight-errors", ignore_preflight_errors]
        status, stdout, stderr = self.module.run_command(command)
        if status != 0:
            msg = "Failed to join current node as master: {}".format(stderr)
            self.module.fail_json(msg=msg)

        return dict(changed=True)

    def join_node(self, discovery_addr, ignore_preflight_errors=None):
        command = [
            self.kubeadm_path, "join", "--token", self.token, "--discovery-token-unsafe-skip-ca-verification",
            discovery_addr
        ]
        if ignore_preflight_errors:
            command += ["--ignore-preflight-errors", ignore_preflight_errors]
        status, stdout, stderr = self.module.run_command(command)
        if status != 0:
            msg = "Failed to join current node as node: {}".format(stderr)
            self.module.fail_json(msg=msg)

        return dict(changed=True)


def main():
    """The main function."""
    module = AnsibleModule(
        argument_spec=dict(
            node_type=dict(type="str", choices=["master", "node"]),
            token=dict(type="str"),
            discovery_addr=dict(type="str"),
            ignore_preflight_errors=dict(type="str", default=""),
        ),
        supports_check_mode=True)

    node_type = module.params['node_type']
    token = module.params["token"]
    discovery_addr = module.params["discovery_addr"]
    ignore_preflight_errors = module.params["ignore_preflight_errors"]
    kubeadm = Kubeadm(module, token)

    if node_type == 'master':
        rst = kubeadm.join_master(discovery_addr, ignore_preflight_errors)

    if node_type == 'node':
        rst = kubeadm.join_node(discovery_addr, ignore_preflight_errors)

    module.exit_json(**rst)


from ansible.module_utils.basic import *  # flake8: noqa
main()