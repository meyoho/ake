#!/usr/bin/python
# (c) 2019, YongSong You <ysyou@alauda.io>

__metaclass__ = type

ANSIBLE_METADATA = {'metadata_version': '1.0',
                    'status': ['preview'],
                    'supported_by': 'alauda'}

DOCUMENTATION = """
module: kube_taint
short_description: Manage kubernetes nodes' taints
description:
  - Taint and Untaint cluster nodes
version: "1.0"
options:
  key:
    required: True
    type: str
    default: null
    description:
      - The key of the taint
  value:
    required: False
    type: str
    default: null
    description:
      - The value of the taint
        If you want to untaint the node you don't need to fill in this field
  effect:
    required: False
    type: str
    choices: ["NoSchedule", "PreferNoSchedule", "NoExecute"]
    default: null
    description:
      - The effect of the taint
        In case of you want to untaint nodes, if you don't specify this field then all taints matched specified key will be deleted, otherwise only the taint matches both the key and value will be deleted.
  state:
    required: True
    type: str
    choices: ["present", "absent"]
    default: null
    description:
      - present will make sure the taint will exist on specified nodes,
        absent will make sure the taint will not exist on specified nodes
  nodes:
    required: True
    options:
      all:
        required: False
        type: bool
        default: False
        description:
          - If apply this taint/untaint action to all nodes.
            If this field is set true, the name and selector fields will be ignored.
      selector:
        required: False
        type: str
        default: null
        description:
          - Label query selector to filter on, taints will only be applied to selected nodes.
            If this field is set true, the name field will be ignored
      name:
        required: False
        type: str
        default: null
        description:
          - The name of the node which will be taint/untaint.
requirements:
  - kubectl
"""

EXAMPLES = """
- name: taint node1
  kube_taint:
    key: test
    value: content
    effect: NoSchedule
    state: present
    nodes:
      name: "node1"
      
- name: taint nodes matched label selector
  kube_taint:
    key: test
    value: content
    effect: PreferNoSchedule
    state: present
    nodes:
      selector: "key1=value1,key2=value2"
 
- name: taint all nodes
  kube_taint:
    key: test
    value: content
    effect: NoExecute
    state: present
    nodes:
      all: True
      
- name: untaint all effects of a specified key
  kube_taint:
    key: test
    state: absent
    nodes:
      name: "node3"
      
- name: untaint a specified effect
  kube_taint:
    key: test
    effect: NoSchedule
    state: absent
    nodes:
      node: "node4"

kube_taint:
  key:
  value:
  effect: "NoSchedule/PreferNoSchedule/NoExecute"
  state: "present/absent"
  nodes: 
    all: True/False
    selector: ""
    name: "master1" 
"""


class KubeTaintManager:

    def __init__(self, module, nodes):
        self.module = module
        self.kubectl =  module.get_bin_path('kubectl', True)
        self.base_cmd = [self.kubectl, "taint", "nodes"]
        if nodes.get("all"):
            self.base_cmd.append("--all")
        elif nodes.get("selector"):
            self.base_cmd.extend(["-l", nodes["selector"]])
        else:
            self.base_cmd.append(nodes["name"])

    def _execute(self, cmd, ignored_errors=()):
        try:
            rc, out, err = self.module.run_command(cmd)
            if rc != 0:
                ignored = False
                for e in ignored_errors:
                    if e.lower() in err.lower():
                        ignored = True
                        break
                if not ignored:
                    self.module.fail_json(
                        msg=("error running {} command (rc={}), "
                             "out='{}', err='{}'").format(" ".join(cmd), rc, out, err)
                    )
        except Exception as exc:
            self.module.fail_json(
                msg='error running {} command: {}'.format(" ".join(cmd), exc))
        return out.splitlines()

    def taint(self, key, value, effect):
        cmd = ["--overwrite", "{}={}:{}".format(key, value, effect)]
        self._execute(self.base_cmd + cmd)

    def untaint(self, key, effect=None):
        if effect:
            cmd = ["{}:{}-".format(key, effect)]
        else:
            cmd = ["{}-".format(key)]
        self._execute(self.base_cmd + cmd, ignored_errors=["not found"])


def main():
    module = AnsibleModule(
        argument_spec=dict(
            key=dict(required=True),
            value=dict(),
            effect=dict(choices=["NoSchedule", "PreferNoSchedule", "NoExecute"]),
            state=dict(required=True, choices=["present", "absent"]),
            nodes=dict(required=True, type="dict")
        ),
        supports_check_mode=True)
    if module.check_mode:
        module.exit_json(changed=False)

    key = module.params["key"]
    value = module.params.get("value")
    effect = module.params.get("effect")
    state = module.params["state"]
    nodes = module.params["nodes"]

    if not any([nodes.get("all"), nodes.get("selector"), nodes.get("name")]):
        module.fail_json(msg="no nodes specified.")
    manager = KubeTaintManager(module, nodes)

    if state == "present":
        if not value:
            module.fail_json(msg="parameter value must be specified when state is present.")
        if not effect:
            module.fail_json(msg="parameter effect must be specified when state is present.")
        manager.taint(key, value, effect)
    else:
        manager.untaint(key, effect)

    module.exit_json(changed=True)


from ansible.module_utils.basic import *  # flake8: noqa
main()