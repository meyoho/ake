import os
import shutil
import contextlib
import subprocess
import re

YUM_REPOS_PATH = '/etc/yum.repos.d/'
TEMP = YUM_REPOS_PATH + 'temp/'

def backup_repos():
    repos = os.listdir(YUM_REPOS_PATH)
    if not os.path.exists(TEMP):
        os.mkdir(TEMP)
    for r in repos:
        shutil.move(YUM_REPOS_PATH + r,TEMP)
    shutil.copy('/etc/yum.conf','/etc/yum.temp')
    subprocess.call('sed -i "s/obsoletes=1/obsoletes=0/g" /etc/yum.conf',shell=True)
    subprocess.call('sed -i "s/plugins=1/plugins=0/g" /etc/yum.conf',shell=True)

def restore_repos():
    if os.path.exists(TEMP):
        for i in os.listdir(TEMP):
            shutil.move(TEMP + i,YUM_REPOS_PATH)
        shutil.rmtree(TEMP)
    shutil.move('/etc/yum.temp','/etc/yum.conf')

@contextlib.contextmanager
def enable_repo(repos):
    backup_repos()
    temp_repo_name = ["temp_repo_{}".format(i) for i in range(len(repos))]
    for name, url in zip(temp_repo_name, repos):
        repo_config = "[" + "{}".format(name) + "]\n" + \
                      "name={} \n".format(name) + \
                      "baseurl={} \n".format(url) + \
                      "enabled=1 \n" + \
                      "gpgcheck=0 \n"
        with open( YUM_REPOS_PATH + name + '.repo', 'w') as f:
            f.write(repo_config)
    yield
    restore_repos()
    for i in temp_repo_name:
        os.remove(YUM_REPOS_PATH + i + '.repo')

def install_packages(module,repos,packages):
    new_package = []
    module.run_command('yum clean all && rm -rf /var/cache/yum')
    with enable_repo(repos):
        for pkg in packages:
            if len(pkg.split("=")) > 1:
                name, version = pkg.split("=")
                status, stdout, stderr = module.run_command(['yum', 'list', 'available', name, '--showduplicates', '-q'])
                if status != 0:
                    return status, stdout, stderr
                version_reg = r"\s*(?P<version>\d?:?{}\S*)\s".format(version.replace(".", r"\."))
                version_lst = re.findall(version_reg, stdout)
                if not version_lst:
                    return 1, "", "no candidate version found!"
                new_package.append("{}-{}".format(name, version_lst[-1].split(':')[-1]))
            else:
                new_package.append(pkg)
        status, stdout, stderr = module.run_command(['yum', "install", "-y"] + new_package)
    return status, stdout, stderr

def main():
    module = AnsibleModule(  # flake8: noqa
        argument_spec=dict(
            repo=dict(required=True, type="list"),
            package=dict(required=True, type="list")
        ),
        supports_check_mode=True
    )

    if module.check_mode:
        module.exit_json(changed=True)

    repos = module.params["repo"]
    packages = module.params["package"]

    status, stdout, stderr = install_packages(module, repos, packages)
    if status == 0:
        module.exit_json(changed=True)
    else:
        msg = "Failed to install packages: {}".format(stderr)
        module.fail_json(msg=msg)

from ansible.module_utils.basic import *  # flake8: noqa
main()
