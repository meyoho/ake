#!/usr/bin/python
# (c) 2018, YongSong You <youyongsong@gmail.com>

from __future__ import absolute_import, division, print_function
import os
import time
import socket
import threading
import SocketServer

__metaclass__ = type


ANSIBLE_METADATA = {'metadata_version': '1.0',
                    'status': ['preview'],
                    'supported_by': 'ysyou'}

EXAMPLE_RESP = "hello world!"
ERROR_HELP_MSG = """\
load balancer check failed, please make sure your lb meet the following conditions:
* lb support route the request to the host which sent the request
* lb reverse proxy mode is configured as tcp
* lb support health check and only route request to the healthy host
"""
TIME_AWAIT_RETRIES = 5
CONNECT_RETRIES = 5

class RouteError(Exception):
    pass

class ThreadedTCPRequestHandler(SocketServer.BaseRequestHandler):

    def handle(self):
        response = EXAMPLE_RESP
        self.request.sendall(response)

class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    pass

def test_connection(ip, port, message, timeout=None):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    if not timeout:
        sock.settimeout(timeout)
    try:
        sock.connect((ip, port))
        if not timeout:
            sock.settimeout(None)
    except socket.timeout:
        raise RouteError("Connection timeout, maybe cannot find route to the host.")
    except socket.error as msg:
        raise RouteError(str(msg))
    try:
        sock.sendall(message)
        response = sock.recv(1024)
        if response != EXAMPLE_RESP:
            raise RouteError("Recieved wrong response, maybe lb miss routed or intercepted the request.")
    finally:
        sock.close()

def test_example_connection(lb_host, lb_port, local_port):
    retry_time = 0
    while retry_time < TIME_AWAIT_RETRIES:
        try:
            server = ThreadedTCPServer(("0.0.0.0", local_port), ThreadedTCPRequestHandler)
        except Exception as err:
            if "Address already in use" in str(err):
                retry_time += 1
                time.sleep(15*retry_time)
                continue
            raise
        break
    else:
        raise Exception("Could not start a tcp server at port {}".format(local_port))

    # Start a thread with the server -- that thread will then start one
    # more thread for each request
    server_thread = threading.Thread(target=server.serve_forever)
    # Exit the server thread when the main thread terminates
    server_thread.daemon = True
    server_thread.start()

    connect_retry = 0
    # some lb may need warm-up time to setup health-check, so we give them some opportunities to fail
    while connect_retry < CONNECT_RETRIES:
        for i in range(10):
            try:
                test_connection(lb_host, lb_port, "")
            except Exception as err:
                connect_retry += 1
                if connect_retry < CONNECT_RETRIES:
                    time.sleep(15)
                    continue
                else:
                    server.shutdown()
                    server.server_close()
                    if i > 0:
                        raise RouteError("lb test failed some time, maybe lb does not support health check, and route request to the bad host. failed error: {}".format(err))
                    raise
            time.sleep(1)
        break

    server.shutdown()
    server.server_close()


def main():
    module = AnsibleModule(
        argument_spec=dict(
            lb_host=dict(type='str'),
            lb_port=dict(type='int'),
            local_port=dict(type='int')
        ),
        supports_check_mode=True)

    lb_host = module.params['lb_host']
    lb_port = module.params['lb_port']
    local_port = module.params['local_port']
    try:
        test_example_connection(lb_host, lb_port, local_port)
    except RouteError as err:
        err_msg = "{}\n{}".format(err, ERROR_HELP_MSG)
        module.fail_json(msg=err_msg)
    except Exception as err:
        err_msg = "Some unexpected error happened: {}".format(err)
        module.fail_json(msg=err_msg)

    module.exit_json(changed=False)


from ansible.module_utils.basic import *  # flake8: noqa
main()