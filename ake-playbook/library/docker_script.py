def main():
    module = AnsibleModule( # flake8: noqa
        argument_spec=dict(
            image=dict(required=True, type="str"),
            script_executor=dict(required=True, type="str"),
            script_path=dict(required=True, type="str"),
            args=dict(required=False, type="str"),
            mounts=dict(required=False, type="list")
        ),
        supports_check_mode=True
    )

    if module.check_mode:
        module.exit_json(changed=False)

    image = module.params["image"]
    script_executor = module.params["script_executor"]
    script_path = module.params["script_path"]
    args = module.params["args"]
    mounts = module.params.get("mounts", [])

    docker_bin = module.get_bin_path("docker", required=True)
    command = [
        docker_bin, "run", "--rm", "--entrypoint", script_executor,
        "-v", "{script_path}:{script_path}".format(script_path=script_path)
    ]
    for m in mounts:
        command += ["-v", m]
    command += [image, script_path]
    if args:
        command.append(args)

    status, stdout, stderr = module.run_command(command)

    if status == 0:
        module.exit_json(changed=True)
    else:
        msg = "Failed to run docker script: {}".format(stderr)
        module.fail_json(msg=msg)


from ansible.module_utils.basic import *  # flake8: noqa
main()