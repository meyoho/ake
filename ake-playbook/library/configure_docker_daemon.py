#!/usr/bin/python
# (c) 2018, YongSong You <ysyou@alauda.io>

import os
import json

__metaclass__ = type


ANSIBLE_METADATA = {'metadata_version': '1.0',
                    'status': ['preview'],
                    'supported_by': 'alauda'}

DOCKERCFG_DIR = "/etc/docker"
DOCKERCFG_PATH = "{}/daemon.json".format(DOCKERCFG_DIR)


def configure_docker_daemon(module, default_runtime=None):
    if not os.path.exists(DOCKERCFG_DIR):
        os.mkdir(DOCKERCFG_DIR)

    changed = False
    dockercfg = {}
    if os.path.exists(DOCKERCFG_PATH):
        with open(DOCKERCFG_PATH, "r") as f:
            try:
                dockercfg = json.load(f)
            except Exception as e:
                msg = "Failed to load {}: {}".format(DOCKERCFG_PATH, e)
                module.fail_json(msg=msg)
    
    if default_runtime and default_runtime != dockercfg.get("default-runtime"):
        dockercfg["default-runtime"] = default_runtime
        changed = True

    if changed:
        with open(DOCKERCFG_PATH, "w") as f:
            json.dump(dockercfg, f)
        status, stdout, stderr = module.run_command(["pkill", "-SIGHUP", "dockerd"])
        if status != 0:
            msg = "Failed to reload docker daemon config: {}".format(stderr)
            module.fail_json(msg=msg)

    return dict(changed=changed)


def main():
    """The main function."""
    module = AnsibleModule(
        argument_spec=dict(
            default_runtime=dict(type="str")
        ),
        supports_check_mode=True)

    default_runtime = module.params["default_runtime"]
    if module.check_mode:
        module.exit_json(changed=False)

    rst = configure_docker_daemon(module, default_runtime)
    module.exit_json(**rst)


from ansible.module_utils.basic import *  # flake8: noqa
main()