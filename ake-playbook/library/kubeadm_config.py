#!/usr/bin/python
# (c) 2018, YongSong You <ysyou@alauda.io>

__metaclass__ = type


ANSIBLE_METADATA = {'metadata_version': '1.0',
                    'status': ['preview'],
                    'supported_by': 'alauda'}

OLD_KUBEADM_CONFIG_PATH = "/tmp/old_kubeadm_config.yaml"
NEW_KUBEADM_CONFIG_PATH = "/tmp/new_kubeadm_config.yaml"


def _get_kubeadm_config_data(module, field):
    kubectl_path = module.get_bin_path("kubectl", required=True)
    cmd = [kubectl_path, "get", "cm/kubeadm-config", "-n", "kube-system", "-o", "jsonpath='{"+field+"}'"]
    status, stdout, stderr = module.run_command(cmd)
    if status != 0:
        msg = "Failed to get kubeadm-config : {}.\ncmd is {}".format(stderr, " ".join(cmd))
        module.fail_json(msg=msg)
    return stdout.strip("'")

def download_kubeadm_config(module, download_path):
    data = _get_kubeadm_config_data(module, ".data")
    if "MasterConfiguration" in data:
        kubeadm_config = _get_kubeadm_config_data(module, ".data.MasterConfiguration")
    elif "ClusterConfiguration" in data:
        kubeadm_config = _get_kubeadm_config_data(module, ".data.ClusterConfiguration")
    else:
        module.fail_json(msg=("Could not recognize kubeadm config data, "
                              "'MasterConfiguration' or 'ClusterConfiguration' field not found."))
    with open(download_path, "w") as f:
        f.write(kubeadm_config)

    return dict(changed=True)

def migrate_kubeadm_config(module):
    kubeadm_path = module.get_bin_path("kubeadm", required=True)
    download_kubeadm_config(module, OLD_KUBEADM_CONFIG_PATH)
    with open(OLD_KUBEADM_CONFIG_PATH, "r") as f:
        if "ClusterConfiguration" in f.read():
            return dict(changed=False)

    migrate_cmd = [
        kubeadm_path, "config", "migrate", "--old-config", OLD_KUBEADM_CONFIG_PATH,
        "--new-config", NEW_KUBEADM_CONFIG_PATH
    ]
    status, stdout, stderr = module.run_command(migrate_cmd)
    if status != 0:
        msg = "Failed to migrate kubeadm config : {}.\ncmd is {}".format(stderr, " ".join(migrate_cmd))
        module.fail_json(msg=msg)

    upload_cmd = [
        kubeadm_path, "config", "upload", "from-file", "--config", NEW_KUBEADM_CONFIG_PATH
    ]
    status, stdout, stderr = module.run_command(upload_cmd)
    if status != 0:
        msg = "Failed to upload kubeadm config : {}.\ncmd is {}".format(stderr, " ".join(upload_cmd))
        module.fail_json(msg=msg)

    return dict(changed=True)


def main():
    """The main function."""
    module = AnsibleModule(
        argument_spec=dict(
            action=dict(choices=["migrate", "download", "upload"]),
            download_path=dict(type="str")
        ),
        supports_check_mode=True)

    action = module.params["action"]
    download_path = module.params["download_path"]
    if module.check_mode:
        module.exit_json(changed=True)

    if action == "migrate":
        rst = migrate_kubeadm_config(module)
    elif action == "download":
        if not download_path:
            module.fail_json(msg="parameter download_path is not specified.")
        rst = download_kubeadm_config(module, download_path)
    else:
        module.fail_json(msg="action {} is not supported.".format(action))

    module.exit_json(**rst)


from ansible.module_utils.basic import *  # flake8: noqa
main()