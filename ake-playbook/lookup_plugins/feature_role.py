import os

from ansible.plugins.lookup import LookupBase


class LookupModule(LookupBase):

    def run(self, terms, variables=None, **kwargs):

        ret = []
        current_step = variables.get("step", "")
        enabled_features = variables.get("enabled_features", [])

        features_path = os.path.abspath(
            os.path.join(os.path.dirname(os.path.dirname(__file__)), "roles/features")
        )
        for feature in enabled_features:
            if feature not in os.listdir(features_path):
                continue
            steps = os.listdir("{}/{}".format(features_path, feature))
            if current_step in steps:
                ret.append("features/{}/{}".format(feature, current_step))

        return ret
