[![Alauda coverage](https://img.shields.io/badge/coverage-98%25-green.svg)](https://bitbucket.org/mathildetech/ake/overview)
[![Alauda tests](https://img.shields.io/badge/tests-52%2F52-green.svg)](https://bitbucket.org/mathildetech/ake/overview)
# ake
![ake](./ake.png "Ake")

Alauda Kubernetes Engine provide you an extremely convenient way to deploy a production grade Kubernetes cluster. One command -- Boom! -- Kubernetes cluster shows up.

## Local Development
ake project use [invoke](http://docs.pyinvoke.org/en/latest/getting_started.html) as task execution tool. All development related tasks can be found using `inv -l`. 

### Prerequirements
* `python3`, `pip3`
* run `pip3 install -r requirements-dev.txt` to setup development environment.

### Common Tasks
* `inv lint`: you should run this command to check your code quality before every time you want to make a commit.
* `inv install`: this command will install ake cli from source code in editable mode(the changes of source code will apply to ake cli real time).
* `inv build`: this command will package ake into a single executable file. The format of the executable file depends on environment the command run, this task is commonly used in ci environment. By default this command will using `one file` mode, and you can use `--debug` option to switch build into `one folder` mode.
* `inv clean`: this command will remove the useless intermediate artifacts generated during other processes.

### Contribute
* Please make sure all the UTs be passed, and the coverage should not less than current percentage.
* We use [shields](https://shields.io/) to create the coverage svg. Since we have not automated all the tests, if you increase the percentage of coverage, please click this [link](https://shields.io/) to create a new svg manually.

### Tips
* Make sure you have root right when you run UTs.
