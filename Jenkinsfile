library "alauda-cicd"
def language = "python"
AlaudaPipeline {
    config = [
    agent: 'python-3.6',
    folder: '.',
    scm: [
    credentials: 'global-credentials-aiops'
    ],
    docker: [
    repository: "index.alauda.cn/alaudak8s/ake",
    credentials: "infrastructure-dockercfg--infrastructure--alauda-registry",
    context: ".",
    dockerfile: "Dockerfile",
    enabled: false
    ],
    sonar: [
    binding: "infra-sq",
    enabled: false
    ],
    ]
    env = [
    GOPATH: "",
    GOPROXY: "https://athens.acp.alauda.cn",
//   AWS_ACCESS_KEY_ID: credentials("ake_aws_access_key_id"),
//   AWS_SECRET_ACCESS_KEY: credentials("ake_aws_secret_access_key"),
    AWS_DEFAULT_REGION: "cn-north-1",
    ]
    steps = [
        [
            name: "Unit test",
            container: language,
            groovy: [
            """
                try {
                sh script: '''
                apt-get update
                apt-get install -y python-pip
                pip install --upgrade pip
                pip install -r requirements-dev.txt'''
                } finally {
                    junit allowEmptyResults: true, testResults: 'pkg/**/*.xml'
                }
            """
            ]
        ],
        [
            name: "Build ake app",
            container: language,
            groovy: [
                """
                    sh "inv build"
                    archiveArtifacts artifacts: 'dist/*', fingerprint: true
                """,
                ]
        ],
        [
            name: "Build ake image",
            container: "tools",
            groovy: [
                """
                    CURRENT_VERSION = sh (script: '''cat ake/__version__.py | tr -d "'"|tr -d "__version__ ="''', returnStdout: true)
                    sh "echo \$CURRENT_VERSION"
                    CURRENT_VERSION = CURRENT_VERSION.trim()
                    if (env.GIT_BRANCH == "master") {
                        CURRENT_VERSION = CURRENT_VERSION + "-beta-" + env.COMMIT_ID
                    } else if (!(env.GIT_BRANCH).startsWith('release-')) {
                        CURRENT_VERSION = CURRENT_VERSION + "-alpha-" + env.COMMIT_ID
                    }
                    echo "Current version: \$CURRENT_VERSION"
                    IMAGE_TAG = CURRENT_VERSION
                    echo "Image tag: \$IMAGE_TAG"
                    IMAGE = deploy.dockerBuild("Dockerfile", ".", "index.alauda.cn/alaudak8s/ake", env.COMMIT_ID, "alaudak8s",)
                    IMAGE.start()
                    IMAGE.push().push(IMAGE_TAG).push("latest")
                    if (env.GIT_BRANCH == "master") {
                        K8S_MINOR_VERSION = sh (script: '''ake version | grep 'kubernetes'|awk '{print \$4}' ''', returnStdout: true)
                        IMAGE.push(K8S_MINOR_VERSION)
                    } else if ((env.GIT_BRANCH).startsWith('release-')) {
                        K8S_PATCH_VERSION = sh (script: '''ake version | grep 'kubernetes'|awk '{print \$6}' ''', returnStdout: true)
                        IMAGE.push(K8S_PATCH_VERSION)
                    }
                """,
                ]
        ],
        [
            name: "upload-s3",
            container: language,
            groovy: [
                """
                    sh "pip3 install awscli"
                    withCredentials([string(credentialsId: 'ake_aws_access_key_id', variable: 'ID'), string(credentialsId: 'ake_aws_secret_access_key', variable: 'KEY')]) {
                        // some block
                        env.AWS_ACCESS_KEY_ID = "\${ID}"
                        env.AWS_SECRET_ACCESS_KEY = "\${KEY}"
                    }
                    //env.AWS_ACCESS_KEY_ID = credentials("ake_aws_access_key_id")
                    //env.AWS_SECRET_ACCESS_KEY = credentials("ake_aws_secret_access_key")
                    if (env.GIT_BRANCH == "release") {
                        sh "aws s3 cp ./dist/ake s3://get.alauda.cn/deploy/ake/ake"
                    } else if ((env.GIT_BRANCH).startsWith('release-'))
                    {
                        sh "aws s3 cp ./dist/ake s3://get.alauda.cn/deploy/ake/ake-\$CURRENT_VERSION"
                        K8S_VERSION = sh (script: '''ake version | grep 'kubernetes'|awk '{print \$6}' ''', returnStdout: true)
                        sh "aws s3 cp ./dist/ake s3://get.alauda.cn/deploy/ake/ake-\$K8S_VERSION"
                    } else if (env.GIT_BRANCH == "master") {
                        sh "aws s3 cp ./dist/ake s3://get.alauda.cn/deploy/ake/ake-master"
                    }
                """
            ]
        ],
    ]
}
