-r ./requirements.txt
twine==1.9.1
invoke==0.22.0
flake8==3.5.0
pyinstaller==3.3.1
pytest
pytest_mock
coverage
qcloudapi-sdk-python