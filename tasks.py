import os

from invoke import task


def status(s):
    """Prints things in bold."""
    print("🏷️  \033[1m{0}\033[0m".format(s))


@task
def lint(ctx):
    """
    Lint code using flake8 tools.
    """
    status("linting code ...")
    ctx.run("flake8 --show-source --statistics --count")


@task
def install(ctx):
    """
    Install ake as a cli tool for local development.
    """
    status("installing ake ...")
    ctx.run("pip3 install --editable .")
    status("ake installed.")


@task(lint)
def test(ctx, junit=False):
    """
    Run ake test cases.
    """
    unit_test_cmd = (
        "python3 -m coverage run --source=./ake "
        "--omit=./ake/__main__.py,./ake/__version__.py,./ake/patches/*.py,./ake-playbook/library/*.py "
        "-m pytest -s ./ake/tests/"
    )
    if junit:
        unit_test_cmd += " --junitxml ut_result.xml"
    status("begining to run test cases ...")
    ctx.run("cp ./deploy.yml ake-playbook")
    ctx.run("cp ./add_node.yml ake-playbook")
    ctx.run(unit_test_cmd)
    ctx.run("rm -rf test.json")
    ctx.run("python3 -m coverage report -m")
    ctx.run("python3 -m coverage xml -i")
    ctx.run("git checkout ./ake-playbook/deploy.yml ./ake-playbook/add_node.yml")


@task
def stest(ctx, func):
    """
    Run single test case.
    """
    ctx.run("python3 -m pytest ./ake/tests/test_deploy.py::TestDeploy::{}".format(func))


@task(install)
def build(ctx, debug=False):
    """
    Build ake project to a single executable file using pyinstaller.
    """
    status("building with pyinstaller ...")
    if os.path.exists("./man/ake.1.gz"):
        ctx.run("rm -rf ./man/ake.1.gz")
    if not os.path.exists("./man"):
        os.makedirs("./man")
    ctx.run("gzip -c ./ake.1 > ./man/ake.1.gz")
    if debug:
        ctx.run("pyinstaller --noconfirm build_one_folder.spec")
    else:
        ctx.run("pyinstaller --noconfirm build_one_file.spec")
    print("ake build successed! you can find the executable file named ake inside dist folder now.")


@task
def clean(ctx):
    """
    Clean the ake workspace.
    """
    status("cleaning ake workspace ...")

    print("cleaning merged branches ...")
    result = ctx.run("git branch --merged master")
    branches = result.stdout.splitlines()
    for b in branches:
        b = b.strip()
        if b == "release" or b.startswith("v") or b.startswith("*"):
            continue
        print("deleting branch {}".format(b))
        try:
            ctx.run("git branch -d {}".format(b))
        except Exception as err:
            print("failed to delete {}, ignoring it.".format(b))

    print("cleaning intermediate artifacts ...")
    for artifact in ["./build", "./dist", "./ake.egg-info", "./man", "coverage.xml"]:
        if os.path.exists(artifact):
            print(f"removing {artifact} ...")
            try:
                ctx.run(f"rm -r {artifact}")
            except Exception as err:
                pass

    print("removing __pycache__ files ...")
    try:
        ctx.run("find . -name '__pycache__' -exec rm -r {} \;")
    except Exception as err:
        pass

    print("removing retry files ...")
    try:
        ctx.run("find . -name '*.retry' -exec rm -r {} \;")
    except Exception as err:
        pass

    print("removing vagrant box ...")
    try:
        ctx.run("rm ./vagrant_dev/playground/*.box")
    except Exception as err:
        pass


@task
def build_playground(ctx):
    """
    Build ake playground(a vagrant box).
    """
    status("building ake playground box ...")
    ctx.run("cd ./vagrant_dev/playground && vagrant package --output ake-playground.box")
    ctx.run("vagrant box add --force youyongsong/ake-playground ./vagrant_dev/playground/ake-playground.box")
