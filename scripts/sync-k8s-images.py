#!/usr/bin/env python
# flake8: noqa

import os
import time
import subprocess


DEFAULT_VERSION = "v1.15.3"


k8s_version = raw_input("Input k8s version(default {}):".format(DEFAULT_VERSION))
if not k8s_version.strip():
    k8s_version = DEFAULT_VERSION

print("========== Syncing {} components ==========".format(k8s_version))
time.sleep(2)

if os.path.exists("./kubeadm"):
    print("========== Cleanuping kubeadm ==========")
    command = "rm kubeadm"
    print(command)
    subprocess.check_call(command, shell=True)

print("========== Downloading kubeadm ==========")
command = "curl -L --remote-name-all https://storage.googleapis.com/kubernetes-release/release/{}/bin/linux/amd64/kubeadm && chmod +x kubeadm".format(k8s_version)
print(command)
subprocess.check_call(command, shell=True)
print("\n")

print("========== Getting images list ==========")
out = subprocess.check_output(["./kubeadm", "config", "images", "list"]).decode().strip()
images_lst = out.split("\n")
print("\n")

print("========== Syncing images ==========")
for img in images_lst:
    img = img.strip()
    print("---------- Syncing {} ---------".format(img))
    pull_cmd = "docker pull {}".format(img)
    print("\t{}".format(pull_cmd))
    subprocess.check_call(pull_cmd, shell=True)

    cn_img = img.replace("k8s.gcr.io", "index.alauda.cn/claas")
    tag_cn_cmd = "docker tag {} {}".format(img, cn_img)
    print("\t{}".format(tag_cn_cmd))
    subprocess.check_call(tag_cn_cmd, shell=True)

    # io_img = img.replace("k8s.gcr.io", "index.alauda.io/claas")
    # tag_io_cmd = "docker tag {} {}".format(img, io_img)
    # print("\t{}".format(tag_io_cmd))
    # subprocess.check_call(tag_io_cmd, shell=True)

    push_cn_cmd = "docker push {}".format(cn_img)
    print("\t{}".format(push_cn_cmd))
    subprocess.check_call(push_cn_cmd, shell=True)

    # push_io_cmd = "docker push {}".format(io_img)
    # print("\t{}".format(push_io_cmd))
    # subprocess.check_call(push_io_cmd, shell=True)

    print("---------- Synced {} ---------".format(img))
    print("\n")

if os.path.exists("./kubeadm"):
    print("========== Cleanuping kubeadm ==========")
    command = "rm kubeadm"
    print(command)
    subprocess.check_call(command, shell=True)
