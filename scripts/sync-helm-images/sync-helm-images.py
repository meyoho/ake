#!/usr/bin/env python
# flake8: noqa

import os
import time
import subprocess

DEFAULT_VERSION = "v2.14.3"

helm_version = raw_input("Input helm version(default {}):".format(DEFAULT_VERSION))
if not helm_version.strip():
    helm_version = DEFAULT_VERSION

print("========== Building helm {} image ==========".format(helm_version))
time.sleep(2)
print("\n")

print("========== Downloading helm app ==========")
command = """
wget https://storage.googleapis.com/kubernetes-helm/helm-{version}-linux-amd64.tar.gz && \
tar -xzvf helm-{version}-linux-amd64.tar.gz && \
mv linux-amd64/helm . && \
rm helm-{version}-linux-amd64.tar.gz && \
rm -rf linux-amd64
""".format(version=helm_version)
print(command)
subprocess.check_call(command, shell=True)
print("\n")

print("========== Building helm image ==========")
command = "docker build -t index.alauda.cn/claas/helm:{} .".format(helm_version)
print(command)
subprocess.check_call(command, shell=True)
print("\n")

# print("========== Tagging helm io image ==========")
# command = "docker tag index.alauda.cn/claas/helm:{} index.alauda.io/claas/helm:{}".format(helm_version, helm_version)
# print(command)
# subprocess.check_call(command, shell=True)
# print("\n")

print("========== Pushing image ==========")
push_cn_command = "docker push index.alauda.cn/claas/helm:{}".format(helm_version)
print(push_cn_command)
subprocess.check_call(push_cn_command, shell=True)
# push_io_command = "docker push index.alauda.io/claas/helm:{}".format(helm_version)
# print(push_io_command)
# subprocess.check_call(push_io_command, shell=True)

print("========== Syncing tiller image ==========")
img = "gcr.io/kubernetes-helm/tiller:{}".format(helm_version)
cn_img = "index.alauda.cn/claas/tiller:{}".format(helm_version)
io_img = "index.alauda.io/claas/tiller:{}".format(helm_version)

pull_cmd = "docker pull {}".format(img)
print("\t{}".format(pull_cmd))
subprocess.check_call(pull_cmd, shell=True)

tag_cn_cmd = "docker tag {} {}".format(img, cn_img)
print("\t{}".format(tag_cn_cmd))
subprocess.check_call(tag_cn_cmd, shell=True)

# tag_io_cmd = "docker tag {} {}".format(img, io_img)
# print("\t{}".format(tag_io_cmd))
# subprocess.check_call(tag_io_cmd, shell=True)

push_cn_cmd = "docker push {}".format(cn_img)
print("\t{}".format(push_cn_cmd))
subprocess.check_call(push_cn_cmd, shell=True)

# push_io_cmd = "docker push {}".format(io_img)
# print("\t{}".format(push_io_cmd))
# subprocess.check_call(push_io_cmd, shell=True)