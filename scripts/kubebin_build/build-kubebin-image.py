#!/usr/bin/env python
# flake8: noqa

import os
import time
import subprocess

#STABLE_RELEASE="$(curl -sSL https://dl.k8s.io/release/stable.txt)"
DEFAULT_VERSION = "v1.15.3"

k8s_version = raw_input("Input k8s version(default {}):".format(DEFAULT_VERSION))
if not k8s_version.strip():
    k8s_version = DEFAULT_VERSION

print("========== Building {} image ==========".format(k8s_version))
time.sleep(2)
print("\n")

print("========== Cleanuping kube binaries ==========")
for binary in ("kubeadm", "kubelet", "kubectl"):
    if os.path.exists(binary):
        print("rm {}".format(binary))
        os.remove(binary)
print("\n")

print("========== Downloading kube binaries ==========")
for binary in ("kubeadm", "kubelet", "kubectl"):
    command = "curl -L --remote-name-all https://storage.googleapis.com/kubernetes-release/release/{}/bin/linux/amd64/{} && chmod +x {}".format(k8s_version, binary, binary)
    print(command)
    subprocess.check_call(command, shell=True)
print("\n")

print("========== Building kubebin image ==========")
command = "docker build -t index.alauda.cn/claas/kubebin:{} .".format(k8s_version)
print(command)
subprocess.check_call(command, shell=True)
print("\n")

# print("========== Tagging io image ==========")
# command = "docker tag index.alauda.cn/claas/kubebin:{} index.alauda.io/claas/kubebin:{}".format(k8s_version, k8s_version)
# print(command)
# subprocess.check_call(command, shell=True)
# print("\n")

print("========== Pushing image ==========")
push_cn_command = "docker push index.alauda.cn/claas/kubebin:{}".format(k8s_version)
print(push_cn_command)
subprocess.check_call(push_cn_command, shell=True)
# push_io_command = "docker push index.alauda.io/claas/kubebin:{}".format(k8s_version)
# print(push_io_command)
# subprocess.check_call(push_io_command, shell=True)
