#!/usr/bin/env python
# flake8: noqa

import os
import time
import subprocess


CALICO_VERSION = "v3.2.1"
FLANNEL_VERSION = "v0.9.1"


calico_version = raw_input("Input calico version(default {}):".format(CALICO_VERSION))
flannel_version = raw_input("Input flannel version(default {}):".format(FLANNEL_VERSION))
if not calico_version.strip():
    calico_version = CALICO_VERSION
if not flannel_version.strip():
    flannel_version = FLANNEL_VERSION

print("========== Syncing images ==========")
time.sleep(2)

print("========== Collecting images list ==========")
images_lst = (
    ("quay.io/calico/node:{}".format(calico_version), "claas/calico-node"),
    ("quay.io/calico/cni:{}".format(calico_version), "claas/calico-cni"),
    ("quay.io/coreos/flannel:{}".format(flannel_version), "claas/flannel"),
)

print("========== Syncing images ==========")
for img_pair in images_lst:
    img, target_repo = img_pair
    cn_img = "index.alauda.cn/{}:{}".format(target_repo, img.split(":")[-1])
    io_img = "index.alauda.io/{}:{}".format(target_repo, img.split(":")[-1])

    print("---------- Syncing {} ---------".format(img))
    pull_cmd = "docker pull {}".format(img)
    print("\t{}".format(pull_cmd))
    subprocess.check_call(pull_cmd, shell=True)

    tag_cn_cmd = "docker tag {} {}".format(img, cn_img)
    print("\t{}".format(tag_cn_cmd))
    subprocess.check_call(tag_cn_cmd, shell=True)

    # tag_io_cmd = "docker tag {} {}".format(img, io_img)
    # print("\t{}".format(tag_io_cmd))
    # subprocess.check_call(tag_io_cmd, shell=True)

    push_cn_cmd = "docker push {}".format(cn_img)
    print("\t{}".format(push_cn_cmd))
    subprocess.check_call(push_cn_cmd, shell=True)

    # push_io_cmd = "docker push {}".format(io_img)
    # print("\t{}".format(push_io_cmd))
    # subprocess.check_call(push_io_cmd, shell=True)

    print("---------- Synced {} ---------".format(img))
    print("\n")