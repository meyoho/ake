#!/usr/bin/env python
# flake8: noqa

import os
import time
import subprocess

NVIDIA_DEVICE_PLUGIN_VERSION = "1.11"

nvidia_device_plugin_version = raw_input(
    "Input nvidia device plugin version(default {}):".format(NVIDIA_DEVICE_PLUGIN_VERSION))
if not nvidia_device_plugin_version:
    nvidia_device_plugin_version = NVIDIA_DEVICE_PLUGIN_VERSION

print("========== Syncing images ==========")
time.sleep(2)

print("========== Collecting images list ==========")
images_lst = (
    ("nvidia/k8s-device-plugin:{}".format(nvidia_device_plugin_version), "claas/k8s-device-plugin"),
)

print("========== Syncing images ==========")
for img_pair in images_lst:
    img, target_repo = img_pair
    cn_img = "index.alauda.cn/{}:{}".format(target_repo, img.split(":")[-1])
    io_img = "index.alauda.io/{}:{}".format(target_repo, img.split(":")[-1])

    print("---------- Syncing {} ---------".format(img))
    pull_cmd = "docker pull {}".format(img)
    print("\t{}".format(pull_cmd))
    subprocess.check_call(pull_cmd, shell=True)

    tag_cn_cmd = "docker tag {} {}".format(img, cn_img)
    print("\t{}".format(tag_cn_cmd))
    subprocess.check_call(tag_cn_cmd, shell=True)

    # tag_io_cmd = "docker tag {} {}".format(img, io_img)
    # print("\t{}".format(tag_io_cmd))
    # subprocess.check_call(tag_io_cmd, shell=True)

    push_cn_cmd = "docker push {}".format(cn_img)
    print("\t{}".format(push_cn_cmd))
    subprocess.check_call(push_cn_cmd, shell=True)

    # push_io_cmd = "docker push {}".format(io_img)
    # print("\t{}".format(push_io_cmd))
    # subprocess.check_call(push_io_cmd, shell=True)

    print("---------- Synced {} ---------".format(img))
    print("\n")