#!/usr/bin/env python
# flake8: noqa

import os
import time
import subprocess

CERT_MANAGER_VERSION = "v0.8.0"

cert_manager_version = raw_input(
    "Input cert-manager version(default {}):".format(CERT_MANAGER_VERSION))
if not cert_manager_version:
    cert_manager_version = CERT_MANAGER_VERSION

print("========== Syncing images ==========")
time.sleep(2)

print("========== Collecting images list ==========")
images_lst = (
    ("quay.io/jetstack/cert-manager-controller:{}".format(cert_manager_version), "claas/cert-manager-controller"),
    ("quay.io/jetstack/cert-manager-webhook:{}".format(cert_manager_version), "claas/cert-manager-webhook"),
    ("quay.io/jetstack/cert-manager-cainjector:{}".format(cert_manager_version), "claas/cert-manager-cainjector"),
)

print("========== Syncing images ==========")
for img_pair in images_lst:
    img, target_repo = img_pair
    cn_img = "index.alauda.cn/{}:{}".format(target_repo, img.split(":")[-1])

    print("---------- Syncing {} ---------".format(img))
    pull_cmd = "docker pull {}".format(img)
    print("\t{}".format(pull_cmd))
    subprocess.check_call(pull_cmd, shell=True)

    tag_cn_cmd = "docker tag {} {}".format(img, cn_img)
    print("\t{}".format(tag_cn_cmd))
    subprocess.check_call(tag_cn_cmd, shell=True)

    # tag_io_cmd = "docker tag {} {}".format(img, io_img)
    # print("\t{}".format(tag_io_cmd))
    # subprocess.check_call(tag_io_cmd, shell=True)

    push_cn_cmd = "docker push {}".format(cn_img)
    print("\t{}".format(push_cn_cmd))
    subprocess.check_call(push_cn_cmd, shell=True)

    # push_io_cmd = "docker push {}".format(io_img)
    # print("\t{}".format(push_io_cmd))
    # subprocess.check_call(push_io_cmd, shell=True)

    print("---------- Synced {} ---------".format(img))
    print("\n")